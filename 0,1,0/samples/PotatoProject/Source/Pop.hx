package platformer;
/** Class Pop from platformer
  * Created by Chlorodatafile the 12/08/2015 for the first time in PotatoProject.
  *
  **/

import brocchinia.app.Surface;
import brocchinia.app.events.CatchableEvent;
import openfl.events.KeyboardEvent;
import brocchinia.app.PopUp;

class Pop extends PopUp {

    var inited:Bool;

    public function new() {
        super([CatchableEvent.KEY_DOWN],false,false);
        inited=false;
    }

    override public function onPopIn():Void {}

    override public function onPopOut():Void {}

    override public function onUpdate():Bool {
        if (!inited) {
            inited=true;
            return true;
        }
        return false;
    }

    override public function onDraw(surface:Surface):Void {
        trace(program.performer.id+" "+surface.width);
        graphics.clear();
        graphics.beginFill(0xFFFFF,0.5);
        graphics.drawRect(100,100,surface.width-200,surface.height-200);
        graphics.endFill();
    }

    override public function caughtKeyDown(e:KeyboardEvent):Void {
        trace("caught");
        program.performer.closePopUp();
        program.performer.launch(0,[],new LoadingScreen());
    }


}
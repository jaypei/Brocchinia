package platformer;
/** Class Dk from
  * Created by Chlorodatafile the 16/07/2015 for the first time in PotatoProject.
  *
  **/

import brocchinia.app.Surface;
import brocchinia.app.LoadingInstance.LoadingStep;
import brocchinia.app.Program;
import brocchinia.io.bind.saving.BindIO;
import brocchinia.io.Ressource;
import brocchinia.io.events.BindEvent.BindingEvent;
import brocchinia.io.bind.Bind;
import brocchinia.io.Lang;
import brocchinia.app.Application;
import openfl.events.KeyboardEvent;
import brocchinia.util.math.TickDelta;

class Menu extends Program {

    var k:TickDelta;

    public function new() {
        super([KEY_DOWN,BINDING_RECEIVED,BINDING_CANCELLED],2);
    }

    override public function onStart():Void {

    }

    override public function onStop():Void {
        Ressource.get.profile.config=BindIO.config;
        Ressource.saveProfile();
        Bind.endCatchingPhase();
    }


    override public function loading(loadingStep:LoadingStep):Void {
        k=100;
        loadingStep++;
        Lang.load(0);
        trace(Lang.fixed(0));
        Bind.startCatchingPhase(new MyBindCatcher());
        loadingStep++;
    }

    override public function onUpdate():Bool {
        if (k--==0){
            trace(Lang.get(0));
            k++;
        }
        return true;
    }

    override public function onDraw(surface:Surface):Void {
        surface.foreground.clear();
        surface.foreground.beginFill(0x540000,0.5);
        surface.foreground.drawRect(0,0,1280,720);
        surface.foreground.endFill();
    }

    override public function caughtKeyDown(e:KeyboardEvent):Void {
        trace(performer.id+" keydown");
        if (!Bind.catching)
            Bind.catchInput(0);
        trace(performer.id+" keydown");
    }

    override public function caughtBindingCancelled(e:BindingEvent):Void {
        trace(performer.id+" canceled");
    }


    override public function caughtBindingReceived(e:BindingEvent):Void {
        performer.launch(2,[], new LoadingScreen());
    }


}
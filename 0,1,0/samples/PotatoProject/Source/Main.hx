package ;

import brocchinia.app.Surface;
import brocchinia.app.script.ScriptRessource;
import brocchinia.io.bind.saving.BindConfig;
import brocchinia.io.bind.saving.BindIO;
import brocchinia.io.Lang;
import brocchinia.io.bind.Bind;
import brocchinia.io.Ressource;
import openfl.ui.Keyboard;
import openfl.events.KeyboardEvent;
import brocchinia.app.Application;
import brocchinia.app.script.Script;
import openfl.display.Graphics;
import brocchinia.util.math.TimeDelta;
import brocchinia.Time;
import brocchinia.io.events.GamepadEvent;

class Main extends Script {

    public static function main() {
        Time.set();
        new Ressource<Profile>();
        Ressource.loadProfile();
        Bind.initialise();
        BindIO.config=Ressource.get.profile.config;
        Lang.initialise();
        Application.initialise([Main,Menu,Game],[DEFAULT,NONE,DEFAULT]);
        Application.launch(0,[],new LoadingScreen());
        Application.newWindow([Main,Menu,Game],[DEFAULT,NONE,DEFAULT],{width:500,height:500});
        Bind.linkToWindow(Application.windows[1].core);
        Application.windows[1].launch(0,[],new LoadingScreen());
    }

    public function new() super(
        [KEY_DOWN],
        {
            goToModule:1,
            loadingInstance:new LoadingScreen(),
            states:[
                {
                    duration:400,
                    draw:function(src:ScriptRessource,surface:Surface):Void {
                        surface.background.beginFill(0xFFFFFF);
                        surface.background.drawRect(0,0,1280,720);
                        surface.background.endFill();
                    }
                },{
                    duration:400,
                    wait:true,
                    draw:function(src:ScriptRessource,surface:Surface):Void {
                        surface.background.beginFill(0x000054,src.deltaTime%400);
                        surface.background.drawRect(0,0,1280,720);
                        surface.background.endFill();
                    }
                }
            ]
        }
    );

    override public function caughtKeyDown(e:KeyboardEvent):Void {
        switch (e.charCode) {
            case Keyboard.SPACE, Keyboard.ENTER : skip();
            case Keyboard.ESCAPE : skip(true);
            default:
        }
    }

    override public function onStop():Void {
        super.onStop();
        performer.surface.background.clear();
    }


}

typedef Profile = {config:BindConfig};


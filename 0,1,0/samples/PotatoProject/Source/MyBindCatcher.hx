package ;
/**
 * MyBindCatcher from package .
 * Created by Chlorodatafile the 24/08/2015 at 17:21, on the project PotatoProject
 * Version : 0.0.1
 *
 */


import lime.ui.KeyCode;
import brocchinia.io.bind.BindedID;
import brocchinia.io.bind.BindCatcher;
class MyBindCatcher implements BindCatcher {

    public function new() {}

    public function getForbiddenKeyList():Array<KeyCode> return [];

    public function getConstraint():Array<Bool> return [false,false,false,false,false];

    public function getCatchStopKey():KeyCode return 32;

    public function setCatchingID(v:BindedID):Void {}

    public function getCatchingID():BindedID return 0;


}

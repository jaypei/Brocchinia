package platformer;
/** Class Dk from
  * Created by Chlorodatafile the 16/07/2015 for the first time in PotatoProject.
  *
  **/

import brocchinia.app.Surface;
import brocchinia.app.LoadingInstance.LoadingStep;
import brocchinia.app.events.CatchableEvent;
import brocchinia.app.Program;
import brocchinia.io.Lang;
import brocchinia.app.Application;
import openfl.events.KeyboardEvent;
import brocchinia.util.math.TickDelta;

class Game extends Program {

    var k:TickDelta;

    public function new() {
        super([CatchableEvent.KEY_DOWN],2);
    }

    override public function onStart():Void {

    }

    override public function onStop():Void {

    }

    override public function onPause():Void {
        trace("Paused");
    }

    override public function onResume():Void {
        trace("Resumed");
    }


    override public function loading(loadingStep:LoadingStep):Void {
        k=100;
        loadingStep++;
        Lang.load(1);
        trace(Lang.fixed(0));
        loadingStep++;
    }

    override public function onUpdate():Bool {
        if (k--==0){
            trace(Lang.get(0));
            k++;
        }
        return true;
    }

    override public function onDraw(surface:Surface):Void {
        surface.foreground.clear();
        surface.foreground.beginFill(0x005400,0.5);
        surface.foreground.drawRect(0,0,1280,720);
        surface.foreground.endFill();
    }

    override public function caughtKeyDown(e:KeyboardEvent):Void {
        performer.openPopUp(new Pop());
    }
}
package platformer;
/** Class LoadingScreen from platformer
  * Created by Chlorodatafile the 10/08/2015 for the first time in PotatoProject.
  *
  **/

import brocchinia.app.LoadingInstance;

class LoadingScreen extends LoadingInstance {

    public function new() {
        super();
    }

    override public function draw():Void {
    }

    override public function dispose():Void {
        trace("loading done");
    }

}
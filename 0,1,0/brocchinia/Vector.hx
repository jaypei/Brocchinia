package brocchinia;

/** Class Vector from brocchinia
  * Created by Chlorodatafile the 02/07/15 for the first time in genlisea.
  * This class is used to simplify the usage of the multidimensionnal Vector.
  **/
import brocchinia.util.ds.Array2;
import brocchinia.util.ds.Array3;
import brocchinia.util.ds.Array4;

typedef Vector<T> = openfl.Vector<T>;

private typedef V2<T> = Vector<Vector<T>>;
private typedef V3<T> = Vector<V2<T>>;
private typedef V4<T> = Vector<V3<T>>;

@:forward(iterator)
abstract Vector4<T>(V4<T>) from V4<T> to V4<T> {

    public var length(get,never):Int;
    public inline function get_length():Int return this.length;

    public inline function new(w:Int,h:Int,d:Int,t:Int,def:Int->Int->Int->Int->T)
        this = Vector.ofArray([for (i in 0...w)
            [for (j in 0...h)
                [for (k in 0...d)
                    [for (l in 0...t) def(i,j,k,l)]]]]);

    public inline static function ofArray4<T>(t:Array4<T>):Null<Vector4<T>> return Vector.ofArray(t);

    public inline static function ofArray<T>(t:Array<Array<Array<Array<T>>>>):Null<Vector4<T>> return Vector.ofArray(t);

    public inline function clone():Null<Vector4<T>>
        return Vector.ofArray([for (i in 0...get_length())
            [for (j in 0...this[i].length)
                [for (k in 0...this[i][j].length)
                    [for (l in 0...this[i][j][k].length) this[i][j][k][l]]]]]);

    public inline function copy():Vector4<T> return this.copy();

    public inline function filter(f:T -> Bool):Vector4<T>
        return Vector.ofArray([for (i in 0...get_length())
            [for (j in 0...this[i].length)
                [for (k in 0...this[i][j].length)
                    [for (l in 0...this[i][j][k].length) if (f(this[i][j][k][l])) this[i][j][k][l]]]]]);

    @to
    public inline function toString():String
        return ""+([for (i in 0...get_length())
            [for (j in 0...this[i].length)
                [for (k in 0...this[i][j].length)
                    [for (l in 0...this[i][j][k].length) this[i][j][k][l]]]]]);

    @:arrayAccess
    public inline function get(key:Int):Null<Vector3<T>> return this[key];

    @:arrayAccess
    public inline function arrayWrite(key:Int, v:Vector3<T>):Null<Vector3<T>> return this[key]=v;
}

@:forward(iterator)
abstract Vector3<T>(V3<T>) from V3<T> to V3<T> {

    public var length(get,never):Int;
    public inline function get_length():Int return this.length;

    public inline function new(w:Int,h:Int,d:Int,def:Int->Int->Int->T)
        this = Vector.ofArray([for (i in 0...w)
            [for (j in 0...h)
                [for (k in 0...d) def(i,j,k)]]]);

    public inline static function ofArray3<T>(t:Array3<T>):Null<Vector3<T>> return Vector.ofArray(t);

    public inline static function ofArray<T>(t:Array<Array<Array<T>>>):Null<Vector3<T>> return Vector.ofArray(t);

    public inline function clone():Null<Vector3<T>>
        return Vector.ofArray([for (i in 0...get_length())
            [for (j in 0...this[i].length)
                [for (k in 0...this[i][j].length)
                    this[i][j][k]]]]);

    public inline function copy():Vector3<T> return this.copy();

    public inline function filter(f:T -> Bool):Vector3<T>
        return Vector.ofArray([for (i in 0...get_length())
            [for (j in 0...this[i].length)
                [for (k in 0...this[i][j].length)
                    if (f(this[i][j][k]))
                        this[i][j][k]]]]);

    @to
    public inline function toString():String
        return ""+( [for (i in 0...get_length())
            [for (j in 0...this[i].length)
                [for (k in 0...this[i][j].length)
                    this[i][j][k]]]]);

    @:arrayAccess
    public inline function get(key:Int):Null<Vector2<T>> return this[key];

    @:arrayAccess
    public inline function arrayWrite(key:Int, v:Vector2<T>):Null<Vector2<T>> return this[key]=v;
}

@:forward(iterator)
abstract Vector2<T>(V2<T>) from V2<T> to V2<T> {

    public var length(get,never):Int;
    public inline function get_length():Int return this.length;

    public inline function new(w:Int,h:Int,def:Int->Int->T)
        this = Vector.ofArray([for (i in 0...w) [for (j in 0...h) def(i,j)]]);

    public inline static function ofArray2<T>(t:Array2<T>):Null<Vector2<T>> return Vector.ofArray(t);

    public inline static function ofArray<T>(t:Array<Array<T>>):Vector2<T> return Vector.ofArray(t);

    public inline function clone():Null<Vector2<T>>
        return Vector.ofArray([for (i in 0...get_length()) [for (j in 0...this[i].length) this[i][j]]]);

    public inline function copy():Vector2<T> return this.copy();

    public inline function filter(f:T -> Bool):Vector2<T>
        return Vector.ofArray([for (i in 0...get_length()) [for (j in 0...this[i].length) if (f(this[i][j])) this[i][j]]]);

    @to
    public inline function toString():String
        return ""+([for (i in 0...get_length()) [for (j in 0...this[i].length) this[i][j]]]);

    @:arrayAccess
    public inline function get(key:Int):Null<Vector<T>> return this[key];

    @:arrayAccess
    public inline function arrayWrite(key:Int, v:Vector<T>):Null<Vector<T>> return this[key]=v;
}
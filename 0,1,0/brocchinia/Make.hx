package brocchinia;

/** Class Abstract from brocchinia
  * Created by Chlorodatafile the 02/07/15
  * Some class to manage an pseudo abstract class, like Java do.
  * It also contain some utilities to make an To Do Method.
  **/

enum MethodError {
    Unimplemented(name:String);
    Unfinished(name:String);
}

abstract Make(Void) {
    @:extern public inline static function Abtsract(name:String="") throw MethodError.Unimplemented(name);
    @:extern public inline static function Unfinished(name:String="") throw MethodError.Unfinished(name);
}
/* Exemple :

class MyClass {

    public function clone():MyClass Make.Abstract("clone");

    public function copy():MyClass Make.Unfinished("copy");

}
 */
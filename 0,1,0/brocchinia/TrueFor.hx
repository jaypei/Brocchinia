package brocchinia;
/** Class F from brocchinia
  * Imported by Chlorodatafile from http://yal.cc/haxe-some-cleaner-c-style-for-loops/
  * the 23/07/2015 for the first time in RVB-Project.
  * This class allow you to make traditionnal for loop.
  *
  * An exemple of the using, this in other langage :
for (var i = 0; i < 10; i++) {
	if (i == 3) i++;
	if (i == 5) continue;
	trace(i);
}
  * will become this with this class :
import brocchinia.TrueFor.cfor
cfor(var i = 0, i < 10, i++, {
	if (i == 3) i++;
	if (i == 5) continue;
	trace(i);
});
  * who is close enough to be interisting, it's why it's implemented in Brocchinia.
  * Note that the compile code will look like this :
var i = 0;
while (i < 10) {
	if (i == 3) i++;
	if (i == 5) {
		i++;
		continue;
	}
	trace(i);
	i++;
}
  * Why a while loop ? Because there is no traditionnal for in Haxe.
  **/
abstract TrueFor(Void) {
    public static macro function cfor(init, cond, post, expr) {
    #if !display
        var func = null;
        func = function(expr:haxe.macro.Expr) {
            return switch (expr.expr) {
                case EContinue: macro { $post; $expr; }
                case EWhile(_, _, _): expr;
                case ECall(macro cfor, _): expr;
                case EFor(_): expr;
                case EIn(_): expr;
                default: haxe.macro.ExprTools.map(expr, func);
            }
        }
        expr = func(expr);
    #end
        return macro {
            $init;
            while ($cond) {
                $expr;
                $post;
            }
        };
    }
}
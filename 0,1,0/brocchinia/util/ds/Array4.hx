package brocchinia.util.ds;
/** Class Array4 from brocchinia.util.ds
  * Created by Chlorodatafile the 12/08/2015 for the first time in RVB-Project.
  * A class to manage quadridimmensionnal Array easily
  **/

private typedef A2<T> = Array<Array<T>>;
private typedef A3<T> = Array<A2<T>>;
private typedef A4<T> = Array<A3<T>>;

@:forward(iterator)
abstract Array4<T>(A4<T>) from A4<T> to A4<T> {

    public var length(get,never):Int;
    public inline function get_length():Int return this.length;

    public inline function new(w:Int,h:Int,d:Int,t:Int,def:Int->Int->Int->Int->T)
    this = [for (i in 0...w)
        [for (j in 0...h)
            [for (k in 0...d)
                [for (l in 0...t) def(i,j,k,l)]]]];

    public inline function concat(t:Array4<T>):Array4<T> return this.concat(t);

    public inline function clone():Array4<T>
    return [for (i in 0...get_length())
        [for (j in 0...this[i].length)
            [for (k in 0...this[i][j].length)
                [for (l in 0...this[i][j][k].length) this[i][j][k][l]]]]];

    public inline function copy():Array4<T> return this.copy();

    public inline function filter(f:T -> Bool):Array4<T>
    return [for (i in 0...get_length())
        [for (j in 0...this[i].length)
            [for (k in 0...this[i][j].length)
                [for (l in 0...this[i][j][k].length) if (f(this[i][j][k][l])) this[i][j][k][l]]]]];

    @:to
    public inline function toString():String
    return ""+([for (i in 0...get_length())
        [for (j in 0...this[i].length)
            [for (k in 0...this[i][j].length)
                [for (l in 0...this[i][j][k].length) this[i][j][k][l]]]]]);

    @:arrayAccess
    public inline function get(key:Int):Null<Array3<T>> return this[key];

    @:arrayAccess
    public inline function arrayWrite(key:Int, v:Array3<T>):Null<Array3<T>> return this[key]=v;
}
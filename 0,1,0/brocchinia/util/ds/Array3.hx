package brocchinia.util.ds;
/** Class Array3 from brocchinia.util.ds
  * Created by Chlorodatafile the 12/08/2015 for the first time in RVB-Project.
  * A class to manage tridimmensionnal Array easily
  **/

private typedef A2<T> = Array<Array<T>>;
private typedef A3<T> = Array<A2<T>>;

@:forward(iterator)
abstract Array3<T>(A3<T>) from A3<T> to A3<T> {

    public var length(get,never):Int;
    public inline function get_length():Int return this.length;

    public inline function new(w:Int,h:Int,d:Int,def:Int->Int->Int->T)
    this = [for (i in 0...w)
        [for (j in 0...h)
            [for (k in 0...d) def(i,j,k)]]];

    public inline function concat(t:Array3<T>):Array3<T> return this.concat(t);

    public inline function clone():Array3<T>
    return [for (i in 0...get_length())
        [for (j in 0...this[i].length)
            [for (k in 0...this[i][j].length)
                this[i][j][k]]]];

    public inline function copy():Array3<T> return this.copy();

    public inline function filter(f:T -> Bool):Array3<T>
    return [for (i in 0...get_length())
        [for (j in 0...this[i].length)
            [for (k in 0...this[i][j].length)
                if (f(this[i][j][k]))
                    this[i][j][k]]]];

    @:to
    public inline function toString():String
    return ""+( [for (i in 0...get_length())
        [for (j in 0...this[i].length)
            [for (k in 0...this[i][j].length)
                this[i][j][k]]]]);

    @:arrayAccess
    public inline function get(key:Int):Null<Array2<T>> return this[key];

    @:arrayAccess
    public inline function arrayWrite(key:Int, v:Array2<T>):Null<Array2<T>> return this[key]=v;
}
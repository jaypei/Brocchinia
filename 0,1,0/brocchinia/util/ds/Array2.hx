package brocchinia.util.ds;
/** Class Array2 from brocchinia.util.ds
  * Created by Chlorodatafile the 12/08/2015 for the first time in RVB-Project.
  * A class to manage bidimmensionnal Array easily
  **/

private typedef A2<T> = Array<Array<T>>;

@:forward(iterator)
abstract Array2<T>(A2<T>) from A2<T> to A2<T> {

    public var length(get,never):Int;
    public inline function get_length():Int return this.length;

    public inline function new(w:Int,h:Int,def:Int->Int->T)
    this = [for (i in 0...w) [for (j in 0...h) def(i,j)]];

    public inline function concat(t:Array2<T>):Array2<T> return this.concat(t);

    public inline function clone():Array2<T>
    return [for (i in 0...get_length()) [for (j in 0...this[i].length) this[i][j]]];

    public inline function copy():Array2<T> return this.copy();

    public inline function filter(f:T -> Bool):Array2<T>
    return [for (i in 0...get_length()) [for (j in 0...this[i].length) if (f(this[i][j])) this[i][j]]];

    @:to
    public inline function toString():String
    return ""+([for (i in 0...get_length()) [for (j in 0...this[i].length) this[i][j]]]);

    @:arrayAccess
    public inline function get(key:Int):Null<Array<T>> return this[key];

    @:arrayAccess
    public inline function arrayWrite(key:Int, v:Array<T>):Null<Array<T>> return this[key]=v;
}
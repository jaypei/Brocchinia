package brocchinia.util.math;
/** Class Lerp from brocchinia.util.math
  * Created by chlorodatafile the 07/07/15 for the first time in RVB-Project.
  * This class allow you to generate some LerpFunction, who take for arguments the origin,
  * the distance needed to being to the final position, and the time value, between 0 and 1, in Float.
  **/

private typedef FFFtoF = Float->Float->Float->Float;

@:callable abstract LerpFunction(FFFtoF) from FFFtoF to FFFtoF {
    public inline static var PId2:Float = 1.570796326794896;
    public inline static var PId4:Float = 0.785398163397448;

    @:arrayAccess public inline function emulate(iteration:Int):Array<Float>
        return [for (t in 0...(iteration+1)) this(0,1,t/iteration)];

    public inline function new(f:FFFtoF) this=f;

    public inline static function trigo(cos:Float=0,sin:Float=0,tan:Float=0):LerpFunction
        return new LerpFunction(switch[cos,sin,tan] {
            //k0
            case [0,0,0]:
                throw "Error : Cannot create a Trigo Function without values.";
            //k1
            case [_,0,0]:
                function(a:Float,b:Float,t:Float):Float
                {return a+b*cos*Math.cos(t*PId2);};
            case [0,_,0]:
                function(a:Float,b:Float,t:Float):Float
                {return a+b*sin*Math.sin(t*PId2);};
            case [0,0,_]:
                function(a:Float,b:Float,t:Float):Float
                {return a+b*tan*Math.tan(t*PId4);};
            //k2
            case [_,_,0]:
                function(a:Float,b:Float,t:Float):Float
                {return a+b*(cos*Math.cos(t*=PId2)+sin*Math.sin(t));};
            case [_,0,_]:
                function(a:Float,b:Float,t:Float):Float
                {return a+b*(cos*Math.cos(t*PId2)+tan*Math.tan(t*PId4));};
            case [0,_,_]:
                function(a:Float,b:Float,t:Float):Float
                {return a+b*(sin*Math.sin(t*PId2)+tan*Math.tan(t*PId4));};
            //k3
            case [_,_,_]:
                function(a:Float,b:Float,t:Float):Float
                {return a+b*(tan*Math.tan(t*PId4)+cos*Math.cos(t*=PId2)+sin*Math.sin(t));};
        });

    public inline static function ease(c1:Float=0,c2:Float=0,c3:Float=0,c4:Float=0,c5:Float=0):LerpFunction
        return new LerpFunction(switch[c1,c2,c3,c4,c5] {
            //k0
            case [0,0,0,0,0]:
                throw "Error : Cannot create a Ease Function without values.";
            //k1
            case [_,0,0,0,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*c1;};
            case [0,_,0,0,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*t*c2;};
            case [0,0,_,0,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*t*t*c3;};
            case [0,0,0,_,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*(t*=t)*t*c4;};
            case [0,0,0,0,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(t*=t)*t*c5;};
            //k2
            case [_,_,0,0,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(c1+c2*t);};
            case [_,0,_,0,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(c1+t*t*c3);};
            case [0,_,_,0,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*t*(c2+t*c3);};
            case [_,0,0,_,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*(t*c1+(t*=t)*t*c4);};
            case [0,_,0,_,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*(t*=t)*(c2+t*c4);};
            case [0,0,_,_,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*(t*(t*=t)*c3+t*t*c4);};
            case [_,0,0,0,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(c1+(t*=t)*t*c5);};
            case [0,_,0,0,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*(t*(t*=t)*t*c5+t*c2);};
            case [0,0,_,0,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(t*=t)*(c3+t*c5);};
            case [0,0,0,_,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*(t*(t*=(t*=t))*c5+t*c4);};
            //k3
            case [_,_,_,0,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(c1+t*(c2+t*c3));};
            case [_,_,0,_,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(c1+t*(c2=t*t*c4));};
            case [_,0,_,_,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(c1+t*t*(c3+t*c4));};
            case [0,_,_,_,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*t*(c2+t*(c3+t*c4));};
            case [_,_,0,0,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(c1+t*(c2+t*t*t*c5));};
            case [_,0,_,0,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(c1+(t*=t)*(c3+t*c5));};
            case [0,_,_,0,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*(t*((t*=t)*(c3+t*c5))+t*c2);};
            case [_,0,0,_,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*(t*(c1+(t*=(t*=t))*c5)+t*c4);};
            case [0,_,0,_,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*(t*(t*=t)*t*c5+t*(c2+t*c4));};
            case [0,0,_,_,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*t*t*(c3+t*(c4+t*c5));};
            //k4
            case [_,_,_,_,0]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(c1+t*(c2+t*(c3+t*c4)));};
            case [_,_,_,0,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(c1+t*(c2+t*(c3+t*t*c5)));};
            case [_,_,0,_,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(c1+t*(c2+t*t*(c4+c5*t)));};
            case [_,0,_,_,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(c1+t*t*(c3+t*(c4+c5*t)));};
            case [0,_,_,_,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*t*(c2+t*(c3+t*(c4+t*c5)));};
            //k5
            case [_,_,_,_,_]:
                function(a:Float,b:Float,t:Float):Float
                    {return a+b*t*(c1+t*(c2+t*(c3+t*(c4+t*c5))));};
        });
}

class Ease extends Lerp {
    public static var inQuintic:LerpFunction = LerpFunction.ease(0,0,0,0,1);
    public static var inQuartic:LerpFunction = LerpFunction.ease(0,0,0,1);
    public static var inCubic:LerpFunction = LerpFunction.ease(0,0,1);
    public static var inQuadratic:LerpFunction = LerpFunction.ease(0,1);
    public static var inOutCubic:LerpFunction = LerpFunction.ease(0,3,-2);
    public static var inOutQuintic:LerpFunction = LerpFunction.ease(0,0,10,-15,6);
    public static var outQuintic:LerpFunction = LerpFunction.ease(5,-10,10,-5,1);
    public static var outQuartic:LerpFunction = LerpFunction.ease(4,-6,4,-1);
    public static var outCubic:LerpFunction = LerpFunction.ease(3,-3,-1);
    public static var outInCubic:LerpFunction = LerpFunction.ease(3,-6,4);
    public static var outInQuartic:LerpFunction = LerpFunction.ease(4,-9,6);
    public static var backInCubic:LerpFunction = LerpFunction.ease(0,-3,4);
    public static var backInQuartic:LerpFunction = LerpFunction.ease(0,-3,2,2);
    public static var outBackCubic:LerpFunction = LerpFunction.ease(6,-9,4);
    public static var outBackQuartic:LerpFunction = LerpFunction.ease(8,-15,10,-2);
    public static var outSmallElastic:LerpFunction = LerpFunction.ease(15,-67,126,-106,33);
    public static var outBigElastic:LerpFunction = LerpFunction.ease(20,-100,200,-175,56);
    public static var inSmallElastic:LerpFunction = LerpFunction.ease(0,-5,32,-59,33);
    public static var inBigElastic:LerpFunction = LerpFunction.ease(0,-10,60,-105,56);
}

class Lerp {
    public static var lerp:LerpFunction = LerpFunction.ease(1);
    public static var cos:LerpFunction = LerpFunction.trigo(1);
    public static var sin:LerpFunction = LerpFunction.trigo(0,1);
    public static var tan:LerpFunction = LerpFunction.trigo(0,0,1);
    public static var sqrt:LerpFunction = new LerpFunction(
        function(a:Float,b:Float,t:Float):Float
            {return a+b*Math.sqrt(t);});
    public static var sinElastic:LerpFunction = new LerpFunction(
        function(a:Float,b:Float,t:Float):Float
            {return a+b*Math.sin(t*7.853981633974483);});
}
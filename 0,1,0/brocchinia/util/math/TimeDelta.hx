package brocchinia.util.math;
/** Class TimeDelta from brocchinia.util.math
  * Created by chlorodatafile the 06/07/15 for the first time in RVB-Project.
  * TimeDelta give a Delta between the given time and now. It's expressed in Milliseconds.
  **/
import brocchinia.Time;

abstract TimeDelta(Int) from Int to Int {

    public inline function new(delta:Int) this=(Time.now-delta);

    @:to inline public function toString():String return "TimeDelta: "+this;
    @:to public function toInt():Int return this;

    @:op(A%B) @:noCompletion public inline function coefficientedBy(v:Float):Float return Math.max(0,Math.min(1,this/v));
    @:op(-A) @:noCompletion public inline function neg():Int return -this;

    @:op(A==B) @:noCompletion @:commutative static public inline function equal(a:TimeDelta,v:Int):Bool return a.toInt()==v;
    @:op(A!=B) @:noCompletion @:commutative static public inline function notEqual(a:TimeDelta,v:Int):Bool return a.toInt()!=v;

    @:op(A+B) @:noCompletion @:commutative static public inline function add(a:TimeDelta,v:Int):Int return a.toInt()+v;
    @:op(A*B) @:noCompletion @:commutative static public inline function mul(a:TimeDelta,v:Int):Int return a.toInt()*v;

    @:op(A-B) @:noCompletion static public inline function subB(v:Int,a:TimeDelta):Int return v-a.toInt();
    @:op(A/B) @:noCompletion static public inline function divB(v:Int,a:TimeDelta):Float return v/a.toInt();

    @:op(A>=B) @:noCompletion static public inline function supEqualB(v:Int,a:TimeDelta):Bool return v>=a.toInt();
    @:op(A<=B) @:noCompletion static public inline function infEqualB(v:Int,a:TimeDelta):Bool return v<=a.toInt();
    @:op(A>B) @:noCompletion static public inline function supB(v:Int,a:TimeDelta):Bool return v>a.toInt();
    @:op(A<B) @:noCompletion static public inline function infB(v:Int,a:TimeDelta):Bool return v<a.toInt();

    @:op(A-B) @:noCompletion static public inline function sub(a:TimeDelta,v:Int):Int return a.toInt()-v;
    @:op(A/B) @:noCompletion static public inline function div(a:TimeDelta,v:Int):Float return a.toInt()/v;

    @:op(A>=B) @:noCompletion static public inline function supEqual(a:TimeDelta,v:Int):Bool return a.toInt()>=v;
    @:op(A<=B) @:noCompletion static public inline function infEqual(a:TimeDelta,v:Int):Bool return a.toInt()<=v;
    @:op(A>B) @:noCompletion static public inline function sup(a:TimeDelta,v:Int):Bool return a.toInt()>v;
    @:op(A<B) @:noCompletion static public inline function inf(a:TimeDelta,v:Int):Bool return a.toInt()<v;
}
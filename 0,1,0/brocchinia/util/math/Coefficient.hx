package brocchinia.util.math;
/** Class Coefficient from brocchinia.util.math
  * Created by Chlorodatafile the 26/07/2015 for the first time in RVB-Project.
  *
  **/

abstract Coefficient(Float) from Float to Float {

    public inline function new(v:Float) this = v;
    public inline static function make(v:Float,a:Int=1):Coefficient return new Coefficient(a/v);
    @:to public inline function toFloat():Float return this;
    @:to public inline function toString():String return "Coefficient : "+this;

    @:op(A*B) @commutative public static inline function mult(a:Coefficient,b:Float):Float return a.toFloat()*b;
    @:op(A/B) public static inline function divA(a:Coefficient,b:Float):Float return a.toFloat()/b;
    @:op(A/B) public static inline function divB(b:Float,a:Coefficient):Float return b/a.toFloat();

    @:op(A+B) @commutative public static inline function add(a:Coefficient,b:Float):Float return a.toFloat()+b;
    @:op(A-B) public static inline function subA(a:Coefficient,b:Float):Float return a.toFloat()-b;
    @:op(A-B) public static inline function subB(b:Float,a:Coefficient):Float return b-a.toFloat();

    @:op(-A) public inline function neg():Coefficient return new Coefficient(-this);

}
package brocchinia.util.math;
/** Class Radian from brocchinia.util.math
  * Created by Chlorodatafile the 25/07/2015 for the first time in RVB-Project.
  * Working with Degree, this class help you to use the angle.
  **/

abstract Radian(Float) from Float to Float {

    public inline static var PI2:Float   = 6.283185307179586;
    public inline static var PI:Float    = 3.141592653589793;
    public inline static var PId2:Float  = 1.570796326794896;
    public inline static var PId4:Float  = 0.785398163397448;
    public inline static var PId8:Float  = 0.392699081698724;
    public inline static var PId16:Float = 0.196349540849362;

    public inline function new(v:Float=0) this = v;

    @:from public static inline function fromFloatDegree(v:Degree<Float>):Radian return new Radian(v/180*Math.PI);
    @:from public static inline function fromIntDegree(v:Degree<Int>):Radian return new Radian(v/180*Math.PI);
    @:to public inline function toDegree():Degree<Float> return new Degree<Float>(this/Math.PI*180);
    @:to public inline function toFloat():Float return this;

    @:op(A++) @:noCompletion public inline function incr():Float return this+=PI2;
    @:op(A--) @:noCompletion public inline function decr():Float return this-=PI2;
    @:op(-A) @:noCompletion public inline function neg():Float return -this;
    @:op(A%B) @:noCompletion public inline function circleRounded(nCircle:Int):Float return this%(nCircle*Math.PI);

    @:op(A==B) @:noCompletion @:commutative static public inline function equalDegreeFloat(a:Radian,v:Degree<Float>):Bool return a.toFloat()==v.toRadian().toFloat();
    @:op(A!=B) @:noCompletion @:commutative static public inline function notEqualDegreeFloat(a:Radian,v:Degree<Float>):Bool return a.toFloat()!=v.toRadian().toFloat();
    @:op(A+B) @:noCompletion @:commutative static public inline function addDegreeFloat(a:Radian,v:Degree<Float>):Float return a.toFloat()+v.toRadian().toFloat();
    @:op(A*B) @:noCompletion @:commutative static public inline function mulDegreeFloat(a:Radian,v:Degree<Float>):Float return a.toFloat()*v.toRadian().toFloat();
    @:op(A>=B) @:noCompletion static public inline function supEqualDegreeFloat(a:Radian,v:Degree<Float>):Bool return a.toFloat()>=v.toRadian().toFloat();
    @:op(A<=B) @:noCompletion static public inline function infEqualDegreeFloat(a:Radian,v:Degree<Float>):Bool return a.toFloat()<=v.toRadian().toFloat();
    @:op(A>B) @:noCompletion static public inline function supDegreeFloat(a:Radian,v:Degree<Float>):Bool return a.toFloat()>v.toRadian().toFloat();
    @:op(A<B) @:noCompletion static public inline function infDegreeFloat(a:Radian,v:Degree<Float>):Bool return a.toFloat()<v.toRadian().toFloat();
    @:op(A-B) @:noCompletion static public inline function subDegreeFloat(a:Radian,v:Degree<Float>):Float return a.toFloat()-v.toRadian().toFloat();
    @:op(A/B) @:noCompletion static public inline function divDegreeFloat(a:Radian,v:Degree<Float>):Float return a.toFloat()/v.toRadian().toFloat();
    @:op(A>=B) @:noCompletion static public inline function supEqualDegreeFloatB(v:Degree<Float>,a:Radian):Bool return v.toRadian().toFloat()>=a.toFloat();
    @:op(A<=B) @:noCompletion static public inline function infEqualDegreeFloatB(v:Degree<Float>,a:Radian):Bool return v.toRadian().toFloat()<=a.toFloat();
    @:op(A>B) @:noCompletion static public inline function supDegreeFloatB(v:Degree<Float>,a:Radian):Bool return v.toRadian().toFloat()>a.toFloat();
    @:op(A<B) @:noCompletion static public inline function infDegreeFloatB(v:Degree<Float>,a:Radian):Bool return v.toRadian().toFloat()<a.toFloat();
    @:op(A-B) @:noCompletion static public inline function subDegreeFloatB(v:Degree<Float>,a:Radian):Float return v.toRadian().toFloat()-a.toFloat();
    @:op(A/B) @:noCompletion static public inline function divDegreeFloatB(v:Degree<Float>,a:Radian):Float return v.toRadian().toFloat()/a.toFloat();

    @:op(A==B) @:noCompletion @:commutative static public inline function equalDegreeInt(a:Radian,v:Degree<Int>):Bool return a.toFloat()==v.toRadian().toFloat();
    @:op(A!=B) @:noCompletion @:commutative static public inline function notEqualDegreeInt(a:Radian,v:Degree<Int>):Bool return a.toFloat()!=v.toRadian().toFloat();
    @:op(A+B) @:noCompletion @:commutative static public inline function addDegreeInt(a:Radian,v:Degree<Int>):Float return a.toFloat()+v.toRadian().toFloat();
    @:op(A*B) @:noCompletion @:commutative static public inline function mulDegreeInt(a:Radian,v:Degree<Int>):Float return a.toFloat()*v.toRadian().toFloat();
    @:op(A>=B) @:noCompletion static public inline function supEqualDegreeInt(a:Radian,v:Degree<Int>):Bool return a.toFloat()>=v.toRadian().toFloat();
    @:op(A<=B) @:noCompletion static public inline function infEqualDegreeInt(a:Radian,v:Degree<Int>):Bool return a.toFloat()<=v.toRadian().toFloat();
    @:op(A>B) @:noCompletion static public inline function supDegreeInt(a:Radian,v:Degree<Int>):Bool return a.toFloat()>v.toRadian().toFloat();
    @:op(A<B) @:noCompletion static public inline function infDegreeInt(a:Radian,v:Degree<Int>):Bool return a.toFloat()<v.toRadian().toFloat();
    @:op(A-B) @:noCompletion static public inline function subDegreeInt(a:Radian,v:Degree<Int>):Float return a.toFloat()-v.toRadian().toFloat();
    @:op(A/B) @:noCompletion static public inline function divDegreeInt(a:Radian,v:Degree<Int>):Float return a.toFloat()/v.toRadian().toFloat();
    @:op(A>=B) @:noCompletion static public inline function supEqualDegreeIntB(v:Degree<Int>,a:Radian):Bool return v.toRadian().toFloat()>=a.toFloat();
    @:op(A<=B) @:noCompletion static public inline function infEqualDegreeIntB(v:Degree<Int>,a:Radian):Bool return v.toRadian().toFloat()<=a.toFloat();
    @:op(A>B) @:noCompletion static public inline function supDegreeIntB(v:Degree<Int>,a:Radian):Bool return v.toRadian().toFloat()>a.toFloat();
    @:op(A<B) @:noCompletion static public inline function infDegreeIntB(v:Degree<Int>,a:Radian):Bool return v.toRadian().toFloat()<a.toFloat();
    @:op(A-B) @:noCompletion static public inline function subDegreeIntB(v:Degree<Int>,a:Radian):Float return v.toRadian().toFloat()-a.toFloat();
    @:op(A/B) @:noCompletion static public inline function divDegreeIntB(v:Degree<Int>,a:Radian):Float return v.toRadian().toFloat()/a.toFloat();

    @:op(A==B) @:noCompletion @:commutative static public inline function equal(a:Radian,v:Float):Bool return a.toFloat()==(v*Math.PI);
    @:op(A!=B) @:noCompletion @:commutative static public inline function notEqual(a:Radian,v:Float):Bool return a.toFloat()!=(v*Math.PI);
    @:op(A+B) @:noCompletion @:commutative static public inline function add(a:Radian,v:Float):Float return a.toFloat()+(v*Math.PI);
    @:op(A*B) @:noCompletion @:commutative static public inline function mul(a:Radian,v:Float):Float return a.toFloat()*(v*Math.PI);
    @:op(A>=B) @:noCompletion static public inline function supEqual(a:Radian,v:Float):Bool return a.toFloat()>=(v*Math.PI);
    @:op(A<=B) @:noCompletion static public inline function infEqual(a:Radian,v:Float):Bool return a.toFloat()<=(v*Math.PI);
    @:op(A>B) @:noCompletion static public inline function sup(a:Radian,v:Float):Bool return a.toFloat()>(v*Math.PI);
    @:op(A<B) @:noCompletion static public inline function inf(a:Radian,v:Float):Bool return a.toFloat()<(v*Math.PI);
    @:op(A-B) @:noCompletion static public inline function sub(a:Radian,v:Float):Float return a.toFloat()-(v*Math.PI);
    @:op(A/B) @:noCompletion static public inline function div(a:Radian,v:Float):Float return a.toFloat()/(v*Math.PI);
    @:op(A>=B) @:noCompletion static public inline function supEqualB(v:Float,a:Radian):Bool return (v*Math.PI)>=a.toFloat();
    @:op(A<=B) @:noCompletion static public inline function infEqualB(v:Float,a:Radian):Bool return (v*Math.PI)<=a.toFloat();
    @:op(A>B) @:noCompletion static public inline function supB(v:Float,a:Radian):Bool return (v*Math.PI)>a.toFloat();
    @:op(A<B) @:noCompletion static public inline function infB(v:Float,a:Radian):Bool return (v*Math.PI)<a.toFloat();
    @:op(A-B) @:noCompletion static public inline function subB(v:Float,a:Radian):Float return (v*Math.PI)-a.toFloat();
    @:op(A/B) @:noCompletion static public inline function divB(v:Float,a:Radian):Float return (v*Math.PI)/a.toFloat();

    @:op(A==B) @:noCompletion public inline function equalRadian(v:Radian):Bool return this==v;
    @:op(A!=B) @:noCompletion public inline function notEqualRadian(v:Radian):Bool return this!=v;
    @:op(A>=B) @:noCompletion public inline function supEqualRadian(v:Radian):Bool return this>=v;
    @:op(A<=B) @:noCompletion public inline function infEqualRadian(v:Radian):Bool return this<=v;
    @:op(A>B) @:noCompletion public inline function supRadian(v:Radian):Bool return this>v;
    @:op(A<B) @:noCompletion public inline function infRadian(v:Radian):Bool return this<v;
    @:op(A+B) @:noCompletion public inline function addRadian(v:Radian):Float return this+v;
    @:op(A-B) @:noCompletion public inline function subRadian(v:Radian):Float return this-v;
    @:op(A/B) @:noCompletion public inline function divRadian(v:Radian):Float return this/v;
    @:op(A*B) @:noCompletion public inline function mulRadian(v:Radian):Float return this*v;

    @:op(A+=B) @:noCompletion public inline function addSet(v:Float):Float return this+=v;
    @:op(A-=B) @:noCompletion public inline function subSet(v:Float):Float return this-=v;
    @:op(A/=B) @:noCompletion public inline function divSet(v:Float):Float return this/=v;
    @:op(A*=B) @:noCompletion public inline function mulSet(v:Float):Float return this*=v;

    @:op(A+=B) @:noCompletion public inline function addSetR(v:Radian):Float return this+=v;
    @:op(A-=B) @:noCompletion public inline function subSetR(v:Radian):Float return this-=v;
    @:op(A/=B) @:noCompletion public inline function divSetR(v:Radian):Float return this/=v;
    @:op(A*=B) @:noCompletion public inline function mulSetR(v:Radian):Float return this*=v;

    @:op(A+=B) @:noCompletion public inline function addSetDF(v:Degree<Float>):Float return this+=v.toRadian();
    @:op(A-=B) @:noCompletion public inline function subSetDF(v:Degree<Float>):Float return this-=v.toRadian();
    @:op(A/=B) @:noCompletion public inline function divSetDF(v:Degree<Float>):Float return this/=v.toRadian();
    @:op(A*=B) @:noCompletion public inline function mulSetDF(v:Degree<Float>):Float return this*=v.toRadian();

    @:op(A+=B) @:noCompletion public inline function addSetDI(v:Degree<Int>):Float return this+=v.toRadian();
    @:op(A-=B) @:noCompletion public inline function subSetDI(v:Degree<Int>):Float return this-=v.toRadian();
    @:op(A/=B) @:noCompletion public inline function divSetDI(v:Degree<Int>):Float return this/=v.toRadian();
    @:op(A*=B) @:noCompletion public inline function mulSetDI(v:Degree<Int>):Float return this*=v.toRadian();
}
package brocchinia.util.math;
/** Class Percent from brocchinia.util.math
  * Created by Chlorodatafile the 26/07/2015 for the first time in RVB-Project.
  *
  **/

abstract Percent(Float) {

    public inline function new(v:Float) this = v;
    @:to public inline function toFloat():Float return this;
    @:to public inline function toString():String return "1%="+this;

    @:op(A*B) @commutative public static inline function mult(a:Percent,b:Float):Float return a.toFloat()*b;
    @:op(A/B) public static inline function divA(a:Percent,b:Float):Float return a.toFloat()/b;
    @:op(A/B) public static inline function divB(b:Float,a:Percent):Float return b/a.toFloat();

    @:op(A+B) @commutative public static inline function add(a:Percent,b:Float):Float return a.toFloat()+b;
    @:op(A-B) public static inline function subA(a:Percent,b:Float):Float return a.toFloat()-b;
    @:op(A-B) public static inline function subB(b:Float,a:Percent):Float return b-a.toFloat();

    @:op(-A) public inline function neg():Percent return new Percent(-this);

}
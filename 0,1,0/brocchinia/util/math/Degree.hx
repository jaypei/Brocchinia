package brocchinia.util.math;
/** Class Degree from brocchinia.util.math
  * Created by Chlorodatafile the 13/07/2015 for the first time in RVB-Project.
  * An abstract class to help you to use the angle.
  **/

abstract Degree<T:Float>(T) from T to T {

    private inline function toType():T return this;

    public inline function new(v:T) this = v;

    @:from public static inline function fromRadian(v:Radian):Degree<Float> return new Degree<Float>(v/Math.PI*180);
    @:from public static inline function fromFloat(v:Float):Degree<Float> return new Degree<Float>(v);
    @:from public static inline function fromInt(v:Int):Degree<Int> return new Degree<Int>(v);
    @:to public static inline function toFloatDegree(v:Degree<Int>):Degree<Float> return new Degree<Float>(v);
    @:to public static inline function toIntDegree(v:Degree<Float>):Degree<Int> return new Degree<Int>(v);
    @:to public inline function toRadian():Radian return new Radian(this/180*Math.PI);
    @:to public inline function toFloat():Float return this;
    @:to public inline function toInt():Int return cast this;

    @:op(A++) @:noCompletion public inline function incr():T return this+=360;
    @:op(A--) @:noCompletion public inline function decr():T return this-=360;
    @:op(-A) @:noCompletion public inline function neg():T return -this;
    @:op(A%B) @:noCompletion public inline function circleRounded(nCircle:Int):T return this%(nCircle*180);

    @:op(A==B) @:noCompletion public inline function equalDegree(v:Degree<T>):Bool return this==v.toType();
    @:op(A!=B) @:noCompletion public inline function notEqualDegree(v:Degree<T>):Bool return this!=v.toType();
    @:op(A>=B) @:noCompletion public inline function supEqualDegree(v:Degree<T>):Bool return this>=v.toType();
    @:op(A<=B) @:noCompletion public inline function infEqualDegree(v:Degree<T>):Bool return this<=v.toType();
    @:op(A>B) @:noCompletion public inline function supDegree(v:Degree<T>):Bool return this>v.toType();
    @:op(A<B) @:noCompletion public inline function infDegree(v:Degree<T>):Bool return this<v.toType();
    @:op(A+B) @:noCompletion public inline function addDegree(v:Degree<T>):Degree<T> return this+v.toType();
    @:op(A-B) @:noCompletion public inline function subDegree(v:Degree<T>):Degree<T> return this-v.toType();
    @:op(A/B) @:noCompletion public inline function divDegree(v:Degree<T>):Degree<Float> return this/v.toType();
    @:op(A*B) @:noCompletion public inline function mulDegree(v:Degree<T>):Degree<T> return this*v.toType();

    @:op(A==B) @:noCompletion public inline function equalDegreeRadian(v:Radian):Bool return toRadian().toFloat()==v.toFloat();
    @:op(A!=B) @:noCompletion public inline function notEqualDegreeRadian(v:Radian):Bool return toRadian().toFloat()!=v.toFloat();
    @:op(A>=B) @:noCompletion public inline function supEqualDegreeRadian(v:Radian):Bool return toRadian().toFloat()>=v.toFloat();
    @:op(A<=B) @:noCompletion public inline function infEqualDegreeRadian(v:Radian):Bool return toRadian().toFloat()<=v.toFloat();
    @:op(A>B) @:noCompletion public inline function supDegreeRadian(v:Radian):Bool return toRadian().toFloat()>v.toFloat();
    @:op(A<B) @:noCompletion public inline function infDegreeRadian(v:Radian):Bool return toRadian().toFloat()<v.toFloat();
    @:op(A+B) @:noCompletion public inline function addDegreeRadian(v:Radian):Float return toRadian().toFloat()+v.toFloat();
    @:op(A-B) @:noCompletion public inline function subDegreeRadian(v:Radian):Float return toRadian().toFloat()-v.toFloat();
    @:op(A/B) @:noCompletion public inline function divDegreeRadian(v:Radian):Float return toRadian().toFloat()/v.toFloat();
    @:op(A*B) @:noCompletion public inline function mulDegreeRadian(v:Radian):Float return toRadian().toFloat()*v.toFloat();

    @:op(A==B) @:noCompletion @:commutative static public inline function equalIntForDegreeFloat(a:Degree<Float>,v:Int):Bool return v==a.toInt();
    @:op(A!=B) @:noCompletion @:commutative static public inline function notEqualIntForDegreeFloat(a:Degree<Float>,v:Int):Bool return v!=a.toInt();
    @:op(A+B) @:noCompletion @:commutative static public inline function addIntForDegreeFloat(a:Degree<Float>,v:Int):Int return a.toInt()+v;
    @:op(A*B) @:noCompletion @:commutative static public inline function mulIntForDegreeFloat(a:Degree<Float>,v:Int):Int return a.toInt()*v;
    @:op(A>=B) @:noCompletion static public inline function supEqualIntForDegreeFloat(a:Degree<Float>,v:Int):Bool return a.toInt()>=v;
    @:op(A<=B) @:noCompletion static public inline function infEqualIntForDegreeFloat(a:Degree<Float>,v:Int):Bool return a.toInt()<=v;
    @:op(A>B) @:noCompletion static public inline function supIntForDegreeFloat(a:Degree<Float>,v:Int):Bool return a.toInt()>v;
    @:op(A<B) @:noCompletion static public inline function infIntForDegreeFloat(a:Degree<Float>,v:Int):Bool return a.toInt()<v;
    @:op(A-B) @:noCompletion static public inline function subIntForDegreeFloat(a:Degree<Float>,v:Int):Int return a.toInt()-v;
    @:op(A/B) @:noCompletion static public inline function divIntForDegreeFloat(a:Degree<Float>,v:Int):Float return a.toInt()/v;
    @:op(A>=B) @:noCompletion static public inline function supEqualIntForDegreeFloatB(v:Int,a:Degree<Float>):Bool return a.toInt()<=v;
    @:op(A<=B) @:noCompletion static public inline function infEqualIntForDegreeFloatB(v:Int,a:Degree<Float>):Bool return a.toInt()>=v;
    @:op(A>B) @:noCompletion static public inline function supIntForDegreeFloatB(v:Int,a:Degree<Float>):Bool return a.toInt()<v;
    @:op(A<B) @:noCompletion static public inline function infIntForDegreeFloatB(v:Int,a:Degree<Float>):Bool return a.toInt()>v;
    @:op(A-B) @:noCompletion static public inline function subIntForDegreeFloatB(v:Int,a:Degree<Float>):Int return v-a.toInt();
    @:op(A/B) @:noCompletion static public inline function divIntForDegreeFloatB(v:Int,a:Degree<Float>):Float return v/a.toInt();

    @:op(A==B) @:noCompletion @:commutative static public inline function equalFloatForDegreeFloat(a:Degree<Float>,v:Float):Bool return v==a.toFloat();
    @:op(A!=B) @:noCompletion @:commutative static public inline function notEqualFloatForDegreeFloat(a:Degree<Float>,v:Float):Bool return v!=a.toFloat();
    @:op(A+B) @:noCompletion @:commutative static public inline function addForDegreeFloat(a:Degree<Float>,v:Float):Float return a.toFloat()+v;
    @:op(A*B) @:noCompletion @:commutative static public inline function mulForDegreeFloat(a:Degree<Float>,v:Float):Float return a.toFloat()*v;
    @:op(A>=B) @:noCompletion static public inline function supEqualForDegreeFloat(a:Degree<Float>,v:Float):Bool return a.toFloat()>=v;
    @:op(A<=B) @:noCompletion static public inline function infEqualForDegreeFloat(a:Degree<Float>,v:Float):Bool return a.toFloat()<=v;
    @:op(A>B) @:noCompletion static public inline function supForDegreeFloat(a:Degree<Float>,v:Float):Bool return a.toFloat()>v;
    @:op(A<B) @:noCompletion static public inline function infForDegreeFloat(a:Degree<Float>,v:Float):Bool return a.toFloat()<v;
    @:op(A-B) @:noCompletion static public inline function subForDegreeFloat(a:Degree<Float>,v:Float):Float return a.toFloat()-v;
    @:op(A/B) @:noCompletion static public inline function divForDegreeFloat(a:Degree<Float>,v:Float):Float return a.toFloat()/v;
    @:op(A>=B) @:noCompletion static public inline function supEqualForDegreeFloatB(v:Float,a:Degree<Float>):Bool return v>=a.toFloat();
    @:op(A<=B) @:noCompletion static public inline function infEqualForDegreeFloatB(v:Float,a:Degree<Float>):Bool return v<=a.toFloat();
    @:op(A>B) @:noCompletion static public inline function supForDegreeFloatB(v:Float,a:Degree<Float>):Bool return v>a.toFloat();
    @:op(A<B) @:noCompletion static public inline function infForDegreeFloatB(v:Float,a:Degree<Float>):Bool return v<a.toFloat();
    @:op(A-B) @:noCompletion static public inline function subForDegreeFloatB(v:Float,a:Degree<Float>):Float return v-a.toFloat();
    @:op(A/B) @:noCompletion static public inline function divForDegreeFloatB(v:Float,a:Degree<Float>):Float return v/a.toFloat();

    @:op(A==B) @:noCompletion @:commutative static public inline function equalIntForDegreeInt(a:Degree<Int>,v:Int):Bool return v==a.toInt();
    @:op(A!=B) @:noCompletion @:commutative static public inline function notEqualIntForDegreeInt(a:Degree<Int>,v:Int):Bool return v!=a.toInt();
    @:op(A+B) @:noCompletion @:commutative static public inline function addInt(a:Degree<Int>,v:Int):Int return a.toInt()+v;
    @:op(A*B) @:noCompletion @:commutative static public inline function mulInt(a:Degree<Int>,v:Int):Int return a.toInt()*v;
    @:op(A>=B) @:noCompletion static public inline function supEqualInt(a:Degree<Int>,v:Int):Bool return a.toInt()>=v;
    @:op(A<=B) @:noCompletion static public inline function infEqualInt(a:Degree<Int>,v:Int):Bool return a.toInt()<=v;
    @:op(A>B) @:noCompletion static public inline function supInt(a:Degree<Int>,v:Int):Bool return a.toInt()>v;
    @:op(A<B) @:noCompletion static public inline function infInt(a:Degree<Int>,v:Int):Bool return a.toInt()<v;
    @:op(A-B) @:noCompletion static public inline function subInt(a:Degree<Int>,v:Int):Int return a.toInt()-v;
    @:op(A/B) @:noCompletion static public inline function divInt(a:Degree<Int>,v:Int):Float return a.toInt()/v;
    @:op(A>=B) @:noCompletion static public inline function supEqualIntB(v:Int,a:Degree<Int>):Bool return v>=a.toInt();
    @:op(A<=B) @:noCompletion static public inline function infEqualIntB(v:Int,a:Degree<Int>):Bool return v<=a.toInt();
    @:op(A>B) @:noCompletion static public inline function supIntB(v:Int,a:Degree<Int>):Bool return v>a.toInt();
    @:op(A<B) @:noCompletion static public inline function infIntB(v:Int,a:Degree<Int>):Bool return v<a.toInt();
    @:op(A-B) @:noCompletion static public inline function subIntB(v:Int,a:Degree<Int>):Int return v-a.toInt();
    @:op(A/B) @:noCompletion static public inline function divIntB(v:Int,a:Degree<Int>):Float return v/a.toInt();

    @:op(A==B) @:noCompletion @:commutative static public inline function equalFloatForDegreeInt(a:Degree<Int>,v:Float):Bool return v==a.toFloat();
    @:op(A!=B) @:noCompletion @:commutative static public inline function notEqualFloatForDegreeInt(a:Degree<Int>,v:Float):Bool return v!=a.toFloat();
    @:op(A+B) @:noCompletion @:commutative static public inline function add(a:Degree<Int>,v:Float):Float return a.toFloat()+v;
    @:op(A*B) @:noCompletion @:commutative static public inline function mul(a:Degree<Int>,v:Float):Float return a.toFloat()*v;
    @:op(A>=B) @:noCompletion static public inline function supEqual(a:Degree<Int>,v:Float):Bool return a.toFloat()>=v;
    @:op(A<=B) @:noCompletion static public inline function infEqual(a:Degree<Int>,v:Float):Bool return a.toFloat()<=v;
    @:op(A>B) @:noCompletion static public inline function sup(a:Degree<Int>,v:Float):Bool return a.toFloat()>v;
    @:op(A<B) @:noCompletion static public inline function inf(a:Degree<Int>,v:Float):Bool return a.toFloat()<v;
    @:op(A-B) @:noCompletion static public inline function sub(a:Degree<Int>,v:Float):Float return a.toFloat()-v;
    @:op(A/B) @:noCompletion static public inline function div(a:Degree<Int>,v:Float):Float return a.toFloat()/v;
    @:op(A>=B) @:noCompletion static public inline function supEqualB(v:Float,a:Degree<Int>):Bool return v>=a.toFloat();
    @:op(A<=B) @:noCompletion static public inline function infEqualB(v:Float,a:Degree<Int>):Bool return v<=a.toFloat();
    @:op(A>B) @:noCompletion static public inline function supB(v:Float,a:Degree<Int>):Bool return v>a.toFloat();
    @:op(A<B) @:noCompletion static public inline function infB(v:Float,a:Degree<Int>):Bool return v<a.toFloat();
    @:op(A-B) @:noCompletion static public inline function subB(v:Float,a:Degree<Int>):Float return v-a.toFloat();
    @:op(A/B) @:noCompletion static public inline function divB(v:Float,a:Degree<Int>):Float return v/a.toFloat();

    @:op(A+=B) @:noCompletion public inline function addSetInt(v:Int):T return this=cast (this+v);
    @:op(A-=B) @:noCompletion public inline function subSetInt(v:Int):T return this=cast (this-v);
    @:op(A/=B) @:noCompletion public inline function divSetInt(v:Int):T return this=cast (this/v);
    @:op(A*=B) @:noCompletion public inline function mulSetInt(v:Int):T return this=cast (this*v);

    @:op(A+=B) @:noCompletion public inline function addSet(v:Float):T return this=cast (this+v);
    @:op(A-=B) @:noCompletion public inline function subSet(v:Float):T return this=cast (this-v);
    @:op(A/=B) @:noCompletion public inline function divSet(v:Float):T return this=cast (this/v);
    @:op(A*=B) @:noCompletion public inline function mulSet(v:Float):T return this=cast (this*v);

    @:op(A+=B) @:noCompletion public inline function addSetDF(v:Degree<Float>):T return this=cast (this+v.toFloat());
    @:op(A-=B) @:noCompletion public inline function subSetDF(v:Degree<Float>):T return this=cast (this-v.toFloat());
    @:op(A/=B) @:noCompletion public inline function divSetDF(v:Degree<Float>):T return this=cast (this/v.toFloat());
    @:op(A*=B) @:noCompletion public inline function mulSetDF(v:Degree<Float>):T return this=cast (this*v.toFloat());

    @:op(A+=B) @:noCompletion public inline function addSetDI(v:Degree<Int>):T return this=cast (this+v.toInt());
    @:op(A-=B) @:noCompletion public inline function subSetDI(v:Degree<Int>):T return this=cast (this-v.toInt());
    @:op(A/=B) @:noCompletion public inline function divSetDI(v:Degree<Int>):T return this=cast (this/v.toInt());
    @:op(A*=B) @:noCompletion public inline function mulSetDI(v:Degree<Int>):T return this=cast (this*v.toInt());
}
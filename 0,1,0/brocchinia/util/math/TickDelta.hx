package brocchinia.util.math;
/** Class TickDelta from brocchinia.util.math
  * Created by chlorodatafile the 06/07/15 for the first time in RVB-Project.
  *
  **/
import brocchinia.Vector;

abstract TickDelta(Vector<UInt>) {
    public var max(get,set):UInt;
    @:noCompletion public inline function get_max():UInt return this[0];
    @:noCompletion public inline function set_max(v:UInt):UInt return this[0]=v;

    public var value(get,set):UInt;
    @:noCompletion public inline function get_value():UInt return this[1];
    @:noCompletion public inline function set_value(v:UInt):UInt return this[1]=v;

    public var coef(get,never):Float;
    public inline function get_coef():Float return this[1]/this[0];

    public inline function new(max:UInt,init:UInt=0) this = Vector.ofArray([max,init]);

    @:to public inline function toFloat():Float return (this[0]!=0) ? this[1]/this[0] : 0;
    @:to public inline function toInt():Int return (this[0]!=0) ? Math.round(this[1]/this[0]) : 0;
    @:to public inline function toUInt():UInt return (this[0]!=0) ? Math.round(this[1]/this[0]) : 0;
    @:to public inline function toString():String return "TickDelta: "+this[1]+"/"+this[0]+" : "+toFloat();
    @:from public static inline function fromUInt(v:UInt):TickDelta return new TickDelta(v,v);

    @:op(A+=B) @:noCompletion public inline function addSet(b:UInt):UInt return this[1]=this[1]+b;
    @:op(A-=B) @:noCompletion public inline function subSet(b:UInt):UInt return this[1]=this[1]-b;
    @:op(A*=B) @:noCompletion public inline function mulSet(b:UInt):UInt return this[1]=this[1]*b;
    @:op(A--) @:noCompletion public inline function decr():UInt return (this[1]==0) ? 0 : (this[1]=this[1]-1)+1;
    @:op(--A) @:noCompletion public inline function retDecr():UInt return this[1]=(this[1]==0) ? this[1] : this[1]-1;
    @:op(A++) @:op(++A) public inline function reset():UInt return this[1]=this[0];

    @:op(A+B) @:noCompletion @:commutative static public inline function add(a:TickDelta,b:UInt):UInt return a.value+b;
    @:op(A*B) @:noCompletion @:commutative static public inline function mul(a:TickDelta,b:UInt):UInt return a.value*b;

    @:op(A/B) @:noCompletion static public inline function divB(a:TickDelta,b:UInt):Float return b/a.value;
    @:op(A-B) @:noCompletion static public inline function subB(a:TickDelta,b:UInt):UInt return b-a.value;

    @:op(A/B) @:noCompletion static public inline function div(a:TickDelta,b:UInt):Float return a.value/b;
    @:op(A-B) @:noCompletion static public inline function sub(a:TickDelta,b:UInt):UInt return a.value-b;

    @:op(A==B) @:noCompletion @:commutative static public inline function equal(a:TickDelta,b:UInt):Bool return a.value==b;
    @:op(A!=B) @:noCompletion @:commutative static public inline function notEqual(a:TickDelta,b:UInt):Bool return a.value!=b;

    @:op(A>=B) @:noCompletion static public inline function supEqual(a:TickDelta,b:UInt):Bool return a.value>=b;
    @:op(A<=B) @:noCompletion static public inline function infEqual(a:TickDelta,b:UInt):Bool return a.value<=b;
    @:op(A>B) @:noCompletion static public inline function sup(a:TickDelta,b:UInt):Bool return a.value>b;
    @:op(A<B) @:noCompletion static public inline function inf(a:TickDelta,b:UInt):Bool return a.value<b;

    @:op(A>=B) @:noCompletion static public inline function supEqualB(b:UInt,a:TickDelta):Bool return b>=a.value;
    @:op(A<=B) @:noCompletion static public inline function infEqualB(a:TickDelta,b:UInt):Bool return b<=a.value;
    @:op(A>B) @:noCompletion static public inline function supB(a:TickDelta,b:UInt):Bool return b>a.value;
    @:op(A<B) @:noCompletion static public inline function infB(a:TickDelta,b:UInt):Bool return b<a.value;
}
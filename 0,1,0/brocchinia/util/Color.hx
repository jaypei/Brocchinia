package brocchinia.util;
/** Class Color from brocchinia.util
  * Created by Chlorodatafile the 14/07/2015 for the first time in RVB-Project.
  * Some class to manage easily a color from
  **/


abstract Color(Void) {
    public static inline function ARGB(a:UInt=0,r:UInt=0,g:UInt=0,b:UInt=0):ARGB return new ARGB(b|(a<<24)|(r<<16)|(g<<8));
    public static inline function RGB(r:UInt=0,g:UInt=0,b:UInt=0):RGB return new RGB((r<<16)|(g<<8)|b);
}

abstract ARGB(UInt) from UInt to UInt{
    public var a(get,set):UInt;
    public inline function get_a():UInt return       (this&0xFF000000)>>>24;
    public inline function set_a(v:UInt):UInt return this=(this&0x00FFFFFF)|(v<<24);

    public var r(get,set):UInt;
    public inline function get_r():UInt return       (this&0x00FF0000)>>>16;
    public inline function set_r(v:UInt):UInt return this=(this&0xFF00FFFF)|(v<<16);

    public var g(get,set):UInt;
    public inline function get_g():UInt return       (this&0x0000FF00)>>>8;
    public inline function set_g(v:UInt):UInt return this=(this&0xFFFF00FF)|(v<<8);

    public var b(get,set):UInt;
    public inline function get_b():UInt return       this&0x000000FF;
    public inline function set_b(v:UInt):UInt return this=(this&0xFFFFFF00)|v;

    public var alpha(get,never):Float;
    public inline function get_alpha():Float return (get_a())/255;

    public var rgb(get,never):RGB;
    public inline function get_rgb():RGB return toRGB();

    public static inline function make(a:UInt=0,r:UInt=0,g:UInt=0,b:UInt=0):Int return b|(a<<24)|(r<<16)|(g<<8);

    public inline function new(v:Int) this = v;

    @:op(A*B) public inline function multiply(o:ARGB):ARGB
        return new ARGB(make(Math.round((a*o.a)/255),Math.round((r*o.r)/255),Math.round((g*o.g)/255),Math.round((b*o.b)/255)));

    @:op(A*=B) public inline function multiplySet(o:ARGB):ARGB
        return this = multiply(o);

    @:op(A%B) public inline function merge(o:ARGB):ARGB
        return new ARGB(make(Math.round((a+o.a)/2),Math.round((r+o.r)/2),Math.round((g+o.g)/2),Math.round((b+o.b)/2)));

    @:op(A%=B) public inline function mergeSet(o:ARGB):ARGB
        return this = merge(o);

    @:op(A*B) public inline function multiplyRGB(o:RGB):ARGB
        return new ARGB(make(a,Math.round((r*o.r)/255),Math.round((g*o.g)/255),Math.round((b*o.b)/255)));

    @:op(A*=B) public inline function multiplySetRGB(o:RGB):ARGB
        return this = multiply(o);

    @:op(A%B) public inline function mergeRGB(o:RGB):ARGB
        return new ARGB(make(a,Math.round((r+o.r)/2),Math.round((g+o.g)/2),Math.round((b+o.b)/2)));

    @:op(A%=B) public inline function mergeSetRGB(o:RGB):ARGB
        return this = merge(o);

    @:from public static inline function fromRGB(o:RGB):ARGB return new ARGB(o.getColor()|0xFF000000);
    @:to public inline function toRGB():RGB return new RGB(this&0x00FFFFFF);
    @:to public inline function getColor():UInt return this;
    @:to public inline function toString():String return "Color(A:"+get_a()+";R:"+get_r()+";G:"+get_g()+";B:"+get_b()+")";
}

abstract RGB(Int) from Int to Int {
    public var r(get,set):UInt;
    public inline function get_r():UInt return       (this&0x00FF0000)>>>16;
    public inline function set_r(v:UInt):UInt return this=(this&0xFF00FFFF)|(v<<16);

    public var g(get,set):UInt;
    public inline function get_g():UInt return       (this&0x0000FF00)>>>8;
    public inline function set_g(v:UInt):UInt return this=(this&0xFFFF00FF)|(v<<8);

    public var b(get,set):UInt;
    public inline function get_b():UInt return       this&0x000000FF;
    public inline function set_b(v:UInt):UInt return this=(this&0xFFFFFF00)|v;

    public static inline function make(r:UInt=0,g:UInt=0,b:UInt=0):Int return (r<<16)|(g<<8)|b;

    public inline function new(v:Int) this = v;

    @:op(A*B) public inline function multiply(o:RGB):RGB
        return new RGB(make(Math.round((r*o.r)/255),Math.round((g*o.g)/255),Math.round((b*o.b)/255)));

    @:op(A*=B) public inline function multiplySet(o:RGB):RGB
        return this = multiply(o);

    @:op(A%B) public inline function merge(o:RGB):RGB
        return new RGB(make(Math.round((r+o.r)/2),Math.round((g+o.g)/2),Math.round((b+o.b)/2)));

    @:op(A%=B) public inline function mergeSet(o:RGB):RGB
        return this = merge(o);

    @:op(A*B) public inline function multiplyARGB(o:ARGB):RGB
        return new RGB(make(Math.round((r*o.r)/255),Math.round((g*o.g)/255),Math.round((b*o.b)/255)));

    @:op(A*=B) public inline function multiplySetARGB(o:ARGB):RGB
        return this = multiply(o);

    @:op(A%B) public inline function mergeARGB(o:ARGB):RGB
        return new RGB(make(Math.round((r+o.r)/2),Math.round((g+o.g)/2),Math.round((b+o.b)/2)));

    @:op(A%=B) public inline function mergeSetARGB(o:ARGB):RGB
        return this = merge(o);

    @:from public static inline function fromARGB(o:ARGB):RGB return new RGB(o.getColor()&0x00FFFFFF);
    @:to public inline function toARGB():ARGB return new ARGB(this|0xFF000000);
    @:to public inline function getColor():Int return this;
    @:to public inline function toString():String return "Color(R:"+get_r()+";G:"+get_g()+";B:"+get_b()+")";
}
package brocchinia.util.bitwise;

/**
 * BitList from package brocchinia.util.bitwise.
 * Created by Chlorodatafile the 27/08/2015 at 13:59, on the project PotatoProject
 * Version : 0.0.1
 * This abstract help you to manage a BitList in a more easier way.
 */


abstract BitList(IBitList) from IBitList to IBitList {
    public inline function new(value:IBitList) this = value;

    public var length(get,never):UInt;
    public inline function get_length():UInt return this.get_length();

    @:arrayAccess //get
    public inline function pop(key:BitwiseDataKind):Int return this.pop(key);

    @:arrayAccess //set
    public inline function push(key:BitwiseDataKind, value:Int):Int return this.push(key,value);

}
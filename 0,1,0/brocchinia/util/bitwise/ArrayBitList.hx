package brocchinia.util.bitwise;

/**
 * ArrayBitList from package brocchinia.util.bitwise.
 * Created by Chlorodatafile the 27/08/2015 at 14:31, on the project PotatoProject
 * Version : 1.4.0
 * This class let you manage a BitList stocked in an Array.
 */


class ArrayBitList implements IBitList {

    var usedBit:UInt;
    var data:Array<Int>;

    public inline static function makeBitList():BitList return new BitList(new ArrayBitList());

    public inline function asBitList():BitList return new BitList(this);

    public function new() {
        data=new Array<Int>();
        usedBit=0;
    }

    public var length(get,never):UInt;
    public function get_length():UInt {
        return usedBit;
    }

    public function push(kind:BitwiseDataKind, value:Int):Int {
        var size:UInt=kind.value;
        var signed:Bool=kind.hadSign;

        if (size>32||size==0)
            throw BitwiseError.UnnallowedSize(size);

        var arrayPos:Int = (usedBit>>>5);
        var linePointer:Int = usedBit & 0x1F;

        if (signed) value=(1<<(size-1))-value;

        if (linePointer==0)
            data[arrayPos]  = value;

        else if (size+linePointer<32)
            data[arrayPos] |= value<<linePointer;

        else {
            data[arrayPos] |= value << linePointer;
            data[arrayPos+1]= value >>> (32-linePointer);
        }

        usedBit+=size;

        return value;
    }

    public function pop(kind:BitwiseDataKind):Int {
        var size:UInt=kind.value;
        var signed:Bool=kind.hadSign;

        if (size>32||size==0)
            throw BitwiseError.UnnallowedSize(size);

        if (usedBit-size>=0) {
            var complement = 32-size;

            var valueToReturn:Int = (data[0] << complement)>>> complement;

            data[0]=data[0] >>> size;

            for (k in 1...data.length) {
                data[k-1]|=data[k]<<complement;
                data[k]   =data[k]>>>size;
            }

            usedBit-=size;

            return (signed) ? (1<<(size-1))-valueToReturn : valueToReturn;

        } else
            throw BitwiseError.WrongUsage("Reading Out of Bound");
    }


}

package brocchinia.util.bitwise;

/**
 * BitwiseDataKind from package brocchinia.util.bitwise.
 * Created by Chlorodatafile the 27/08/2015 at 13:53, on the project PotatoProject
 * Version : 0.0.1
 * This class contain all the kind of Data you can encounter.
 */


@:enum
abstract BitwiseDataKind(Int) from Int to Int from UInt to UInt {
    var BIT         = 0x000001;
    var BYTE        = 0x000008;
    var BYTES       = 0x000020;

    var SIGNED_BYTE = 0x800008;

    var BITS        = 0x000000;
    var SIGNED_BITS = 0x800000;

    @:arrayAccess @:noCompletion public inline function or(v:UInt):BitwiseDataKind return this|v;

    public inline function isSigned():Bool return this&SIGNED_BITS!=0;

    public var hadSign(get,never):Bool;
    @:noCompletion public inline function get_hadSign():Bool return isSigned();

    public var value(get,never):UInt;
    @:noCompletion public inline function get_value():UInt return this&0x3F;

    @:op(A == B) @:noCompletion public static inline function equalA(a:BitwiseDataKind, b:BitwiseDataKind):Bool
    return (a:Int) == (b:Int);
    @:op(A != B) @:noCompletion public static inline function notEqualA(a:BitwiseDataKind, b:BitwiseDataKind):Bool
    return (a:Int) != (b:Int);
    @:op(A == B) @:noCompletion @:commutative public static inline function equalB(a:BitwiseDataKind, b:Int):Bool
    return (a:Int) == b;
    @:op(A != B) @:noCompletion @:commutative public static inline function notEqualB(a:BitwiseDataKind, b:Int):Bool
    return (a:Int) != b;

    @:op(A > B) @:noCompletion public static inline function majA(a:BitwiseDataKind, b:BitwiseDataKind):Bool
    return (a:Int) > (b:Int);
    @:op(A >= B) @:noCompletion public static inline function majEqualA(a:BitwiseDataKind, b:BitwiseDataKind):Bool
    return (a:Int) >= (b:Int);
    @:op(A < B) @:noCompletion public static inline function minA(a:BitwiseDataKind, b:BitwiseDataKind):Bool
    return (a:Int) < (b:Int);
    @:op(A <= B) @:noCompletion public static inline function minEqualA(a:BitwiseDataKind, b:BitwiseDataKind):Bool
    return (a:Int) <= (b:Int);

    @:op(A > B) @:noCompletion public static inline function majB(a:BitwiseDataKind, b:Int):Bool
    return (a:Int) > b;
    @:op(A >= B) @:noCompletion public static inline function majEqualB(a:BitwiseDataKind, b:Int):Bool
    return (a:Int) >= b;
    @:op(A < B) @:noCompletion public static inline function minB(a:BitwiseDataKind, b:Int):Bool
    return (a:Int) < b;
    @:op(A <= B) @:noCompletion public static inline function minEqualB(a:BitwiseDataKind, b:Int):Bool
    return (a:Int) <= b;

    @:op(A > B) @:noCompletion public static inline function majC(a:Int, b:BitwiseDataKind):Bool
    return a > (b:Int);
    @:op(A >= B) @:noCompletion public static inline function majEqualC(a:Int, b:BitwiseDataKind):Bool
    return a >= (b:Int);
    @:op(A < B) @:noCompletion public static inline function minC(a:Int, b:BitwiseDataKind):Bool
    return a < (b:Int);
    @:op(A <= B) @:noCompletion public static inline function minEqualC(a:Int, b:BitwiseDataKind):Bool
    return a <= (b:Int);
}
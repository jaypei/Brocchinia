package brocchinia.util.bitwise;

/**
 * ByteArrayBitList from package brocchinia.util.bitwise.
 * Created by Chlorodatafile the 27/08/2015 at 14:31, on the project PotatoProject
 * Version : 0.0.1
 * This class manage a BitList through a ByteArray, who is usefull when you read or write a file.
 */


import lime.utils.ByteArray;

class ByteArrayBitList implements IBitList {

    public var internalData(default,null):ByteArray;
    var _length:UInt;
    var b:Int; //buffer
    var s:Int; //security

    public inline static function makeBitList(?data:ByteArray):BitList return new BitList(new ByteArrayBitList(data));

    public inline function asBitList():BitList return new BitList(this);

    public function new(?data:ByteArray) {
        internalData=data==null?new ByteArray():data;
        length=0;
        b=0;
        s=0;
    }

    public var length(get,never):UInt;
    public function get_length():UInt {
        return _length+(internalData.bytesAvailable<<3);
    }

    public function push(kind:BitwiseDataKind,value:Int):Int {
        var size:UInt=kind.value;
        var signed:Bool=kind.hadSign;

        if (length>=32) {
            length-=32;
            internalData.writeInt(b);
            b=s;
            s=0;
        }

        if (size>32||size==0)
            throw BitwiseError.UnnallowedSize(size,size>32);

        if (signed) value=(1<<(size-1))-value;

        if (pointer==0)
            b=value;
        else if ((length+size)<32)
            b|= value<<length;
        else {
            b |= (value << length);
            s  =  value >>> (32-length);
        }

        length+=size;
        return value;

    }

    public function pop(kind:BitwiseDataKind):Int {
        var size:UInt=kind.value;
        var signed:Bool=kind.hadSign;

        if (length<32) {
            if (internalData.bytesAvailable>0) {
                if (length==0)
                    b=internalData.readInt();
                else {
                    var t:Int=internalData.readInt();
                    b |= (t << length);
                    s  =  t >>> (32-length);
                }
            }
            length+=32;
        }

        if (size>32||size==0)
            throw BitwiseError.UnnallowedSize(size);


        var complement = 32-size;
        var valueToReturn:Int = (b << complement)>>> complement;

        b = (b >>> size)|(s << complement);
        s = s >>> size;

        length-=size;

        return (signed) ? (1<<(size-1))-valueToReturn : valueToReturn;

    }

}
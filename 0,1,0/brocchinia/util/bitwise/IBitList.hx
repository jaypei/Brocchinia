package brocchinia.util.bitwise;

/**
 * IBitList from package brocchinia.util.bitwise.
 * Created by Chlorodatafile the 27/08/2015 at 13:59, on the project PotatoProject
 * Version : 0.0.1
 * This interface allow you to make your BitList.
 */


interface IBitList {

    public var length(get,never):UInt;
    public function get_length():UInt;

    public function push(kind:BitwiseDataKind,value:Int):Int;

    public function pop(kind:BitwiseDataKind):Int;

}
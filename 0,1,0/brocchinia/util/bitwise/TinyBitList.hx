package brocchinia.util.bitwise;

/**
 * TinyBitList from package brocchinia.util.bitwise.
 * Created by Chlorodatafile the 27/08/2015 at 14:31, on the project PotatoProject
 * Version : 0.8.0
 * This class let you manage a BitList who is stocked in one and only one Int.
 */


class TinyBitList implements IBitList {
    var usedBit:UInt;
    public var value(default,null):Int;

    public inline static function makeBitList():BitList return new BitList(new TinyBitList());

    public inline function asBitList():BitList return new BitList(this);

    public function new() {
        usedBit=0;
        value=0;
    }

    public var length(get,never):UInt;
    public function get_length():UInt {
        return usedBit;
    }

    public function push(kind:BitwiseDataKind, v:Int):Int {
        var size:UInt=kind.value;
        var signed:Bool=kind.hadSign;

        if (size>32||size==0)
            throw BitwiseError.UnnallowedSize(size);

        if (usedBit<32) {
            if (signed) value=(1<<(size-1))-value;

            if (usedBit==0)
                value=v;
            else
                value|=v<<usedBit;

            usedBit+=size;

            return v;
        }
        else
            throw BitwiseError.WrongUsage("Writing Out of Bound");
    }

    public function pop(kind:BitwiseDataKind):Int {
        var size:UInt=kind.value;
        var signed:Bool=kind.hadSign;

        if (size>32||size==0)
            throw BitwiseError.UnnallowedSize(size);

        if (usedBit-size>=0) {
            var complement = 32-size;

            var valueToReturn:Int = (value << complement)>>> complement;

            value=value >>> size;

            this.usedBit-=size;

            return (signed) ? (1<<(size-1))-valueToReturn : valueToReturn;
        }
        else
            throw BitwiseError.WrongUsage("Reading Out of Bound");
    }

}

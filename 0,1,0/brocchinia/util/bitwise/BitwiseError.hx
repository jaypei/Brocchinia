package brocchinia.util.bitwise;

/**
 * BitwiseError from package brocchinia.util.bitwise.
 * Created by Chlorodatafile the 27/08/2015 at 13:58, on the project PotatoProject
 * Version : 0.2.3
 * This enum contain all the error who can be thrown by the bitwise class.
 */


enum BitwiseError {
    UnnapropriateValue(value:Int);
    UnnallowedSize(size:Int);
    WrongUsage(text:String);
}
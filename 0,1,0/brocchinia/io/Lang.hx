package brocchinia.io;

/** Class Lang from brocchinia.io
 * Created by Chlorodatafile the 04/08/2015 for the first time in RVB-Project.
 * Version : 0.0.1
 * This class manage easily the lang notion.
 * ToDo : Notion of BO for each lang included ?
 */


import brocchinia.app.Application;
import sys.io.File;
import sys.io.FileOutput;
import brocchinia.io.events.LangEvent;
import haxe.Json;
import openfl.Lib;
import openfl.Assets;

abstract Lang(Void) {
    public static inline function initialise():Void
        new MLang();

    public static inline function load(chunk:UInt):Void MLang.load(chunk);

    public static inline function list():Array<String> return MLang.list();

    public static inline function get(id:UInt):String return MLang.phrases[id];

    public static inline function fixed(id:UInt):String return MLang.fixedPhrases[id];

    public static var lang(get,set):String;
    @:noCompletion public inline static function get_lang():String return MLang.lang;
    @:noCompletion public inline static function set_lang(lang:String):String return MLang.setLang(lang);

    public static var phrases(get,never):Array<String>;
    @:noCompletion public inline static function get_phrases():Array<String> return MLang.phrases;

    public static var fixedPhrases(get,never):Array<String>;
    @:noCompletion public inline static function get_fixedPhrases():Array<String> return MLang.fixedPhrases;

}

@:final class MLang {

    public static var src:MLang;

    public static var lang:String;
    public static var phrases:Array<String>;
    public static var fixedPhrases:Array<String>;

    var __actualChunk:UInt;

    public function new() {
        src=this;

        // Get the active lang in the .index
        var dotIndex:DotIndex = Json.parse(Assets.getText("lang/index.json"));
        lang=dotIndex.lang;

        // Get the fixed lang part
        var langFile:LangFile = Json.parse(Assets.getText("lang/"+lang+".json"));
        fixedPhrases=langFile.main;
    }

    public static function setLang(newLang:String):String {
        // Edit the .index
        var dotIndex:DotIndex = Json.parse(Assets.getText("lang/index.json"));
        dotIndex.lang=newLang;
        var out:FileOutput=File.write(Assets.getPath("lang/index.json"));
        out.writeString(Json.stringify(dotIndex,""));
        out.close();

        // Change the variable
        lang=newLang;

        // Apply the change
        var langFile:LangFile = Json.parse(Assets.getText("lang/"+lang+".json"));
        phrases=langFile.phrases[src.__actualChunk];
        fixedPhrases=langFile.main;

        Application.dispatchEvent(new LangEvent(LangEvent.LANG_CHANGED,true,false,lang));

        return lang;
    }

    public static function load(chunk:UInt):Void {
        src.__actualChunk=chunk;
        var langFile:LangFile = Json.parse(Assets.getText("lang/"+lang+".json"));
        phrases=langFile.phrases[chunk];

        Application.dispatchEvent(new LangEvent(LangEvent.LANG_CHUNK_LOADED,true,false,lang));
    }

    public static function list():Array<String> {
        var dotIndex:DotIndex = Json.parse(Assets.getText("lang/index.json"));
        return dotIndex.list;
    }

}

private typedef DotIndex = {
    list:Array<String>,
    lang:String
}

private typedef LangFile = {
    main:Array<String>,
    phrases:Array<Array<String>>
}
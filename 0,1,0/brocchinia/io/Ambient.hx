package brocchinia.io;

/**
 * Ambient from package brocchinia.io.
 * Created by Chlorodatafile the 19/08/2015 at 11:30, on the project PotatoProject
 * Version : 0.0.1
 * This class help you to manage a playlist during the program, and to manage the transition.
 * ToDo : Unfinished
 */


import brocchinia.app.Application;
import openfl.events.Event;
import brocchinia.io.events.AmbientEvent;
import openfl.media.Sound;
import openfl.Lib;
import openfl.Assets;

abstract Ambient(Void) {

}

class MAmbient {

    public static var src:MAmbient;

    public var activMusic:UInt;
    public var activPlaylist:UInt;
    var playlistPosition:UInt;
    var music:Array<Sound>;
    var _src:Array<String>;
    var playlist:Array<Array<UInt>>;

    public function new(musics:Array<String>,playlist:Array<Array<UInt>>) {
        src=this;
        _src=musics;
    }

    public function load() {
        music=new Array<Sound>();
        for (i in 0..._src.length)
            music[i]=Assets.getMusic(_src[0][i]);
        _src=null;
        Application.dispatchEvent(new Event(AmbientEvent.AMBIENT_LOAD,true,false));
    }

    public function changePlaylist(to:UInt,music:UInt=0) {
        Application.dispatchEvent(new AmbientEvent(AmbientEvent.AMBIENT_CHANGE,true,false,playlist[to][music]));
    }

    public function changePlaylistMusic(to:UInt) {
        Application.dispatchEvent(new AmbientEvent(AmbientEvent.AMBIENT_CHANGE,true,false,playlist[activPlaylist][to]));
    }

    public function changeMusic(to:UInt) {
        Application.dispatchEvent(new AmbientEvent(AmbientEvent.AMBIENT_CHANGE,true,false,to));
    }

    public function end() {
        Application.dispatchEvent(new AmbientEvent(AmbientEvent.AMBIENT_END,true,false,activMusic));
    }

    public function start(playlistPlayed:UInt=0,playlistPos:UInt=0) {
        activPlaylist=playlistPlayed;
        playlistPosition=playlistPos;
        activMusic=activPlaylist[playlistPlayed][playlistPos];
        Application.dispatchEvent(new AmbientEvent(AmbientEvent.AMBIENT_START,true,false,activMusic));
    }

    public function disposeMusic(id:UInt) {
        music[id].close();
        music[id]=null;
    }

}
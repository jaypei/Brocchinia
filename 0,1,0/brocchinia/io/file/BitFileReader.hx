package brocchinia.io.file;

/** Class BitFileReader from brocchinia.io.file
  * Created by Chlorodatafile the 18/06/15
  * Version : 0.5.4
  * This class allow you to make an authomatised reader who read a file with a predefined pattern and execute all the
  * necessary callbacks.
  * ToDo : Test
  **/


import brocchinia.util.bitwise.BitPile.UnfixedBitPile;
import brocchinia.util.bitwise.BitWiseError;
import lime.utils.ByteArray;
import brocchinia.util.bitwise.BitList;
import brocchinia.io.file.BitFileStructure.BitFileLineStructure;
import brocchinia.io.file.BitFileStructure.BitFileDataStructure;
import brocchinia.io.file.BitFileStructure.BitFileStructure;
import brocchinia.ds.Vector;

typedef BitFileData = {
    @:optional var value:Int;
    @:optional var array:Array<BitFileData>;
}

typedef Callback = BitFileData -> Void;
typedef StepCallback = Void -> Bool;
typedef InitCallback = Void -> Void;

class BitFileReader {

    private var state:Int;
    public var initCall:InitCallback;
    public var stepCallbacks:Vector<StepCallback>;
    public var callbacks:Vector<Callback>;
    private var fileStructure:BitFileStructure;

    public function new(numberOfReadState:Int,fileStructure:BitFileStructure) {
        this.state=0;
        this.fileStructure=fileStructure;
        this.callbacks = new Vector<Callback>(numberOfReadState);
        this.stepCallbacks = new Vector<StepCallback>(numberOfReadState);
    }

    public inline function assignInitCall(callback:InitCallback)
        this.initCall=callback;

    public inline function assignCallback(state:Int,callback:Callback)
        this.callbacks[state]=callback;

    public inline function assignStepCallback(state:Int,callback:StepCallback)
        this.stepCallbacks[state]=callback;

    public function read(file:ByteArray):Void {
        state=0;
        var stack:UnfixedBitPile = new BitList();

        initCall();

        while(state<callbacks.length) {
            if (stack.hadFullBytes(fileStructure.stackSize)) {
                if (stack.pushBit()) {
                    var lineStructure:BitFileLineStructure = fileStructure.lines[state];
                    var data:BitFileData;
                    var key:Int = 0;
                    if (lineStructure.hadHeadComportment) {
                        if (lineStructure.size<0||lineStructure.size>32)
                            throw BitWiseError.UnnallowedSize(lineStructure.size,lineStructure.size>32);
                        key=stack.pushBits(lineStructure.size);
                        data={value:key,array:[for (st in lineStructure.data[key]) analyse(stack,st)]};
                    } else
                        data={array:[for (st in lineStructure.data[key]) analyse(stack,st)]};
                    callbacks[state](data);
                } else {
                    if (stepCallbacks[state]())
                        break;
                    state++;
                }
            } else {
                try {
                    stack.addBytes(file.readInt());
                } catch (error:Dynamic) {
                    stack.addBytes(0);
                }
            }
        }
    }

    private function analyse(stack:UnfixedBitPile,structure:BitFileDataStructure):BitFileData {
        if (structure.size<0||structure.size>32)
            throw BitWiseError.UnnallowedSize(structure.size,structure.size>32);
        var data:BitFileData;
        var key:Int;
        key = stack.pushBits(structure.size,structure.data==null&&structure.signed!=null&&structure.signed);
        if (structure.data!=null&&structure.data[key].length!=0)
            data = {value:key,array:[for (st in structure.data[key]) analyse(stack,st)]};
        else
            data = {value:key};
        return data;
    }

}
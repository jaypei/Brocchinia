package brocchinia.io.file;

/** Class BitFileStructure from brocchinia.io.file
  * Created by Chlorodatafile the 19/06/15
  * Version : 0.5.4
  * This package contain all the neceties to make a BitFile.
  **/


typedef BitFileDataStructure = {
    var size:Int;
    @:optional var signed:Bool;
    @:optional var data:Array<Array<BitFileDataStructure>>;
}

typedef BitFileLineStructure = {
    var hadHeadComportment:Bool;
    @:optional var size:UInt;
    var data:Array<Array<BitFileDataStructure>>;
}

typedef BitFileStructure = {
    var stackSize:UInt;
    var lines:Array<BitFileLineStructure>;
}
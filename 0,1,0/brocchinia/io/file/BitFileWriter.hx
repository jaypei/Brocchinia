package brocchinia.io.file;

/** Class BitFileWriter from brocchinia.io.file
  * Created by Chlorodatafile the 20/06/15
  * Version : 0.5.3
  * A class to help a little to write a Bitfile.
  * ToDo : Writing assist
  * ToDo : Test
  **/


import brocchinia.util.bitwise.BitPile.UnfixedBitPile;
import brocchinia.util.bitwise.BitList;
import brocchinia.ds.Vector;
import lime.utils.ByteArray;

enum BitFileWritingFlag {
    NEW_LINE;
    NEW_STATE;
    FILL_LINE;
    FINISH(path:String);
}

class BitFileWriter {

    private var stack:ByteArray;
    private var current:UnfixedBitPile;

    public function new() {
        current=new BitList();
        stack=null;
    }

    public function newBuffer():Void {
        current=new BitList();
        stack=new ByteArray();
    }

    public function clearBuffer():Void {
        current=null;
        stack=null;
    }

    public function getBuffer():ByteArray {
        return stack;
    }

    public function writeManyBits(values:Array<Int>, sizes:Array<Int>, ?signed:Array<Bool>):Void {
        if (signed==null)
            for (p in 0...sizes.length) {
                current.addBits(values[p],sizes[p],false);
                if (current.hadFullBytes())
                    stack.writeInt(current.pushBytes());
            }
        else
            for (p in 0...sizes.length) {
                current.addBits(values[p],sizes[p],signed[p]);
                if (current.hadFullBytes())
                    stack.writeInt(current.pushBytes());
            }
    }

    public function writeBits(value:Int, size:Int, signed:Bool=false):Void {
        current.addBits(value,size,signed);
        if (current.hadFullBytes())
            stack.writeInt(current.pushBytes());
    }

    public function writeBit(value:Bool=true):Void {
        current.addBits((value)?1:0,1);
        if (current.hadFullBytes())
            stack.writeInt(current.pushBytes());
    }

    public function writeFlag(flag:BitFileWritingFlag) {
        switch(flag) {
            case NEW_LINE:
                writeBit(true);
            case NEW_STATE:
                writeBit(false);
            case FILL_LINE:
                current.fill();
                stack.writeInt(current.pushBytes());
            case FINISH(path):
                writeBits(0,2);
                current.fill();
                stack.writeInt(current.pushBytes());
                stack.writeFile(path);
        }
    }

}
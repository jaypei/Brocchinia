package brocchinia.io.bind.saving;

/**
 * BindIO from package brocchinia.io.bind.saving.
 * Created by Chlorodatafile the 24/08/2015 at 14:17, on the project PotatoProject
 * Version : 0.0.1
 * This class manage an Input Output of the Bind, to make them saved in a Ressource profile.
 */


import brocchinia.app.Application;
import brocchinia.io.bind.Bind.MBind;
import brocchinia.io.events.BindEvent;
import openfl.events.Event;
import brocchinia.io.bind.saving.BindConfig.BindedKeyData;
import brocchinia.io.bind.BindKind;
import openfl.Lib;

class BindIO {
    static inline function makeData(kind:BindKind):BindedKeyData
        return switch (kind) {
            case WAITING: [-1];
            case NONE: [0];
            case KEYBOARD_KEY(key,modifier): [1,key,modifier];
            case GAMEPAD_BUTTON(padID,button): [2,padID,button];
            case GAMEPAD_AXIS(padID,button,positive): [3,padID,button,positive?1:0];
            case MOUSE_CLICK(button): [4,button];
            case MOUSE_WHEEL(xAxis,positive): [5,xAxis?1:0,positive?1:0];
            default: [];
        }

    static inline function makeKind(data:BindedKeyData):BindKind
        return switch (data) {
            case [-1]: BindKind.WAITING;
            case [0] : BindKind.NONE;
            case [1,k,m]: BindKind.KEYBOARD_KEY(k,m);
            case [2,i,b]: BindKind.GAMEPAD_BUTTON(i,b);
            case [3,i,b,p]: BindKind.GAMEPAD_AXIS(i,b,p==1);
            case [4,b]: BindKind.MOUSE_CLICK(b);
            case [5,x,p]: BindKind.MOUSE_WHEEL(x==1,p==1);
            default : BindKind.NONE;
        }

    public static var config(get,set):BindConfig;
    @:noCompletion public static function get_config():BindConfig {
        var stack:BindConfig = new Array<BindedKeyData>();
        var dt:Array<BindKind> = MBind.src.captured;
        for (i in 0...dt.length) {
            stack[i]=makeData(dt[i]);
        }
        return stack;
    }
    @:noCompletion public static function set_config(config:BindConfig):BindConfig {
        var stack:Array<BindKind> = new Array<BindKind>();
        for (i in 0...config.length) {
            stack[i]=makeKind(config[i]);
        }
        Bind.assign(stack);
        Application.dispatchEvent(new Event(BindEvent.BIND_LOAD,true,false));
        return config;
    }
}

package brocchinia.io.bind.saving;

/**
 * BindConfig from package brocchinia.io.bind.saving.
 * Created by Chlorodatafile the 24/08/2015 at 14:41, on the project PotatoProject
 * Version : 0.0.1
 * This file contain just some typedef to make simplier the Ressources profile creation with a Bind on the program.
 */


typedef BindConfig = Array<BindedKeyData>;
typedef BindedKeyData = Array<Int>;
package brocchinia.io.bind;

/**
 * BindKind from package brocchinia.io.bind.
 * Created by Chlorodatafile the 23/08/2015 at 22:14, on the project PotatoProject
 * Version : 0.0.1
 * All the kind of binding who can be made.
 */


import lime.ui.KeyModifier;
import lime.ui.GamepadAxis;
import lime.ui.GamepadButton;
import lime.ui.KeyCode;

enum BindKind {
    WAITING; // Technical value
    NONE; // CatchStopKey press
    KEYBOARD_KEY(key:KeyCode,modifier:KeyModifier); // A key press
    GAMEPAD_BUTTON(padID:Int,button:GamepadButton); // A Gamepad button is pressed
    GAMEPAD_AXIS(padID:Int,button:GamepadAxis,positive:Bool); // A Gamepad axis is pressed
    MOUSE_CLICK(button:Int); // A mouse button is pressed
    MOUSE_WHEEL(xAxis:Bool,positive:Bool); // The mouse wheel roll
}
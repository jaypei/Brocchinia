package brocchinia.io.bind;

/**
 * Bind from package brocchinia.io.bind.
 * Created by Chlorodatafile the 19/08/2015 at 17:10, on the project PotatoProject
 * Version : 0.0.1
 * This class allow you to make Bindable keys during the game.
 */


import brocchinia.app.Application;
import lime.ui.Window;
import lime.ui.Touch;
import brocchinia.io.events.BindEvent;
import brocchinia.io.events.BindEvent.BindedKeyEvent;
import brocchinia.io.events.BindEvent.BindingEvent;
import openfl.events.Event;
import lime.ui.KeyModifier;
import lime.ui.KeyCode;
import lime.ui.GamepadButton;
import lime.ui.GamepadAxis;
import lime.ui.Gamepad;
import openfl.Lib;

abstract Bind(Void) {
    public static var pushed(get,never):Array<Bool>;
    public inline static function get_pushed():Array<Bool> return MBind.src.isPushed;

    public static var captured(get,never):BindCaptured;
    public inline static function get_captured():BindCaptured return new BindCaptured(MBind.src);

    public static var catching(get,never):Bool;
    public inline static function get_catching():Bool return MBind.src.isCapturing!=0;

    public static inline function initialise():Void
        new MBind();

    public static inline function assign(captured:Array<BindKind>):Void
        MBind.src.assign(captured);

    public static inline function translate(kind:BindKind):String
        return MBind.translate(kind);

    public static inline function startCatchingPhase(catcher:BindCatcher):Void
        MBind.src.catchingPhaseInit(catcher);

    public static inline function endCatchingPhase():Void
        MBind.src.catchingPhaseEnd();

    public static inline function catchInput(id:BindedID):Void
        MBind.src.catchInput(id);

    public static inline function linkToWindow(window:Window):Void
        MBind.src.linkToWindow(window);


}

abstract BindCaptured(MBind) {
    public inline function new(v:MBind) this=v;

    public var name(get,never):BindCapturedName;
    public inline function get_name():BindCapturedName return new BindCapturedName(this);

    @:arrayAccess @:noCompletion public inline function get(id:BindedID):BindKind return this.captured[id];
}

abstract BindCapturedName(MBind) {
    public inline function new(v:MBind) this=v;

    @:arrayAccess @:noCompletion public inline function get(id:BindedID):String return MBind.translate(this.captured[id]);
}

class MBind {
    public static function translate(kind:BindKind):String {
        return switch (kind) {
            case WAITING: "...";
            case NONE: "None";
            case KEYBOARD_KEY(key,modifier): _toStringKey(key,modifier);
            case GAMEPAD_BUTTON(padID,button): "[Pad "+padID+"] "+button;
            case GAMEPAD_AXIS(padID,button,positive): "[Pad "+padID+"] "+(button*2+(positive?1:0));
            case MOUSE_CLICK(button): "[Mouse] "+button;
            case MOUSE_WHEEL(xAxis,positive): "[Wheel] "+(xAxis?positive?"left":"right":positive?"up":"down");
            default: "";
        }
    }

    static function _toStringKey(key:KeyCode,modifier:KeyModifier):String {
        return "[Keyboard] \"ToDo\" "+key+" : "+modifier;
        //ToDo : Convetire en texte les données du clavier.
    }

    public static var src:MBind;

    public var isCapturing:UInt;
    public var captured:Array<BindKind>;
    public var isPushed:Array<Bool>;

    var catcher:BindCatcher;
    var __keyboard:Array<UInt>;
    var __padButton:Array<UInt>;
    var __padAxis:Array<UInt>;
    var __mouseButton:Array<UInt>;
    var __mouseWheel:Array<UInt>;

    var sendToWindow:Array<UInt>;

    public function new() {
        if (src==null) {
            src = this;
            linkToWindow(Lib.application.window);
            Gamepad.onConnect.add (onGamepadConnect);
            Touch.onStart.add (onTouchStart);
            Touch.onEnd.add (onTouchEnd);
        } else throw "You can't initialise twice the Bind class";
    }

    public function linkToWindow(window:Window) {
        window.onKeyDown.add(onKeyDown);
        window.onKeyUp.add(onKeyUp);
        window.onMouseDown.add (onMouseDown);
        window.onMouseUp.add (onMouseUp);
        window.onMouseWheel.add (onMouseWheel);
    }

    public function assign(captured:Array<BindKind>):Void {
        this.captured=captured;
        this.isCapturing=0;
        updateSplitters();
    }

    public function catchingPhaseInit(catcher:BindCatcher):Void {
        this.catcher=catcher;
    }

    public function catchingPhaseEnd():Void {
        if (this.isCapturing!=0) catchAbort();
        this.catcher=null;
    }

    public function catchInput(id:BindedID):Void {
        this.catcher.setCatchingID(id);
        this.isCapturing=1;
        this.captured[id]=WAITING;
        Application.dispatchEvent(new BindingEvent(BindEvent.BINDING_BEGIN,true,false,id));
    }

    @:noCompletion private function catchReceive(kind:BindKind):Void {
        this.isCapturing=0;
        this.captured[this.catcher.getCatchingID()]=kind;
        updateSplitters();
        Application.dispatchEvent(new BindingEvent(BindEvent.BINDING_RECEIVED,true,false,this.catcher.getCatchingID()));
    }

    @:noCompletion private function catchAbort():Void {
        this.isCapturing=0;
        this.captured[this.catcher.getCatchingID()]=NONE;
        updateSplitters();
        Application.dispatchEvent(new BindingEvent(BindEvent.BINDING_CANCELLED,true,false,this.catcher.getCatchingID()));
    }
    
    @:noCompletion private function updateSplitters():Void {
        isPushed=[for(i in 0...this.captured.length) false];
        __keyboard=new Array<UInt>();
        __padButton=new Array<UInt>();
        __padAxis=new Array<UInt>();
        __mouseButton=new Array<UInt>();
        __mouseWheel=new Array<UInt>();
        for (k in 0...captured.length)
            switch(captured[k]) {
                case KEYBOARD_KEY(_,_): __keyboard.push(k);
                case GAMEPAD_BUTTON(_,_): __padButton.push(k);
                case GAMEPAD_AXIS(_,_,_): __padAxis.push(k);
                case MOUSE_CLICK(_): __mouseButton.push(k);
                case MOUSE_WHEEL(_,_): __mouseWheel.push(k);
                default:
            }
    }

    @:noCompletion private function onKeyDown(keyCode:KeyCode, modifier:KeyModifier):Void {
        if (isCapturing!=0) {
            if (isCapturing==2) {
                if (keyCode==this.catcher.getCatchStopKey()||this.catcher.getForbiddenKeyList().indexOf(keyCode)!=-1||this.catcher.getConstraint()[0])
                    catchAbort();
                else
                    catchReceive(KEYBOARD_KEY(keyCode,modifier));
            }
            else isCapturing++;
        }
        else
            for(a in __keyboard)
                switch (captured[a]) {
                    case KEYBOARD_KEY(k,m) if (k==keyCode&&m==modifier) :
                        isPushed[a]=true;
                        Application.dispatchEvent(new BindedKeyEvent(BindEvent.BINDED_KEY_PRESS+a,true,false,KEYBOARD));
                    default:
                }
    }
    @:noCompletion private function onKeyUp(keyCode:KeyCode, modifier:KeyModifier):Void {
        if (isCapturing==0)
            for(a in __keyboard)
                switch (captured[a]) {
                    case KEYBOARD_KEY(k,m) if (k==keyCode&&m==modifier) :
                        isPushed[a]=false;
                        Application.dispatchEvent(new BindedKeyEvent(BindEvent.BINDED_KEY_RELEASE+a,true,false,KEYBOARD));
                    default:
                }
    }
    @:noCompletion private function onGamepadConnect(gamepad:Gamepad):Void {
        gamepad.onAxisMove.add (onGamepadAxisMove.bind (gamepad));
        gamepad.onButtonDown.add (onGamepadButtonDown.bind (gamepad));
        gamepad.onButtonUp.add (onGamepadButtonUp.bind (gamepad));
    }
    @:noCompletion private function onGamepadAxisMove(gamepad:Gamepad, axis:GamepadAxis, value:Float):Void {
        if (isCapturing!=0) {
            if (isCapturing==2) {
                if (!this.catcher.getConstraint()[2])
                    catchReceive(GAMEPAD_AXIS(gamepad.id,axis,value>0));
                else
                    catchAbort();
            }
            else isCapturing++;
        } else
            for(a in __padAxis)
                switch (captured[a]) {
                    case GAMEPAD_AXIS(i,b,p) if (i==gamepad.id&&b==axis&&(p==(value>0))) :
                        Application.dispatchEvent(new BindedKeyEvent(BindEvent.BINDED_KEY_PRESS+a,true,false,GAMEPAD_AXIS(value)));
                    default:
                }
    }
    @:noCompletion private function onGamepadButtonDown(gamepad:Gamepad, button:GamepadButton):Void {
        if (isCapturing!=0) {
            if (isCapturing==2) {
                if (!this.catcher.getConstraint()[1])
                    catchReceive(GAMEPAD_BUTTON(gamepad.id,button));
                else
                    catchAbort();
            }
            else isCapturing++;
        } else
            for(a in __padButton)
                switch (captured[a]) {
                    case GAMEPAD_BUTTON(i,b) if (i==gamepad.id&&b==button) :
                        isPushed[a]=true;
                        Application.dispatchEvent(new BindedKeyEvent(BindEvent.BINDED_KEY_PRESS+a,true,false,GAMEPAD_BUTTON));
                    default:
                }
    }
    @:noCompletion private function onGamepadButtonUp(gamepad:Gamepad, button:GamepadButton):Void {
        if (isCapturing==0)
            for(a in __padButton)
                switch (captured[a]) {
                    case GAMEPAD_BUTTON(i,b) if (i==gamepad.id&&b==button) :
                        isPushed[a]=false;
                        Application.dispatchEvent(new BindedKeyEvent(BindEvent.BINDED_KEY_RELEASE+a,true,false,GAMEPAD_BUTTON));
                    default:
                }
    }
    @:noCompletion private function onMouseDown(x:Float, y:Float, button:Int):Void {
        if (isCapturing!=0) {
            if (isCapturing==2) {
                if (!this.catcher.getConstraint()[3])
                    catchReceive(MOUSE_CLICK(button));
                else
                    catchAbort();
            }
            else isCapturing++;
        } else
            for(a in __mouseButton)
                switch (captured[a]) {
                    case MOUSE_CLICK(b) if (b==button) :
                        isPushed[a]=true;
                        Application.dispatchEvent(new BindedKeyEvent(BindEvent.BINDED_KEY_PRESS+a,true,false,MOUSE_BUTTON(x,y)));
                    default:
                }
    }
    @:noCompletion private function onMouseUp(x:Float, y:Float, button:Int):Void {
        if (isCapturing==0)
            for(a in __mouseButton)
                switch (captured[a]) {
                    case MOUSE_CLICK(b) if (b==button) :
                        isPushed[a]=false;
                        Application.dispatchEvent(new BindedKeyEvent(BindEvent.BINDED_KEY_RELEASE+a,true,false,MOUSE_BUTTON(x,y)));
                    default:
                }
    }
    @:noCompletion private function onMouseWheel(deltaX:Float, deltaY:Float):Void {
        if (isCapturing!=0) {
            if (isCapturing==2) {
                if (!this.catcher.getConstraint()[4])
                    catchReceive(MOUSE_WHEEL(deltaX!=0,deltaX!=0?deltaX>0:deltaY>0));
                else
                    catchAbort();
            }
            else isCapturing++;
        } else
            for(a in __mouseButton)
                switch (captured[a]) {
                    case MOUSE_WHEEL(x,p) if (x==(deltaX!=0)&&p==(x?deltaX>0:deltaY>0)) :
                        Application.dispatchEvent(new BindedKeyEvent(BindEvent.BINDED_KEY_PRESS+a,true,false,MOUSE_WHEEL(deltaX,deltaY)));
                    default:
                }
    }
    @:noCompletion private function onTouchStart(touch:Touch):Void {
        if (isCapturing!=0) {
            if (isCapturing==2) {
                if (!this.catcher.getConstraint()[3])
                    catchReceive(MOUSE_CLICK(0));
                else
                    catchAbort();
            }
            else isCapturing++;
        } else if (touch.id==0)
            for(a in __mouseButton)
                switch (captured[a]) {
                    case MOUSE_CLICK(0) :
                        isPushed[a]=true;
                        Application.dispatchEvent(new BindedKeyEvent(BindEvent.BINDED_KEY_PRESS+a,true,false,MOUSE_BUTTON(touch.x,touch.y)));
                    default:
                }
    }
    @:noCompletion private function onTouchEnd(touch:Touch):Void {
        if (isCapturing==0&&touch.id==0)
            for(a in __mouseButton)
                switch (captured[a]) {
                    case MOUSE_CLICK(0) :
                        isPushed[a]=false;
                        Application.dispatchEvent(new BindedKeyEvent(BindEvent.BINDED_KEY_RELEASE+a,true,false,MOUSE_BUTTON(touch.x,touch.y)));
                    default:
                }
    }


}
package brocchinia.io.bind;

/**
 * BindCatcher from package brocchinia.io.bind.
 * Created by Chlorodatafile the 23/08/2015 at 22:46, on the project PotatoProject
 * Version : 0.0.1
 * This interface allow a JIT edition of the Key Binding giving the arrays when needed, and being make when the Bind had to catch.
 */


import lime.ui.KeyCode;

interface BindCatcher {

    function getForbiddenKeyList():Array<KeyCode>; //Give the keys who can't be used for binding

    function getConstraint():Array<Bool>; //Give for the ID whose of the kind can't be used for binding

    function getCatchStopKey():KeyCode; //Give the key who abort the binding

    function setCatchingID(v:BindedID):Void; //Set the ID of the key to bind

    function getCatchingID():BindedID; //Get the ID of the key to bind

}
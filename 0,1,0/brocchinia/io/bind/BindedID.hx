package brocchinia.io.bind;

/**
 * BindedID from package brocchinia.io.bind.
 * Created by Chlorodatafile the 23/08/2015 at 22:49, on the project PotatoProject
 * Version : 0.0.1
 * This class is just a constrain on the UInt to use easier the Bind class.
 */


abstract BindedID(UInt) from UInt to UInt {
    public inline function new(value:UInt) this = value;

    @:from @:noCompletion public static inline function fromUInt(id:UInt):BindedID return new BindedID(id);
    @:to @:noCompletion public inline function getID():UInt return this;

    @:op(A==B) @:noCompletion public inline function equal(a:BindedID):Bool return a.getID()==this;
    @:op(A!=B) @:noCompletion public inline function unEqual(a:BindedID):Bool return a.getID()!=this;
    @:op(A<=B) @:noCompletion public inline function smallerEqual(a:BindedID):Bool return this<=a.getID();
    @:op(A>=B) @:noCompletion public inline function largerEqual(a:BindedID):Bool return this>=a.getID();
    @:op(A<B) @:noCompletion public inline function smaller(a:BindedID):Bool return this<a.getID();
    @:op(A>B) @:noCompletion public inline function larger(a:BindedID):Bool return this>a.getID();

    @:op(A==B) @:commutative @:noCompletion public static inline function equalUInt(a:BindedID,b:UInt):Bool return a.getID()==b;
    @:op(A!=B) @:commutative @:noCompletion public static inline function unEqualUInt(a:BindedID,b:UInt):Bool return a.getID()!=b;

    @:op(A<=B) @:noCompletion public static inline function smallerEqualUInta(a:BindedID,b:UInt):Bool return a.getID()<=b;
    @:op(A>=B) @:noCompletion public static inline function largerEqualUInta(a:BindedID,b:UInt):Bool return a.getID()>=b;
    @:op(A<B) @:noCompletion public static inline function smallerUInta(a:BindedID,b:UInt):Bool return a.getID()<b;
    @:op(A>B) @:noCompletion public static inline function largerUInta(a:BindedID,b:UInt):Bool return a.getID()>b;

    @:op(A<=B) @:noCompletion public static inline function smallerEqualUIntb(b:UInt,a:BindedID):Bool return a.getID()>=b;
    @:op(A>=B) @:noCompletion public static inline function largerEqualUIntb(b:UInt,a:BindedID):Bool return a.getID()<=b;
    @:op(A<B) @:noCompletion public static inline function smallerUIntb(b:UInt,a:BindedID):Bool return a.getID()>b;
    @:op(A>B) @:noCompletion public static inline function largerUIntb(b:UInt,a:BindedID):Bool return a.getID()<b;
}
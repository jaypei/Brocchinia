package brocchinia.io;

/**
 * Ressource from package brocchinia.io.
 * Created by Chlorodatafile the 17/08/2015 at 14:33, on the project PotatoProject
 * Version : 0.0.2
 * This class manage all the ressource of the game, passing by the user profile
 */


import brocchinia.app.Application;
import sys.io.File;
import sys.io.FileOutput;
import haxe.Json;
import openfl.events.Event;
import brocchinia.io.events.RessourceEvent;
import openfl.media.Sound;
import openfl.text.Font;
import openfl.utils.ByteArray;
import openfl.display.BitmapData;
import openfl.Lib;
import openfl.Assets;

@:final class Ressource<Structure> {

    public static var get:Ressource<Dynamic>;

    public var profile:Structure;

    public static var bitmaps:Array<BitmapData>;
    public static var bytes:Array<ByteArray>;
    public static var fonts:Array<Font>;
    public static var sounds:Array<Sound>;

    @:noCompletion var _activeProfile:String;

    public function new<Structure>(?bitmaps:Array<String>,?bytes:Array<String>,?fonts:Array<String>,?sounds:Array<String>) {
        get=this;
        _init([bitmaps==null? [] : bitmaps, bytes==null? [] : bytes, fonts==null? [] : fonts, sounds==null? [] : sounds]);
        var dotIndex:DotIndex = Json.parse(Assets.getText("profiles/index.json"));
        _activeProfile=dotIndex.active;
        Application.dispatchEvent(new Event(RessourceEvent.RESSOURCE_INITED));
    }

    public static function getProfilesList():Array<String> {
        var dotIndex:DotIndex = Json.parse(Assets.getText("profiles/index.json"));
        return dotIndex.profiles;
    }

    public static function newProfile(name:String,defaultProfile:Dynamic):Void {
        var txt:String=Json.stringify(defaultProfile,"");
        var path:String = Assets.getPath("profiles/index.json");
        var out:FileOutput=File.write(path.substring(0,path.length-10)+name+".json");
        out.writeString(txt);
        out.close();
        get._activeProfile=name;
        get._loadProfile();
    }

    public static inline function loadProfile():Void get._loadProfile();
    public static inline function saveProfile():Void get._saveProfile();
    public static inline function changeProfile(name:String):Void get._changeProfile(name);

    @:noCompletion public function _loadProfile():Void {
        profile=Json.parse(Assets.getText("profiles/"+_activeProfile+".json"));
        Application.dispatchEvent(new RessourceEvent(RessourceEvent.CONFIG_LOADED,true,false,profile));
    }

    @:noCompletion public function _saveProfile():Void {
        var txt:String=Json.stringify(profile,"");
        var out:FileOutput=File.write(Assets.getPath("profiles/"+_activeProfile+".json"));
        out.writeString(txt);
        out.close();
        Application.dispatchEvent(new RessourceEvent(RessourceEvent.CONFIG_SAVED,true,false,profile));
    }

    @:noCompletion public function _changeProfile(name:String):Void {
        _saveProfile();
        _activeProfile=name;

        // Edit the .index
        var dotIndex:DotIndex = Json.parse(Assets.getText("profiles/index.json"));
        dotIndex.active=name;
        var out:FileOutput=File.write(Assets.getPath("profiles/index.json"));
        out.writeString(Json.stringify(dotIndex,""));
        out.close();

        _loadProfile();
    }

    @:noCompletion function _init(_src:Array<Array<String>>):Void {
        bitmaps=new Array<BitmapData>();
        for (i in 0..._src[0].length)
            bitmaps[i]=Assets.getBitmapData(_src[0][i]);
        _src[0]=null;

        bytes=new Array<ByteArray>();
        for (i in 0..._src[1].length)
            bytes[i]=Assets.getBytes(_src[1][i]);
        _src[1]=null;

        fonts=new Array<Font>();
        for (i in 0..._src[2].length)
            fonts[i]=Assets.getFont(_src[2][i]);
        _src[2]=null;

        sounds=new Array<Sound>();
        for (i in 0..._src[3].length)
            sounds[i]=Assets.getSound(_src[3][i]);
        _src[3]=null;

        _src=null;
    }

}

private typedef DotIndex = {
    profiles:Array<String>,
    active:String
}
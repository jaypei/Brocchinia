package brocchinia.io.events;

/**
 * Class BindEvent from brocchinia.io.events
 * Created by Chlorodatafile the 10/08/2015 for the first time in RVB-Project.
 * Version : 0.0.1
 * This class contain the Event of the class Bind
 */


import brocchinia.io.bind.BindedID;
import openfl.events.Event;

@:final class BindEvent {

    public static var BIND_LOAD:String = "bindBegin";
    public static var BINDING_BEGIN:String = "bindBegin";
    public static var BINDING_RECEIVED:String = "bindReceived";
    public static var BINDING_CANCELLED:String = "bindCancelled";
    public static var BINDED_KEY_PRESS:String = "bindUseInKey:";
    public static var BINDED_KEY_RELEASE:String = "bindUseOutKey:";

}

class BindingEvent extends Event {

    public var id:BindedID;

    public function new (type:String, bubbles:Bool = true, cancelable:Bool = false, ?id:BindedID) {
        super (type, bubbles, cancelable);
        this.id=id;
    }

    public override function clone ():Event {
        return new BindingEvent (type, bubbles, cancelable,id);
    }

    public override function toString ():String {
        return "[BindingEvent type=\"" + type + "\" bubbles=" + bubbles + " cancelable=" + cancelable + " id=" + id + "]";
    }

}

class BindedKeyEvent extends Event {

    public var data:BindedKeyEventData;

    public function new (type:String, bubbles:Bool = true, cancelable:Bool = false, ?data:BindedKeyEventData) {
        super (type, bubbles, cancelable);
        this.data=data;
    }

    public override function clone ():Event {
        return new BindedKeyEvent (type, bubbles, cancelable,data);
    }

    public override function toString ():String {
        return "[BindedKeyEvent type=\"" + type + "\" bubbles=" + bubbles + " cancelable=" + cancelable + " data=" + data + "]";
    }

}

enum BindedKeyEventData {
    KEYBOARD;
    GAMEPAD_AXIS(value:Float);
    GAMEPAD_BUTTON;
    MOUSE_BUTTON(x:Float, y:Float);
    MOUSE_WHEEL(dx:Float, dy:Float);
}
package brocchinia.io.events;

/**
 * AmbientEvent from package brocchinia.io.events.
 * Created by Chlorodatafile the 19/08/2015 at 11:30, on the project PotatoProject
 * Version : 0.0.1
 * The events emitted by the class Ambient
 */


import openfl.events.Event;

class AmbientEvent extends Event {

    public static var AMBIENT_LOAD:String = "ambientLoaded";
    public static var AMBIENT_CHANGE:String = "ambientChanged";
    public static var AMBIENT_END:String = "ambientEnded";
    public static var AMBIENT_START:String = "ambientStarted";

    public var musicID:UInt;

    public function new(type:String, bubbles:Bool = true, cancelable:Bool = false,id:UInt) {
        super (type, bubbles, cancelable);
        musicID=id;
    }

    public override function clone():Event {
        return new AmbientEvent(type, bubbles, cancelable,musicID);
    }

    public override function toString ():String {
        return "[AmbientEvent type=\"" + type + "\" bubbles=" + bubbles + " cancelable=" + cancelable + " musicID=" + musicID + "]";
    }

}
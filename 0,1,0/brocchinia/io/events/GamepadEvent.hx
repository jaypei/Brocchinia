package brocchinia.io.events;

/** Class GamepadEvent from brocchinia.io.events
  * Created by Chlorodatafile the 10/08/2015 for the first time in RVB-Project.
  * Version : 0.0.1
  * A class to allow the usage of a Gamepad with the lime way.
  * ToDo : Test
  **/


import brocchinia.app.Application;
import lime.ui.GamepadButton;
import lime.ui.GamepadAxis;
import lime.ui.Gamepad;
import openfl.events.Event;
import openfl.Lib;

class GamepadEvent extends Event {

    public static var AXIS_MOVE:String = "gamepadAxisMove";
    public static var BUTTON_DOWN:String = "gamepadButtonDown";
    public static var BUTTON_UP:String = "gamepadButtonUp";
    public static var ADDED:String = "gamepadAdded";
    public static var REMOVED:String = "gamepadRemoved";

    @:noCompletion private static var _eventDispatcherSet:Bool=false;

    public static function setDispatcher() {
        if (!_eventDispatcherSet) {
            Gamepad.onConnect.add(onGamepadConnect);
            _eventDispatcherSet=true;
        }
    }

    @:noCompletion private static function onGamepadConnect(gamepad:Gamepad):Void {
        Application.dispatchEvent(new GamepadEvent(REMOVED,true,false,gamepad));
        gamepad.onAxisMove.add (onGamepadAxisMove.bind (gamepad));
        gamepad.onButtonDown.add (onGamepadButtonDown.bind (gamepad));
        gamepad.onButtonUp.add (onGamepadButtonUp.bind (gamepad));
        gamepad.onDisconnect.add (onGamepadDisconnect.bind (gamepad));
    }
    @:noCompletion private static function onGamepadDisconnect(gamepad:Gamepad):Void {
        Application.dispatchEvent(new GamepadEvent(REMOVED,true,false,gamepad));
    }
    @:noCompletion private static function onGamepadAxisMove(gamepad:Gamepad,axis:GamepadAxis,force:Float):Void {
        Application.dispatchEvent(new GamepadEvent(REMOVED,true,false,gamepad,axis));
    }
    @:noCompletion private static function onGamepadButtonDown(gamepad:Gamepad,button:GamepadButton):Void {
        Application.dispatchEvent(new GamepadEvent(REMOVED,true,false,gamepad,-1,button));
    }
    @:noCompletion private static function onGamepadButtonUp(gamepad:Gamepad,button:GamepadButton):Void {
        Application.dispatchEvent(new GamepadEvent(REMOVED,true,false,gamepad,-1,button));
    }

    public var gamepad:Gamepad;
    public var axis:GamepadAxis;
    public var button:GamepadButton;

    public function new (type:String, bubbles:Bool = true, cancelable:Bool = false, gamepad:Gamepad = null, axis:GamepadAxis=-1, button:GamepadButton=-1) {
        super (type, bubbles, cancelable);
        this.gamepad = gamepad;
    }

    public override function clone ():Event {
        return new GamepadEvent (type, bubbles, cancelable, gamepad);
    }

    public override function toString ():String {
        return "[GamepadEvent type=\"" + type + "\" bubbles=" + bubbles + " cancelable=" + cancelable + " gamepad=" + gamepad + " axis=" + axis + " button=" + button + "]";
    }

}
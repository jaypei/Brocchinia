package brocchinia.io.events;

/**
 * Class LangEvent from brocchinia.io.events
 * Created by Chlorodatafile the 18/08/2015 at 16:01, on the project PotatoProject
 * Version : 0.0.1
 * This class contain all the lang event.
 */


import openfl.events.Event;

class LangEvent extends Event {

    public var lang:String;

    public static var LANG_CHANGED:String = "langChanged";
    public static var LANG_CHUNK_LOADED:String = "langChunkLoaded";

    public function new (type:String, bubbles:Bool = true, cancelable:Bool = false, lang:String) {
        super (type, bubbles, cancelable);
        this.lang=lang;
    }

    public override function clone ():Event {
        return new LangEvent (type, bubbles, cancelable,lang);
    }

    public override function toString ():String {
        return "[LangEvent type=\"" + type + "\" bubbles=" + bubbles + " cancelable=" + cancelable + " lang=" + lang + "]";
    }

}
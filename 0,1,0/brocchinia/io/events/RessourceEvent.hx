package brocchinia.io.events;

/**
 * RessourceEvent from package brocchinia.io.events.
 * Created by Chlorodatafile the 18/08/2015 at 16:01, on the project PotatoProject
 * Version : 0.0.1
 * The events emitted by the class Ressource.
 */


import openfl.events.Event;

class RessourceEvent extends Event {

    var config:Dynamic;

    public static var RESSOURCE_INITED:String = "resrcInited";
    public static var CONFIG_LOADED:String = "resrcConfigLoaded";
    public static var CONFIG_SAVED:String = "resrcConfigSaved";

    public function new(type:String, bubbles:Bool = true, cancelable:Bool = false, ?parameter:Dynamic) {
        super (type, bubbles, cancelable);
        config=parameter;
    }

    public override function clone():Event {
        return new RessourceEvent(type, bubbles, cancelable,config);
    }

    public override function toString ():String {
        return "[RessourceEvent type=\"" + type + "\" bubbles=" + bubbles + " cancelable=" + cancelable + " config=" + config + "]";
    }

}
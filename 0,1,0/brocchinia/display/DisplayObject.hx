package brocchinia.display;
/** Class DisplayObject from brocchinia.display
  * Created by Chlorodatafile the 11/08/2015 for the first time in RVB-Project.
  * A class to use the Graphics part of a DisplayObject in the easier way as possible.
  **/

import openfl.display.Graphics;
import openfl.display.DisplayObject in OpenflDisplayObject;

class DisplayObject extends OpenflDisplayObject {
    public var graphics(get,never):Graphics;
    @:noCompletion private function get_graphics ():Graphics {
        if (__graphics == null) {
            __graphics = new Graphics ();
            @:privateAccess __graphics.__owner = this;
        }
        return __graphics;
    }

    public function new() super();
}

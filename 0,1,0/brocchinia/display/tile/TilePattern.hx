package brocchinia.display.tile;
/** Class TilePattern from brocchinia.display.tile
  * Created by Chlorodatafile the 29/07/2015 for the first time in RVB-Project.
  * A class to manage in an easier way the Tile, with premake Pattern
  **/

abstract TilePattern(BTilePattern) from BTilePattern to BTilePattern {

    public var flags(get,never):Int;
    public inline function get_flags():Int return this.flags;
    public var hadALPHA(get,never):Bool;
    public inline function get_hadALPHA():Bool return flags==TileFlag.ALPHA;
    public var hadRGB(get,never):Bool;
    public inline function get_hadRGB():Bool return flags==TileFlag.RGB;
    public var hadMATRIX(get,never):Bool;
    public inline function get_hadMATRIX():Bool return flags==TileFlag.MATRIX;

    public var alpha(get,set):Float;
    public inline function get_alpha():Float return hadALPHA ? this.pattern[hadRGB ? (hadMATRIX ? 7 : 3) : (hadMATRIX ? 4 : 0)] : Math.NaN;
    public inline function set_alpha(v:Float):Float return this.addAlpha(v);

    public var red(get,set):Float;
    public inline function get_red():Float return hadRGB ? this.pattern[hadMATRIX ? 4 : 0] : Math.NaN;
    public inline function set_red(v:Float):Float return this.addRGB(v,0);

    public var green(get,set):Float;
    public inline function get_green():Float return hadRGB ? this.pattern[hadMATRIX ? 5 : 1] : Math.NaN;
    public inline function set_green(v:Float):Float return this.addRGB(v,1);

    public var blue(get,set):Float;
    public inline function get_blue():Float return hadRGB ? this.pattern[hadMATRIX ? 6 : 2] : Math.NaN;
    public inline function set_blue(v:Float):Float return this.addRGB(v,2);

    public var matrix(get,set):TileMatrix;
    public inline function get_matrix():TileMatrix return hadMATRIX ? new TileMatrix(this.pattern[0],this.pattern[1],this.pattern[2],this.pattern[3]) : null;
    public inline function set_matrix(v:TileMatrix):TileMatrix return this.addMatrix(v);

    public inline function new(v:Array<Float>=null,flags:Int=0) this=new BTilePattern(v==null?[]:v,flags);

    public inline function makeTileData(x:Float,y:Float,id:Float):TileData return new TileData([x,y,id].concat(this.pattern));

    public inline function makeTileBuffer():TileBuffer return new TileBuffer(flags);

    @:to public inline function toArray():Array<Float> return this.pattern;

    @:arrayAccess public inline function get(key:Int):Float return this.pattern[key];
}

class BTilePattern {
    public var flags:Int;
    public var pattern:Array<Float>;

    public function new(content:Array<Float>,flags:Int) {
        this.pattern=content;
        this.flags=flags;
    }

    public function addAlpha(v:Float=0.0):Float {
        flags|=TileFlag.ALPHA;
        var p:Int=0;
        if (flags==TileFlag.MATRIX) p+=4;
        if (flags==TileFlag.RGB) p+=3;
        pattern[p]=v;
        return v;
    }

    public function addRGB(v:Float=0.0,p:Int=0):Float {
        if (flags==TileFlag.MATRIX) p+=4;
        if (flags!=TileFlag.RGB&&flags==TileFlag.ALPHA)
            pattern[p+3]=pattern[p];
        flags|=TileFlag.RGB;
        pattern[p]=v;
        return v;
    }

    public function addMatrix(v:TileMatrix):TileMatrix {
        if (flags==TileFlag.MATRIX&&flags!=0) {
            pattern[4]=pattern[0];
            if (flags==TileFlag.RGB) {
                pattern[5]=pattern[1];
                pattern[6]=pattern[2];
                if (flags==TileFlag.ALPHA)
                    pattern[7]=pattern[3];
            }
        }
        flags|=TileFlag.RGB;
        pattern[0]=v.a;
        pattern[1]=v.b;
        pattern[2]=v.c;
        pattern[3]=v.d;
        return v;
    }

}

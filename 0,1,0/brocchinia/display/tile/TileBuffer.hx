package brocchinia.display.tile;
/** Class TileBuffer from brocchinia.display.tile
  * Created by Chlorodatafile the 23/07/2015 for the first time in RVB-Project.
  * A class to stack the tile before sending them to the surface.
  **/

@:forward(addTileData,addArrayData,add,reset)
abstract TileBuffer(BTileBuffer) from BTileBuffer to BTileBuffer {
    public var flags(get,set):TileFlag;
    public inline function get_flags():TileFlag return this.flags;
    public inline function set_flags(flags:TileFlag):TileFlag return this.reset(true,flags);

    public inline function new(flags:Int=0) this=new BTileBuffer(flags);

    @:op(A+=B) @:op(A+B) @:noCompletion public inline function addTD(o:TileData):TileBuffer return this.addTileData(o);

    @:op(A+=B) @:op(A+B) @:noCompletion public inline function addTB(o:TileBuffer):TileBuffer return this.addTileBuffer(o);

    @:to public inline function toRawTileData():Array<Float> return this.tiles;

    @:arrayAccess public inline function get(key:Int):TileData return this.tiles.slice(key*=this.size,key+this.size);
    @:arrayAccess public inline function set(key:Int,value:TileData):TileData return this.replaceTileData(key,value);

    public inline function remove(key:Int):Void this.tiles.splice(key*=this.size,key+this.size);
}


class BTileBuffer {
    public var size:Int;
    public var flags:TileFlag;
    public var tiles:Array<Float>;

    public function new(flags:Int=0) {
        this.size=3;
        this.flags=flags;
        if (flags==TileFlag.ALPHA) size++;
        if (flags==TileFlag.RGB) size+=3;
        if (flags==TileFlag.MATRIX) size+=4;
        this.tiles = new Array<Float>();
    }

    public function reset(resetFlags:Bool=false,flags:Int=0):Int {
        tiles=new Array<Float>();
        if (resetFlags) {
            this.size=3;
            this.flags=flags;
            if (flags==TileFlag.ALPHA) size++;
            if (flags==TileFlag.RGB) size+=3;
            if (flags==TileFlag.MATRIX) size+=4;
        }
        return flags;
    }

    public function add(x:Float,y:Float,tileID:UInt,?matrix:TileMatrix,?r:Float,?g:Float,?b:Float,?a:Float):BTileBuffer {
        tiles[tiles.length]=x;
        tiles[tiles.length]=y;
        tiles[tiles.length]=tileID;
        if (flags==TileFlag.MATRIX) {
            tiles[tiles.length]=matrix.a;
            tiles[tiles.length]=matrix.b;
            tiles[tiles.length]=matrix.c;
            tiles[tiles.length]=matrix.d;
        }
        if (flags==TileFlag.RGB) {
            tiles[tiles.length]=r;
            tiles[tiles.length]=g;
            tiles[tiles.length]=b;
        }
        if (flags==TileFlag.ALPHA) {
            tiles[tiles.length]= a==null ? r : a;
        }
        return this;
    }

    public function addArrayData(t:Array<Float>):Void {
        var point:Int=3;
        tiles[tiles.length]=t[0];
        tiles[tiles.length]=t[1];
        tiles[tiles.length]=t[2];
        if (flags==TileFlag.MATRIX) {
            tiles[tiles.length]=t[point++];
            tiles[tiles.length]=t[point++];
            tiles[tiles.length]=t[point++];
            tiles[tiles.length]=t[point++];
        }
        if (flags==TileFlag.RGB) {
            tiles[tiles.length]=t[point++];
            tiles[tiles.length]=t[point++];
            tiles[tiles.length]=t[point++];
        }
        if (flags==TileFlag.ALPHA) {
            tiles[tiles.length]= t[point++];
        }
    }

    public function addTileData(t:TileData,iteration:UInt=1):BTileBuffer {
        while(--iteration>0) {
            var point:Int=0;
            tiles[tiles.length]=t.x;
            tiles[tiles.length]=t.y;
            tiles[tiles.length]=t.id;
            if (flags==TileFlag.MATRIX) {
                tiles[tiles.length]=t[point++];
                tiles[tiles.length]=t[point++];
                tiles[tiles.length]=t[point++];
                tiles[tiles.length]=t[point++];
            }
            if (flags==TileFlag.RGB) {
                tiles[tiles.length]=t[point++];
                tiles[tiles.length]=t[point++];
                tiles[tiles.length]=t[point++];
            }
            if (flags==TileFlag.ALPHA) {
                tiles[tiles.length]= t[point++];
            }
        }
        return this;
    }

    public function addTileBuffer(t:BTileBuffer):BTileBuffer {
        if (this.flags==t.flags) this.tiles=this.tiles.concat(t.tiles);
        else throw "Can't add different kind of buffer.";
        return this;
    }

    public function replaceTileData(k:UInt,t:TileData):TileData {
        var tablePoint:Int=k*size;
        var point:Int=0;
        tiles[tablePoint++]=t.x;
        tiles[tablePoint++]=t.y;
        tiles[tablePoint++]=t.id;
        if (flags==TileFlag.MATRIX) {
            tiles[tablePoint++]=t[point++];
            tiles[tablePoint++]=t[point++];
            tiles[tablePoint++]=t[point++];
            tiles[tablePoint++]=t[point++];
        }
        if (flags==TileFlag.RGB) {
            tiles[tablePoint++]=t[point++];
            tiles[tablePoint++]=t[point++];
            tiles[tablePoint++]=t[point++];
        }
        if (flags==TileFlag.ALPHA) {
            tiles[tablePoint++]= t[point++];
        }
        return t;
    }

}
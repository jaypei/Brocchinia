package brocchinia.display.tile;
/** Class TileColor from brocchinia.display.tile
  * Created by Chlorodatafile the 29/07/2015 for the first time in RVB-Project.
  * A class to manage the colors of a Tile, who are Floats between 0 and 1.
  **/

import brocchinia.util.Color;
abstract TileColor(Void) {
    public static inline function RGB(r:Int,g:Int,b:Int):TileRGB return new TileRGB(r/255,g/255,b/255);
    public static inline function RGBA(r:Int,g:Int,b:Int,a:Int):TileRGBA return new TileRGBA(r/255,g/255,b/255,a/255);
}

abstract TileRGB(Vector<Float>) from Vector<Float> to Vector<Float> {
    public var r(get,set):Float;
    public inline function get_r():Float return this[0];
    public inline function set_r(v:Float):Float return this[0]=v;

    public var g(get,set):Float;
    public inline function get_g():Float return this[1];
    public inline function set_g(v:Float):Float return this[1]=v;

    public var b(get,set):Float;
    public inline function get_b():Float return this[2];
    public inline function set_b(v:Float):Float return this[2]=v;

    public inline function new(r:Float,g:Float,b:Float) this = Vector.ofArray([r,g,b]);

    @:op(A*B) public inline function multiply(o:TileRGB):TileRGB
        return new TileRGB(r*o.r,g*o.g,b*o.b);

    @:op(A%B) public inline function merge(o:TileRGB):TileRGB
        return new TileRGB((r+o.r)/2,(g+o.g)/2,(b+o.b)/2);

    @:op(A*B) public inline function multiplyT(o:TileRGBA):TileRGB
        return new TileRGB(r*o.r,g*o.g,b*o.b);

    @:op(A%B) public inline function mergeT(o:TileRGBA):TileRGB
        return new TileRGB((r+o.r)/2,(g+o.g)/2,(b+o.b)/2);

    @:op(A*B) public inline function multiplyRGB(o:RGB):TileRGB
        return new TileRGB(r*o.r/255,g*o.g/255,b*o.b/255);

    @:op(A%B) public inline function mergeRGB(o:RGB):TileRGB
        return new TileRGB((r+o.r/255)/2,(g+o.g/255)/2,(b+o.b/255)/2);

    @:op(A*B) public inline function multiplyARGB(o:ARGB):TileRGB
        return new TileRGB(r*o.r/255,g*o.g/255,b*o.b/255);

    @:op(A%B) public inline function mergeARGB(o:ARGB):TileRGB
        return new TileRGB((r+o.r/255)/2,(g+o.g/255)/2,(b+o.b/255)/2);

    @:from public static inline function fromRGB(o:RGB):TileRGB return new TileRGB(o.r/255,o.g/255,o.b/255);
    @:to public inline function toRGB():RGB return Color.RGB(Math.round(r*255),Math.round(g*255),Math.round(b*255));
    @:to public inline function toString():String return "TileColor(R:"+get_r()+";G:"+get_g()+";B:"+get_b()+")";
    @:to public inline function toTilePattern():TilePattern return new TilePattern([r,g,b],TileFlag.RGB);
    @:to public inline function toRawData():Array<Float> return this;
}

abstract TileRGBA(Vector<Float>) from Vector<Float> to Vector<Float> {
    public var a(get,set):Float;
    public inline function get_a():Float return this[3];
    public inline function set_a(v:Float):Float return this[3]=v;

    public var r(get,set):Float;
    public inline function get_r():Float return this[0];
    public inline function set_r(v:Float):Float return this[0]=v;

    public var g(get,set):Float;
    public inline function get_g():Float return this[1];
    public inline function set_g(v:Float):Float return this[1]=v;

    public var b(get,set):Float;
    public inline function get_b():Float return this[2];
    public inline function set_b(v:Float):Float return this[2]=v;

    public inline function new(r:Float,g:Float,b:Float,a:Float) this = Vector.ofArray([r,g,b,a]);

    @:op(A*B) public inline function multiply(o:TileRGBA):TileRGBA
        return new TileRGBA(r*o.r,g*o.g,b*o.b,a*o.a);

    @:op(A%B) public inline function merge(o:TileRGBA):TileRGBA
        return new TileRGBA((r+o.r)/2,(g+o.g)/2,(b+o.b)/2,(a+o.a)/2);

    @:op(A*B) public inline function multiplyT(o:TileRGB):TileRGBA
        return new TileRGBA(r*o.r,g*o.g,b*o.b,a);

    @:op(A%B) public inline function mergeT(o:TileRGB):TileRGBA
        return new TileRGBA((r+o.r)/2,(g+o.g)/2,(b+o.b)/2,a);

    @:op(A*B) public inline function multiplyARGB(o:ARGB):TileRGBA
        return new TileRGBA(r*o.r/255,g*o.g/255,b*o.b/255,a*o.a/255);

    @:op(A%B) public inline function mergeARGB(o:ARGB):TileRGBA
        return new TileRGBA((r+o.r/255)/2,(g+o.g/255)/2,(b+o.b/255)/2,(a+o.a/255)/2);

    @:op(A*B) public inline function multiplyRGB(o:RGB):TileRGBA
        return new TileRGBA(r*o.r/255,g*o.g/255,b*o.b/255,a);

    @:op(A%B) public inline function mergeRGB(o:RGB):TileRGBA
        return new TileRGBA((r+o.r/255)/2,(g+o.g/255)/2,(b+o.b/255)/2,a);

    @:from public static inline function fromARGB(o:ARGB):TileRGBA return new TileRGBA(o.r/255,o.g/255,o.b/255,o.a/255);
    @:to public inline function toARGB():ARGB return Color.ARGB(Math.round(a*255),Math.round(r*255),Math.round(g*255),Math.round(b*255));
    @:to public inline function toString():String return "TileColor(A:"+get_a()+";R:"+get_r()+";G:"+get_g()+";B:"+get_b()+")";
    @:to public inline function toTilePattern():TilePattern return new TilePattern([r,g,b,a],TileFlag.RGBA);
    @:to public inline function toRawData():Array<Float> return this;
}
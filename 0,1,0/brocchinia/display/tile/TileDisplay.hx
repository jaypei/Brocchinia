package brocchinia.display.tile;

/**
 * TileDisplay from package brocchinia.display.tile.
 * Created by Chlorodatafile the 18/08/2015 at 10:29, on the project PotatoProject
 * Version : 0.0.1
 * This class manage a DisplayObject who can draw in an optimised way some Tiles.
 * ToDo : Test
 */

import openfl.display.Tilesheet;
import openfl.display.Graphics;

class TileDisplay extends DisplayObject {
    var stack:Array<TileBuffer>;
    public var tilesheet:Tilesheet;
    var smooth:Bool;
    var flag:TileFlag;

    public function new(tilesheet:Tilesheet,smooth:Bool=false) {
        super();
        this.flag=-1;
        this.stack=new Array<TileBuffer>();
        this.tilesheet=tilesheet;
        this.smooth=smooth;
    }

    public inline function clear():Void    graphics.clear();

    public function free():Void {
        flag=-1;
        stack=new Array<TileBuffer>();
    }

    public function shift():Null<TileBuffer> {
        return stack.shift();
    }

    public function draw(free:Bool=false):Void {
        for (buff in stack)
            tilesheet.drawTiles(graphics,buff.toRawTileData(),smooth,buff.flags);
        if (free) {
            flag=-1;
            stack=new Array<TileBuffer>();
        }
    }

    public function push(buff:TileBuffer):Void {
        if (flag.isNull()||(flag!=buff.flags)) {
            flag=buff.flags;
            stack.push(buff);
        } else
            stack[stack.length-1].addTB(buff);
    }
}
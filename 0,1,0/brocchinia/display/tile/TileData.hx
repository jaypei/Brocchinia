package brocchinia.display.tile;
/** Class TileData from brocchinia.display.tile
  * Created by Chlorodatafile the 29/07/2015 for the first time in RVB-Project.
  * A class to manage a tile alone, who can be multiplied in a buffer. This Data can be added in the buffer, it's the more
  * close to a tile data than the other class.
  **/

abstract TileData(Array<Float>) from Array<Float> to Array<Float> {
    public var x(get,set):Float;
    public inline function get_x():Float return this[0];
    public inline function set_x(value:Float):Float return this[0]=value;

    public var y(get,set):Float;
    public inline function get_y():Float return this[1];
    public inline function set_y(value:Float):Float return this[1]=value;

    public var id(get,set):UInt;
    public inline function get_id():UInt return cast this[2];
    public inline function set_id(value:UInt):UInt return cast this[2]=value;

    public inline function new(value:Array<Float>) this=value;
    public inline static function make(x:Float,y:Float,id:UInt):TileData return new TileData([x,y,id]);

    public inline function concat(other:Array<Float>):TileData return new TileData(this.concat(other));

    @:to public inline function toRawTileData():Array<Float> return this;
    @:from public static inline function fromRawTileData(value:Array<Float>):TileData return new TileData(value);

    @:op(A+B) @:noCompletion public inline function addBuf(o:TileBuffer):TileBuffer return o.addTileData(this);
    @:op(A+B) @:noCompletion public inline function add(o:TileData):TileBuffer return new TileBuffer().addTileData(this).addTileData(o);
    @:op(A*B) @:noCompletion public inline function mul(n:UInt):TileBuffer return new TileBuffer().addTileData(this,n);

    @:arrayAccess public inline function get(key:Int):Float return this[key+3];
    @:arrayAccess public inline function set(key:Int,value:Float):Float return this[key+3]=value;
}
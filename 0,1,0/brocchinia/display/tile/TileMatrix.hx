package brocchinia.display.tile;

/** Class TileMatrix from brocchinia.display.tile
  * Created by Chlorodatafile the 07/06/15
  * A class to manage the matrix of a tile, to rotate, scale, or skew.
  * ToDo : Test du Radian et Patch arround
  **/

import brocchinia.util.math.Radian;
import openfl.geom.Matrix;

@:forward(setSkew,identity,invert,setTo)
abstract TileMatrix(BTileMatrix) from BTileMatrix to BTileMatrix {
    public var a(get,set):Float;
    @:noCompletion public inline function get_a():Float return this.a;
    @:noCompletion public inline function set_a(v:Float):Float return this.a=v;
    public var b(get,set):Float;
    @:noCompletion public inline function get_b():Float return this.b;
    @:noCompletion public inline function set_b(v:Float):Float return this.b=v;
    public var c(get,set):Float;
    @:noCompletion public inline function get_c():Float return this.c;
    @:noCompletion public inline function set_c(v:Float):Float return this.c=v;
    public var d(get,set):Float;
    @:noCompletion public inline function get_d():Float return this.d;
    @:noCompletion public inline function set_d(v:Float):Float return this.d=v;
    public var tx(get,set):Float;
    @:noCompletion public inline function get_tx():Float return this.tx;
    @:noCompletion public inline function set_tx(v:Float):Float return this.tx=v;
    public var ty(get,set):Float;
    @:noCompletion public inline function get_ty():Float return this.ty;
    @:noCompletion public inline function set_ty(v:Float):Float return this.ty=v;

    public var scale(never,set):Float;
    @:noCompletion public inline function set_scale(v:Float):Float return this.setScale(v,v);
    public var scaleX(get,set):Float;
    @:noCompletion public inline function get_scaleX():Float return this._currentScaleX;
    @:noCompletion public inline function set_scaleX(v:Float):Float return this.setScaleX(v);
    public var scaleY(get,set):Float;
    @:noCompletion public inline function get_scaleY():Float return this._currentScaleY;
    @:noCompletion public inline function set_scaleY(v:Float):Float return this.setScaleY(v);
    public var skewX(get,set):Radian;
    @:noCompletion public inline function get_skewX():Radian return this._currentSkewX;
    @:noCompletion public inline function set_skewX(v:Radian):Radian return this.setSkewX(v);
    public var skewY(get,set):Radian;
    @:noCompletion public inline function get_skewY():Radian return this._currentSkewY;
    @:noCompletion public inline function set_skewY(v:Radian):Radian return this.setSkewY(v);
    public var rotation(get,set):Radian;
    @:noCompletion public inline function get_rotation():Radian return this._currentRotation;
    @:noCompletion public inline function set_rotation(v:Radian):Radian return this.setRotate(v);

    public inline function rotateBy(v:Radian):Void this.rotate(v);
    public inline function skewBy(x:Radian,y:Radian):Void this.skew(x,y);
    public inline function skewByX(x:Radian):Void this.skew(x,0.0);
    public inline function skewByY(y:Radian):Void this.skew(0.0,y);
    public inline function scaleBy(x:Float,y:Float):Void this.scale(x,y);
    public inline function scaleByX(x:Float):Void this.scale(x,1.0);
    public inline function scaleByY(y:Float):Void this.scale(1.0,y);
    public inline function flip():Void this.scale(-1.0,-1.0);
    public inline function flipX():Void this.scale(-1.0,1.0);
    public inline function flipY():Void this.scale(1.0,-1.0);
    public inline function clone():Matrix return this.clone();

    public inline function new(a:Float=1, b:Float=0, c:Float=0, d:Float=1, tx:Float=0, ty:Float=0)
        this=new BTileMatrix(a,b,c,d,tx,ty);

    @:op(A*=B) public inline function concat(o:Matrix):Matrix return this.multSet(o);
    @:op(A*B) public inline static function multA(a:Matrix,b:TileMatrix):Matrix return BTileMatrix.multMatrice(a,b.toMatrix());
    @:op(A*B) public inline static function multB(a:TileMatrix,b:Matrix):Matrix return BTileMatrix.multMatrice(a.toMatrix(),b);

    @:op(A==B) public inline function equal(a:Matrix):Bool return this.equals(a);
    @:op(A==B) public inline static function equalB(a:Matrix,b:TileMatrix):Bool return a.equals(b.toMatrix());

    @:to public inline function toMatrix():Matrix return this;
    @:to public inline function toTilePattern():TilePattern return new TilePattern([a,b,c,d],TileFlag.MATRIX);
    @:to public inline function toRawData():Array<Float> return [a,b,c,d];
    @:from public static inline function fromMatrix(m:Matrix):TileMatrix return new TileMatrix(m.a,m.b,m.c,m.d,m.tx,m.ty);

    @:to public inline function toString():String return "TileMatrix:(|"+a+"   "+b+"||"+c+"   "+d+"|)";
}

class BTileMatrix extends Matrix {

    public var _currentScaleX:Float;
    public var _currentScaleY:Float;
    public var _currentRotation:Radian;
    public var _currentSkewX:Radian;
    public var _currentSkewY:Radian;

    public static function multMatrice(a:Matrix,b:Matrix):Matrix {
        var r:Matrix = a.clone();
        r.concat(b);
        return r;
    }

    public function new(a:Float=1, b:Float=0, c:Float=0, d:Float=1, tx:Float=0, ty:Float=0) {
        super(a,b,c,d,tx,ty);
        this._currentScaleX=1;
        this._currentScaleY=1;
        this._currentRotation=new Radian();
        this._currentSkewX=new Radian();
        this._currentSkewY=new Radian();
    }

    public override function identity() {
        super.identity();
        this._currentScaleX=1;
        this._currentScaleY=1;
        this._currentRotation=new Radian();
        this._currentSkewX=new Radian();
        this._currentSkewY=new Radian();
    }

    public function addToTileBuffer(buffer:TileBuffer,x:Float,y:Float,id:UInt,?pattern:TilePattern):Void {
        var array:Array<Float> = [x,y,id,a,b,c,d];
        if (pattern!=null) array.concat(pattern.toArray());
        buffer.addArrayData(array);
    }

    public function multSet(m:Matrix):Matrix {
        var a1 = a * m.a + b * m.c;
        b = a * m.b + b * m.d;
        a = a1;

        var c1 = c * m.a + d * m.c;
        d = c * m.b + d * m.d;
        c = c1;

        var tx1 = tx * m.a + ty * m.c + m.tx;
        ty = tx * m.b + ty * m.d + m.ty;
        tx = tx1;
        return this;
    }

// SKEW TRANSFORMATION
// #########################################################################

/**
	* Set the skew to a given value in Radians
	*
	* **NOTE:** It applies an absolute skew on the current matrix
	*
	* @param skewX Value for the X axis
	* @param skewX Value for the Y axis
	**/

    public function setSkew(skewX:Radian=null, skewY:Radian=null):Void {

// apply the skew (matrix.c is HORIZONTAL, matrix.b is VERTICAL)
        if (skewX != null) {
            c = Math.tan(skewX) * _currentScaleX;
            _currentSkewX=skewX;
        }
        if (skewY != null) {
            b = Math.tan(skewY) * _currentScaleY;
            _currentSkewY=skewY;
        }
    }

/**
	* Set the skew on the X axis to a given value in Radians
	*
	* **NOTE:** It applies an absolute skew on the current matrix
	*
	* @param skewX Value for the X axis
	**/

    public function setSkewX(skewX:Radian):Radian {
        c = Math.tan(skewX) * _currentScaleX;
        _currentSkewX=skewX;
        return _currentSkewX;
    }

/**
	* Set the skew on the Y axis to a given value in Radians
	*
	* **NOTE:** It applies an absolute skew on the current matrix
	*
	* @param skewY Value for the X axis
	**/

    public function setSkewY(skewY:Radian):Radian {
        b = Math.tan(skewY) * _currentScaleY;
        _currentSkewY=skewY;
        return _currentSkewY;
    }

/**
	* Apply a skew in Radians
	*
	* **NOTE:** 0.0 means no transformation on that axis
	*
	* @param skew Value for the X axis (and Y axis if the skewY is not specified)
	* @param skewY (optional) Value for the Y axis
	**/

    public function skew(skew:Radian, skewY:Radian = null):Void {
// if not specified it will set the x and y skew using the same value
        if (skewY == null) skewY = skew;
        var a1 = a, c1 = c, tx1=tx;
// apply the skew (matrix.c is HORIZONTAL, matrix.b is VERTICAL)
        if (skew != 0.0) {
            var mc = Math.tan(skew);
            a1 = a + b * mc;
            c1 = c + d * mc;
            tx1 = tx + ty * mc;
        }
        if (skewY != 0.0) {
            var mb = Math.tan(skewY);
            b = a * mb + b;
            d = c * mb + d;
            ty = tx * mb + ty;
        }
        tx = tx1;
        a = a1;
        c = c1;
        _currentSkewX+=skew;
        _currentSkewY+=skewY;
    }

// SCALE TRANSFORMATION
// #########################################################################


/**
	* Apply a scale
	*
	* **NOTE:** yfactor=-0.1 is a null value for cross-platform compatibility (apply the same factor on x and y)
	*
	* @param factor Factor for the X axis (and Y axis if the yFactor is not specified)
	* @param yFactor (optional) Factor for the Y axis
	**/

    public override function scale(sx:Float, sy:Float):Void {
        _currentScaleX *= sx;
        _currentScaleY *= sy;
        super.scale(sx,sy);
    }

/**
	* Set the scale on a given value
	*
	* **NOTE:** scaleY=-0.1 is a null value for cross-platform compatibility (use the same value for x and y)
	*
	* @param value Value for the X axis (and Y axis if the scaleY is not specified)
	* @param scaleY (optional) Value for the Y axis
	**/

    public function setScale(value:Float = 1.0, scaleY:Float = null):Float {

        var scaleX:Float = value;
// if not specified it will set the x and y scale using the same value
        if (scaleY == null) scaleY = value;

//apply the transformation
// avoid division by zero
        if (_currentScaleX != 0) {
            var ratio:Float = scaleX / _currentScaleX;
            a *= ratio;
            b *= ratio;
        }
        else {
            var skewY:Float = _currentSkewY;
            a = Math.cos(skewY) * scaleX;
            b = Math.sin(skewY) * scaleX;
        }
// avoid division by zero
        if (_currentScaleY != 0) {
            var ratio:Float = scaleY / _currentScaleY;
            c *= ratio;
            d *= ratio;
        }
        else {
            var skewX:Float = _currentSkewX;
            c = -Math.sin(skewX) * scaleY;
            d = Math.cos(skewX) * scaleY;
        }
        _currentScaleX = scaleX;
        _currentScaleY = scaleY;

        return _currentScaleX;
    }

/**
	* Set the scale for the X axis on a given value
	*
	* @param value Value for the X axis
	**/

    public function setScaleX(scaleX:Float):Float {
// avoid division by zero
        if (_currentScaleX != 0) {
            var ratio:Float = scaleX / _currentScaleX;
            a *= ratio;
            b *= ratio;
        }
        else {
            var skewY:Float = _currentSkewY;
            a = Math.cos(skewY) * scaleX;
            b = Math.sin(skewY) * scaleX;
        }
        _currentScaleX = scaleX;
        return _currentScaleX;
    }

/**
	* Set the scale for the Y axis on a given value
	*
	* @param value Value for the Y axis
	**/

    public function setScaleY(scaleY:Float):Float {
// avoid division by zero
        if (_currentScaleY != 0) {
            var ratio:Float = scaleY / _currentScaleY;
            c *= ratio;
            d *= ratio;
        }
        else {
            var skewX:Float = _currentSkewX;
            c = -Math.sin(skewX) * scaleY;
            d = Math.cos(skewX) * scaleY;
        }
        _currentScaleY = scaleY;
        return _currentScaleY;
    }

// ROTATE TRANSFORMATION
// #########################################################################

/**
	* Apply a rotation
	*
	* @param angle The angle in Radians
	**/

    public override function rotate(angle:Radian):Void {
        super.rotate(angle);
        _currentRotation=angle;
    }

/**
	* Set the rotation on a given value
	*
	* @param angle The absolute angle in Radians
	**/

    public inline function setRotate(angle:Radian):Radian {
        rotate(angle-_currentRotation);
        return _currentRotation;
    }

// #########################################################################

//
}

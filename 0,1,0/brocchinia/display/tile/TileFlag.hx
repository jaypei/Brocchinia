package brocchinia.display.tile;
/** Class TileFlag from brocchinia.display.tile
  * Created by Chlorodatafile the 25/07/2015 for the first time in RVB-Project.
  * A class to simplify the utilisation of the flag for an Tilesheet.
  **/

abstract TileFlag(Int) from Int to Int {
    public static inline var BLEND_NORMAL:TileFlag = new TileFlag(0);
    public static inline var SCALE:TileFlag = new TileFlag(1);
    public static inline var ROTATION:TileFlag = new TileFlag(2);
    public static inline var RGB:TileFlag = new TileFlag(4);
    public static inline var ALPHA:TileFlag = new TileFlag(8);
    public static inline var RGBA:TileFlag = new TileFlag(12);
    public static inline var MATRIX:TileFlag = new TileFlag(16);
    public static inline var COLORED_MATRIX:TileFlag = new TileFlag(20);
    public static inline var TRANSPARENT_MATRIX:TileFlag = new TileFlag(24);
    public static inline var ALTERED_MATRIX:TileFlag = new TileFlag(28);
    public static inline var RECT:TileFlag = new TileFlag(32);
    public static inline var ORIGIN:TileFlag = new TileFlag(64);
    public static inline var BLEND_ADD:TileFlag = new TileFlag(65536);
    public static inline var BLEND_MULTIPLY:TileFlag = new TileFlag(131072);
    public static inline var BLEND_SUBTRACT:TileFlag = new TileFlag(262144);
    public static inline var BLEND_SCREEN:TileFlag = new TileFlag(524288);

    public inline function new(v:Null<Int>) this=v==null? -1 : v;

    @:to public inline function toInt():Int return this;
    @:to public inline function isNull():Bool return this==-1;
    @:from public static inline function fromInt(v:Int):TileFlag return new TileFlag(v);
    @:to public inline function toString():String return "Flag value : "+this;

    @:op(A==B) @:noCompletion public inline function equal(b:TileFlag):Bool return this&b.toInt()!=0;
    @:op(A!=B) @:noCompletion public inline function notEqual(b:TileFlag):Bool return this&b.toInt()==0;
    @:op(A|B) @:noCompletion public inline function bitOr(b:TileFlag):TileFlag return new TileFlag(this|b.toInt());
    @:op(A&B) @:noCompletion public inline function bitAnd(b:TileFlag):TileFlag return new TileFlag(this&b.toInt());

    @:op(A==B) @:noCompletion @:commutative public static inline function equalInt(b:TileFlag,a:Int):Bool return a&b.toInt()!=0;
    @:op(A!=B) @:noCompletion @:commutative public static inline function notEqualInt(b:TileFlag,a:Int):Bool return a&b.toInt()==0;
    @:op(A|B) @:noCompletion @:commutative public static inline function bitOrInt(b:TileFlag,a:Int):TileFlag return new TileFlag(a|b.toInt());
    @:op(A&B) @:noCompletion @:commutative public static inline function bitAndInt(b:TileFlag,a:Int):TileFlag return new TileFlag(a&b.toInt());

}
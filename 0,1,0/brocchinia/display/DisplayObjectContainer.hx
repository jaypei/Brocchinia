package brocchinia.display;
/** Class DisplayObjectContainer from brocchinia.display
  * Created by Chlorodatafile the 12/08/2015 for the first time in RVB-Project.
  * A class to use the Graphics part of a DisplayObject in the easier way as possible.
  * And had the possibility of a DisplayObjectContainer.
  **/

import openfl.display.Graphics;
class DisplayObjectContainer extends openfl.display.DisplayObjectContainer {
    public var graphics(get,never):Graphics;
    @:noCompletion private function get_graphics ():Graphics {
        if (__graphics == null) {
            __graphics = new Graphics ();
            @:privateAccess __graphics.__owner = this;
        }
        return __graphics;
    }

    public function new() super();
}
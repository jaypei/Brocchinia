package brocchinia;
/** Class Time from brocchinia
  * Created by Chlorodatafile the 12/08/2015 for the first time in RVB-Project.
  *
  **/
import brocchinia.util.math.TimeDelta;
import openfl.Lib;

@:final class Time {
    public static var now(get,never):Int;
    public static inline function get_now():Int return Lib.getTimer();

    public static var launch(get,never):Int;
    public static inline function get_launch():Int return self.__init;

    public static var delta(get,never):TimeDelta;
    public static function get_delta():TimeDelta {
        self.__dt=new TimeDelta(launch);
        return self.__dt;
    }

    @:noCompletion public static var self:Time;

    @:noCompletion private var __dt:TimeDelta;
    @:noCompletion public var __init:Int;

    public static function set() {
        if (self==null)
            self=new Time();
    }

    private function new() {
        __init=now;
    }
}
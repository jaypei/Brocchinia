package brocchinia.app;

/**
 * LoadingInstance from package brocchinia.app.
 * Created by Chlorodatafile the 28/08/2015 at 00:21, on the project PotatoProject
 * Version : 0.0.2
 * This class allow you to manage a JIT Loading screen.
 */


import brocchinia.display.DisplayObject;
import openfl.display.Graphics;
import brocchinia.util.math.TickDelta;

class LoadingInstance extends DisplayObject {
    public var progressionDelta:TickDelta;

    public function new() super();

    @:final public function progress():UInt {
        --progressionDelta;
        draw();
        return progressionDelta.value;
    }

    public function draw():Void Make.Abtsract("LoadingInstance.draw():Void");

    public function dispose():Void Make.Abtsract("LoadingInstance.dispose():Void");

    public function set(from:UInt,to:UInt,buffer:UInt):Void {
        ++buffer;
        progressionDelta=buffer;
    }

}

abstract LoadingStep(LoadingInstance) {
    public inline function new(v:LoadingInstance) this=v;

    @:op(A++) public inline function next():UInt return this.progress();

    @:from public static inline function fromLoadingInstance(v:LoadingInstance):LoadingStep return new LoadingStep(v);

    @:to public inline function toFloat():Float return (1-this.progressionDelta.get_coef())*100;
    @:to public inline function toString():String return "Loading progress : "+toFloat()+"%";
}
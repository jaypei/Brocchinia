package brocchinia.app;

/**
 * Window from package brocchinia.app.
 * Created by Chlorodatafile the 28/08/2015 at 14:27, on the project PotatoProject
 * Version : 0.1.0
 * This class manage simply the Lime window for Application
 */


import brocchinia.app._internal.ProgramManager;
import brocchinia.app._internal.WindowID;
import brocchinia.app._internal.GraphicsDefiner;
import lime.app.Config.WindowConfig;
import lime.app.Application;
import openfl.display.Stage;
import openfl.display.Window in OpenflWindow;

abstract Window(AWindow) {
    public inline function new(w:AWindow) this=w;

    public inline function launch(id:UInt,arg:Array<Dynamic>,loadingObject:LoadingInstance,disposeLast:Bool=true):Void
        manager.launch(id,arg,loadingObject,disposeLast);

    public inline function load(id:UInt,arg:Array<Dynamic>,loadingObject:LoadingInstance,disposeLast:Bool=true):Void
        manager.load(id,arg,loadingObject,disposeLast);

    public inline function resume(id:UInt,disposeLast:Bool=true):Void
        manager.resume(id,disposeLast);

    public inline function openPopUp(popUp:PopUp):Void
        manager.openPopUp(popUp);

    public inline function closePopUp():Void
        manager.closePopUp();

    public inline function close():Void
        this.close();

    public var stage(get,never):Stage;
    @:noCompletion @:to public function get_stage():Stage return this.stage;

    public var surface(get,never):Surface;
    @:noCompletion @:to public function get_surface():Surface return new Surface(this.programManager);

    public var core(get,never):AWindow;
    @:noCompletion @:to public inline function get_core():AWindow return this;

    public var manager(get,never):ProgramManager;
    @:noCompletion @:to public inline function get_manager():ProgramManager return this.programManager;

    public var id(get,never):WindowID;
    @:noCompletion public inline function get_id():WindowID return this.wid;

    @:to public inline function toString():String return id.toString();
}

class AWindow extends OpenflWindow {
    public var programManager:ProgramManager;
    public var wid:WindowID;

    @:noCompletion private var prg:Array<Class<Program>>;
    @:noCompletion private var rdg:Array<GraphicsDefiner>;

    public function new(id:WindowID,programs:Array<Class<Program>>,?redefineGraphics:Array<GraphicsDefiner>,config:WindowConfig = null) {
        super (config);
        prg=programs;
        rdg=redefineGraphics;
        wid=id;
    }

    public override function create(application:Application):Void {
        super.create(application);
        programManager=new ProgramManager(wid,this,prg,rdg);
        prg=null;
        rdg=null;
    }

    @:to public override function toString():String return "Brocchinia "+wid.toString();
}

package brocchinia.app;

/**
 * Application from package brocchinia.app.
 * Created by Chlorodatafile the 28/08/2015 at 13:49, on the project PotatoProject
 * Version : 0.9.8
 * This class is the heart of all the Framework, you will use the function of the class Application to manage your Application.
 */


import brocchinia.app.Window.AWindow;
import brocchinia.app.events.ApplicationEvent;
import brocchinia.app._internal.ProgramManager;
import brocchinia.app._internal.GraphicsDefiner;
import brocchinia.app._internal.WindowArray;
import brocchinia.app._internal.WindowID;
import lime.app.Config.WindowConfig;
import lime.ui.Window in LimeWindow;
import openfl.display.Stage;
import openfl.events.Event;
import openfl.Lib;

abstract Application(Void) {
    public static inline function initialise(programs:Array<Class<Program>>,?redefineGraphics:Array<GraphicsDefiner>):Void
        new MApplication(programs,redefineGraphics);

    public static inline function exit(exitCode:Int = 0):Void MApplication.exit(exitCode);

    public static inline function close():Void MApplication.__self.close();
    
    public static inline function dispatchEvent(event:Event,all:Bool=true):Void {
        if (all)
            for (window in Lib.application.windows)
                window.stage.dispatchEvent(event);
        else
            Lib.application.window.stage.dispatchEvent(event);
    }

    public static inline function addEventListener(flag:String,listener:Dynamic->Void,all:Bool=false):Void {
        if (all)
            for (window in Lib.application.windows)
                window.stage.addEventListener(flag,listener);
        else
            Lib.application.window.stage.addEventListener(flag,listener);
    }

    public static inline function removeEventListener(flag:String,listener:Dynamic->Void,all:Bool=false):Void{
        if (all)
            for (window in Lib.application.windows)
                window.stage.removeEventListener(flag,listener);
        else
            Lib.application.window.stage.removeEventListener(flag,listener);
    }

    public static inline function launch(id:UInt,arg:Array<Dynamic>,loadingObject:LoadingInstance,disposeLast:Bool=true):Void
        MApplication.__self.launch(id,arg,loadingObject,disposeLast);

    public static inline function load(id:UInt,arg:Array<Dynamic>,loadingObject:LoadingInstance,disposeLast:Bool=true):Void
        MApplication.__self.load(id,arg,loadingObject,disposeLast);

    public static inline function resume(id:UInt,disposeLast:Bool=true):Void
        MApplication.__self.resume(id,disposeLast);

    public static inline function openPopUp(popUp:PopUp):Void
        MApplication.__self.openPopUp(popUp);

    public static inline function closePopUp():Void
        MApplication.__self.closePopUp();

    public static inline function newWindow(programs:Array<Class<Program>>,?redefineGraphics:Array<GraphicsDefiner>,config:WindowConfig = null):WindowID
        return MApplication.__self.newWindow(programs,redefineGraphics,config);

    public static var current(get,never):Program;
    @:noCompletion public static inline function get_current():Program return MApplication.__self.current;

    public static var currentID(get,never):UInt;
    @:noCompletion public static inline function get_currentID():UInt return MApplication.__self.currentID;

    public static var stage(get,never):Stage;
    @:noCompletion public static inline function get_stage():Stage return MApplication.__self.stage;

    public static var surface(get,never):Surface;
    @:noCompletion public static inline function get_surface():Surface return new Surface(MApplication.__self);

    public static var window(get,never):LimeWindow;
    @:noCompletion public static inline function get_window():LimeWindow return Lib.application.window;

    public static var windows(get,never):WindowArray;
    @:noCompletion public static inline function get_windows():WindowArray return new WindowArray(MApplication.__self.windows);
}

class MApplication extends ProgramManager {
    public static var __self:MApplication;

    public var windows:Array<Window>;

    public function new(programs:Array<Class<Program>>,?redefineGraphics:Array<GraphicsDefiner>) {
        super(MAIN,Lib.application.window,programs,redefineGraphics);
        if (__self!=null) throw "You can run only one application";
        __self = this;
        windows=new Array<Window>();
        Application.dispatchEvent(new Event(ApplicationEvent.APPLICATION_INITIALISED));
    }

    public function newWindow(programs:Array<Class<Program>>,?redefineGraphics:Array<GraphicsDefiner>,config:WindowConfig = null):WindowID {
        var nwid:WindowID=windows.length+1;
        var win:Window = new Window(new AWindow(nwid,programs,redefineGraphics,config));
        Lib.application.createWindow(win.core);
        Application.dispatchEvent(new ApplicationEvent(ApplicationEvent.OPEN_WINDOW,win.manager));
        windows.push(win);
        return nwid;
    }

    public static function exit(exitCode:Int):Void {
        Application.dispatchEvent(new Event(ApplicationEvent.APPLICATION_TERMINATED));
        Sys.exit(exitCode);
    }

}
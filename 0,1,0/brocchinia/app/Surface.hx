package brocchinia.app;

/**
 * Surface from package brocchinia.app.
 * Created by Chlorodatafile the 28/08/2015 at 00:27, on the project PotatoProject
 * Version : 0.0.3
 * This class manage an simplified access to the window data.
 */


import brocchinia.util.math.Coefficient;
import brocchinia.util.math.Percent;
import openfl.display.Graphics;

abstract Surface(ISurface) from ISurface to ISurface {

    public var background(get, never):Graphics;
    @:noCompletion public inline function get_background():Graphics return this.__background;

    public var foreground(get, never):Graphics;
    @:noCompletion public inline function get_foreground():Graphics return this.__foreground;

    public var mainground(get, never):Graphics;
    @:noCompletion public inline function get_mainground():Graphics return this.__mainground;


    public var width(get, never):Int;
    @:noCompletion public inline function get_width():Int return this.__width;

    public var height(get, never):Int;
    @:noCompletion public inline function get_height():Int return this.__height;


    public var wPercent(get, never):Percent;
    @:noCompletion public inline function get_wPercent():Percent return this.__wPercent;

    public var hPercent(get, never):Percent;
    @:noCompletion public inline function get_hPercent():Percent return this.__hPercent;


    public var wCoefficient(get, never):Coefficient;
    @:noCompletion public inline function get_wCoefficient():Coefficient return this.__wCoefficient;

    public var hCoefficient(get, never):Coefficient;
    @:noCompletion public inline function get_hCoefficient():Coefficient return this.__hCoefficient;


    @:noCompletion public inline function new(v:ISurface) this = v;

}

interface ISurface {
    public var __wCoefficient(default,null):Coefficient;
    public var __hCoefficient(default,null):Coefficient;

    public var __wPercent(default,null):Percent;
    public var __hPercent(default,null):Percent;

    public var __width(default,null):Int;
    public var __height(default,null):Int;

    public var __background(default,null):Graphics;

    public var __foreground(default,null):Graphics;

    public var __mainground(default,null):Graphics;
}
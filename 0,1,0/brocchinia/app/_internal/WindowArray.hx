package brocchinia.app._internal;

/**
 * WindowArray from package brocchinia.app._internal.
 * Created by Chlorodatafile the 28/08/2015 at 23:04, on the project PotatoProject
 * Version : 0.0.1
 * This class manage a restricted access on the Application's Window Array
 */


abstract WindowArray(Array<Window>) {
    public inline function new(v:Array<Window>) this=v;

    @:arrayAccess public inline function get(k:WindowID):Window return this[k-1];

    @:arrayAccess public inline function getUInt(k:UInt):Window return this[k-1];
}
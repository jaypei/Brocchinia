package brocchinia.app._internal;

/**
 * GraphicsDefiner from package brocchinia.app._internal.
 * Created by Chlorodatafile the 28/08/2015 at 00:49, on the project PotatoProject
 * Version : 0.1.5
 * This class contain all the kind of definition for a Graphics panel on an Application.
 */


import openfl.display.Stage;
import openfl.display.Sprite;
import openfl.display.Shape;
import openfl.display.DisplayObject;
import brocchinia.display.DisplayObject in BrocchiniaDisplay;
import brocchinia.display.DisplayObjectContainer in BrocchiniaDisplayObjectContainer;

enum GraphicsDefiner {
    // Remove the graphics access
    NONE;
    DISPLAY(display:DisplayObject);

    // Keep the graphics access
    DEFAULT;
    SHAPE(display:Shape);
    SPRITE(display:Sprite);
    ACCESS_DISPLAY(display:BrocchiniaDisplay);
    ACCESS_DISPLAY_CONTAINER(display:BrocchiniaDisplayObjectContainer);
}
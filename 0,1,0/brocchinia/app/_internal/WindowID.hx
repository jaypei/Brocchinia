package brocchinia.app._internal;

/**
 * WindowID from package brocchinia.app._internal.
 * Created by Chlorodatafile the 28/08/2015 at 00:29, on the project PotatoProject
 * Version : 0.0.2
 * This class is here to help people to differenciate the windows.
 */

@:enum
abstract WindowID(UInt) from UInt to UInt {
    var MAIN = 0;

    @:op(A == B) @:noCompletion public static inline function equalA(a:WindowID, b:WindowID):Bool
    return (a:UInt) == (b:UInt);

    @:op(A != B) @:noCompletion public static inline function notEqualA(a:WindowID, b:WindowID):Bool
    return (a:UInt) != (b:UInt);

    @:op(A == B) @:noCompletion @:commutative public static inline function equalB(a:WindowID, b:UInt):Bool
    return (a:UInt) == b;

    @:op(A != B) @:noCompletion @:commutative public static inline function notEqualB(a:WindowID, b:UInt):Bool
    return (a:UInt) != b;

    @:op(A > B) @:noCompletion public static inline function majA(a:WindowID, b:WindowID):Bool
    return (a:UInt) > (b:UInt);

    @:op(A >= B) @:noCompletion public static inline function majEqualA(a:WindowID, b:WindowID):Bool
    return (a:UInt) >= (b:UInt);

    @:op(A < B) @:noCompletion public static inline function minA(a:WindowID, b:WindowID):Bool
    return (a:UInt) < (b:UInt);

    @:op(A <= B) @:noCompletion public static inline function minEqualA(a:WindowID, b:WindowID):Bool
    return (a:UInt) <= (b:UInt);

    @:op(A > B) @:noCompletion public static inline function majB(a:WindowID, b:UInt):Bool
    return (a:UInt) > b;

    @:op(A >= B) @:noCompletion public static inline function majEqualB(a:WindowID, b:UInt):Bool
    return (a:UInt) >= b;

    @:op(A < B) @:noCompletion public static inline function minB(a:WindowID, b:UInt):Bool
    return (a:UInt) < b;

    @:op(A <= B) @:noCompletion public static inline function minEqualB(a:WindowID, b:UInt):Bool
    return (a:UInt) <= b;

    @:op(A > B) @:noCompletion public static inline function majC(a:UInt, b:WindowID):Bool
    return a > (b:UInt);

    @:op(A >= B) @:noCompletion public static inline function majEqualC(a:UInt, b:WindowID):Bool
    return a >= (b:UInt);

    @:op(A < B) @:noCompletion public static inline function minC(a:UInt, b:WindowID):Bool
    return a < (b:UInt);

    @:op(A <= B) @:noCompletion public static inline function minEqualC(a:UInt, b:WindowID):Bool
    return a <= (b:UInt);

    @:to @:noCompletion public inline function toString():String return this==(MAIN:UInt)?"Main Window":"Window "+this;
}
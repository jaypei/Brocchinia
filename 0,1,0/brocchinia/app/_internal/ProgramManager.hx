package brocchinia.app._internal;

/**
 * ProgramManager from package brocchinia.app._internal.
 * Created by Chlorodatafile the 28/08/2015 at 10:36, on the project PotatoProject
 * Version : 0.1.0
 * This class allow you to make an Application window splitted on many Program.
 */


import brocchinia.app.events.ApplicationEvent;
import brocchinia.display.DisplayObject;
import brocchinia.app._internal.GraphicsDefiner;
import brocchinia.app._internal.WindowID;
import brocchinia.app.events.EventDispatcher;
import brocchinia.app.Surface.ISurface;
import brocchinia.Make;
import brocchinia.util.math.Coefficient;
import brocchinia.util.math.Percent;
import brocchinia.Vector;
import openfl.display.Graphics;
import openfl.display.Stage;
import openfl.events.Event;
import lime.ui.Window;
import openfl.Lib;

class ProgramManager extends EventDispatcher implements ISurface implements Performer {

//Private attribute
    private var graphics(get,never):Graphics;
    @:noCompletion private function get_graphics ():Graphics {
        if (__graphics == null) {
            __graphics = new Graphics ();
            @:privateAccess __graphics.__owner = this;
        }
        return __graphics;
    }

    @:noCompletion private var __originalWidth:Int = -1;
    @:noCompletion private var __originalHeight:Int = -1;

    @:noCompletion private var __isProgramRunning:Bool;
    @:noCompletion private var __inited:Bool;

    @:noCompletion private var __executed:Array<Program>;
    @:noCompletion private var __program:Vector<Class<Program>>;

    @:noCompletion private var __activPopUp:PopUp;

    @:noCompletion private var __ofWindow:Window;

//Public attribute
    public var current(get,never):Program;
    public function get_current():Null<Program> return this.__executed[currentID];

    public var currentID(default,null):UInt;


//Core
    public function new(id:WindowID,ref:Window,programs:Array<Class<Program>>,?redefineGraphics:Array<GraphicsDefiner>) {
        super();

        addEventListener(Event.ADDED_TO_STAGE, __onAddition);

        __ofWindow=ref;
        var reference = ref.stage;

        reference.addChild(this);

        this.id=id;

        this.__program=programs;
        this.__executed=new Array<Program>();
        
        if (redefineGraphics!=null) {
            switch(redefineGraphics) {
                case [NONE,NONE,NONE]: throw "You must define at least one graphics";
                case [_,_,_] :
                    switch(redefineGraphics[0]) {
                        case SHAPE(display):
                            this.__background = display.graphics;
                            reference.addChild(display);
                        case SPRITE(display):
                            this.__background = display.graphics;
                            reference.addChild(display);
                        case ACCESS_DISPLAY(display):
                            this.__background = display.graphics;
                            reference.addChild(display);
                        case ACCESS_DISPLAY_CONTAINER(display):
                            this.__background = display.graphics;
                            reference.addChild(display);

                        case DISPLAY(data):
                            reference.addChild(data);

                        case DEFAULT:
                            this.__background = this.graphics;
                        default:
                    }
                    switch(redefineGraphics[1]) {
                        case SHAPE(display):
                            this.__mainground = display.graphics;
                            reference.addChild(display);
                        case SPRITE(display):
                            this.__mainground = display.graphics;
                            reference.addChild(display);
                        case ACCESS_DISPLAY(display):
                            this.__mainground = display.graphics;
                            reference.addChild(display);
                        case ACCESS_DISPLAY_CONTAINER(display):
                            this.__mainground = display.graphics;
                            reference.addChild(display);

                        case DISPLAY(data):
                            reference.addChild(data);

                        case DEFAULT:
                            var ground = new DisplayObject();
                            this.__mainground = ground.graphics;
                            reference.addChild(ground);
                        default:
                    }
                    switch(redefineGraphics[2]) {
                        case SHAPE(display):
                            this.__foreground = display.graphics;
                            reference.addChild(display);
                        case SPRITE(display):
                            this.__foreground = display.graphics;
                            reference.addChild(display);
                        case ACCESS_DISPLAY(display):
                            this.__foreground = display.graphics;
                            reference.addChild(display);
                        case ACCESS_DISPLAY_CONTAINER(display):
                            this.__foreground = display.graphics;
                            reference.addChild(display);

                        case DISPLAY(data):
                            reference.addChild(data);

                        case DEFAULT:
                            var ground = new DisplayObject();
                            this.__foreground = ground.graphics;
                            reference.addChild(ground);
                        default:
                    }
                default: throw "Wrong usage of redefineGraphics:[GraphicsDefiner,GraphicsDefiner,GraphicsDefiner]";
            }
        } else {
            var mainG = new DisplayObject();
            var foreG = new DisplayObject();
            this.__background = this.graphics;
            this.__mainground = mainG.graphics;
            this.__foreground = foreG.graphics;
            reference.addChild(mainG);
            reference.addChild(foreG);
        }

    }

    @:noCompletion @:final private function __onResize(e):Void {
        if (!__inited) {__onInit(); return; }
        __width = __ofWindow.width;
        __height = __ofWindow.height;
        __wPercent = new Percent(__width / 100);
        __hPercent = new Percent(__height / 100);
        __wCoefficient = new Coefficient(__width / __originalWidth);
        __hCoefficient = new Coefficient(__height / __originalHeight);
        if (__isProgramRunning)
            __executed[currentID].onResize(new Surface(this));
        else {
            if (__activPopUp.stillDraw) __executed[currentID].onResize(new Surface(this));
            __activPopUp.onResize(new Surface(this));
        }
    }

    @:noCompletion @:final private function __onAddition(e):Void {
        removeEventListener(Event.ADDED_TO_STAGE, __onAddition);

        __width = __ofWindow.width;
        __height = __ofWindow.height;

        __originalWidth = __width;
        __originalHeight = __height;
        __wCoefficient = new Coefficient(1);
        __hCoefficient = new Coefficient(1);

        stage.addEventListener(Event.RESIZE, __onResize);

#if ios
        haxe.Timer.delay(__onInit, 100); // iOS 6
#else
        __onInit();
#end
    }

    @:noCompletion @:final private function __onInit():Void {
        if (__inited) return;
        __inited = true;
        __isProgramRunning=true;
        addEventListener(Event.ENTER_FRAME, __onUpdate);
    }

    @:noCompletion @:final private function __onUpdate(e):Void {
        if (__isProgramRunning) {
            if (__executed[currentID].onUpdate())
                __executed[currentID].onDraw(new Surface(this));
        } else {
            if (__activPopUp.stillUpdate&&__executed[currentID].onUpdate()&&__activPopUp.stillDraw)
                    __executed[currentID].onDraw(new Surface(this));
            if (__activPopUp.onUpdate())
                __activPopUp.onDraw(new Surface(this));
        }
    }


//ISurfaxe
    public var __wCoefficient(default,null):Coefficient;
    public var __hCoefficient(default,null):Coefficient;

    public var __wPercent(default,null):Percent;
    public var __hPercent(default,null):Percent;

    public var __width(default,null):Int;
    public var __height(default,null):Int;

    public var __background(default,null):Graphics;
    public var __mainground(default,null):Graphics;
    public var __foreground(default,null):Graphics;


//Performer
    public function launch(id:UInt,arg:Array<Dynamic>,loadingObject:LoadingInstance,disposeLast:Bool=true):Void {
        stage.addChild(loadingObject);

        __dualExecutionPrevent(disposeLast);

        var oldID:UInt = currentID;
        currentID = id;
        __dualExecutionPrevent(true);

        __executed[currentID] = Type.createInstance(__program[currentID], arg);
        __executed[currentID].assignPerformer(this);
        __executed[currentID].id=id;

        loadingObject.set(oldID, currentID, __executed[currentID].loadingLength);
        loadingObject.draw();

        __executed[currentID].loading(loadingObject);
        __catchEvent(__executed[currentID]);

        loadingObject.progress();

        stage.removeChild(loadingObject);
        loadingObject.dispose();
        __executed[id].onStart();
        Application.dispatchEvent(new ApplicationEvent(ApplicationEvent.LAUNCH_PROGRAM,this));
    }

    public function load(id:UInt,arg:Array<Dynamic>,loadingObject:LoadingInstance,disposeLast:Bool=true):Void {
        if (__executed[id]!=null) resume(id,disposeLast);
        else launch(id,arg,loadingObject,disposeLast);
    }

    public function resume(id:UInt,disposeLast:Bool=true):Void {
        __dualExecutionPrevent(disposeLast);

        currentID = id;
        __catchEvent(__executed[id]);
        __executed[id].onResume();
        Application.dispatchEvent(new ApplicationEvent(ApplicationEvent.RESUME_PROGRAM,this));
    }

    public function openPopUp(popUp:PopUp):Void {
        if (__isProgramRunning) {
            if (__executed[currentID]!=null) {
                __executed[currentID].onPause();
                __removeEvent(__executed[currentID]);
                Application.dispatchEvent(new ApplicationEvent(ApplicationEvent.PAUSE_PROGRAM,this));
                popUp.assignProgram(__executed[currentID]);
            }
            __isProgramRunning=false;
            __activPopUp=popUp;
            __catchEvent(__activPopUp);
            stage.addChild(__activPopUp);
            __activPopUp.onPopIn();
            Application.dispatchEvent(new ApplicationEvent(ApplicationEvent.POPUP_POP_IN,this));
        }
    }

    public function closePopUp():Void {
        if (!__isProgramRunning) {
            __isProgramRunning=true;
            __activPopUp.onPopOut();
            stage.removeChild(__activPopUp);
            __removeEvent(__activPopUp);
            __activPopUp=null;
            if (__executed[currentID]!=null) {
                __catchEvent(__executed[currentID]);
                __executed[currentID].onResume();
                Application.dispatchEvent(new ApplicationEvent(ApplicationEvent.RESUME_PROGRAM,this));
            }
            Application.dispatchEvent(new ApplicationEvent(ApplicationEvent.POPUP_POP_OUT,this));
        }
    }

    public function close():Void {
        Application.dispatchEvent(new ApplicationEvent(ApplicationEvent.CLOSE_WINDOW,this));
        Lib.application.windows[id].close();
    }

    public var surface(get,never):Surface;
    public function get_surface():Surface return new Surface(this);

    public var id(default,null):WindowID;

//Performer technical
    @:noCompletion @:final private inline function __dualExecutionPrevent(dispose:Bool):Void {
        if (__executed[currentID]!=null) {
            if (dispose) {
                __removeEvent(__executed[currentID]);
                __executed[currentID].onStop();
                __executed[currentID] = null;
                closePopUp();
                Application.dispatchEvent(new ApplicationEvent(ApplicationEvent.DISPOSE_PROGRAM,this));
            }
            else {
                __executed[currentID].onPause();
                __removeEvent(__executed[currentID]);
                Application.dispatchEvent(new ApplicationEvent(ApplicationEvent.PAUSE_PROGRAM,this));
            }
        }
    }

}

package brocchinia.app;

/**
 * Performer from package brocchinia.app.
 * Created by Chlorodatafile the 28/08/2015 at 00:16, on the project PotatoProject
 * Version : 0.0.2
 * This interface is a convenient way to make a link between the Program and he's origin
 */


import brocchinia.app._internal.WindowID;
import openfl.display.Stage;
import openfl.events.Event;

interface Performer {
    public function launch(id:UInt,arg:Array<Dynamic>,loadingObject:LoadingInstance,disposeLast:Bool=true):Void;

    public function load(id:UInt,arg:Array<Dynamic>,loadingObject:LoadingInstance,disposeLast:Bool=true):Void;

    public function resume(id:UInt,disposeLast:Bool=true):Void;

    public function openPopUp(popUp:PopUp):Void;

    public function closePopUp():Void;

    public function close():Void;

    public var surface(get,never):Surface;
    @:noCompletion public function get_surface():Surface;

    public var id(default,null):WindowID;

    public var stage(default,null):Stage;
}
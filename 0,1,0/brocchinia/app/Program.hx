package brocchinia.app;

/**
 * Program from package app.
 * Created by Chlorodatafile the 28/08/2015 at 01:01, on the project PotatoProject
 * Version : 0.0.1
 *
 */


import brocchinia.app.LoadingInstance.LoadingStep;
import brocchinia.app.events.CatchableEvent;
import brocchinia.app.events.EventCatcher;
import brocchinia.io.events.AmbientEvent;
import brocchinia.io.events.RessourceEvent;
import brocchinia.io.events.LangEvent;
import brocchinia.display.DisplayObject;
import brocchinia.Make;
import brocchinia.io.events.GamepadEvent;
import brocchinia.io.events.BindEvent;
import openfl.events.MouseEvent;
import openfl.events.KeyboardEvent;
import openfl.events.JoystickEvent;
import openfl.events.ProgressEvent;
import openfl.events.Event;
import openfl.events.TimerEvent;
import openfl.events.FocusEvent;
import openfl.events.UncaughtErrorEvent;
import openfl.events.SecurityErrorEvent;
import openfl.events.IOErrorEvent;
import openfl.events.AsyncErrorEvent;
import openfl.events.ErrorEvent;
import openfl.events.DataEvent;
import openfl.events.TextEvent;
import openfl.events.SampleDataEvent;
import openfl.events.NetStatusEvent;
import openfl.events.HTTPStatusEvent;
import openfl.events.TouchEvent;
import openfl.events.AccelerometerEvent;

class Program implements EventCatcher {

    public var catchedEvent(default,null):Array<CatchableEvent>;
    public var loadingLength:UInt;
    public var performer:Performer;
    public var id:UInt;

    public function new(catchedEvent:Array<CatchableEvent>,loadingLength:UInt) {
        this.catchedEvent=catchedEvent;
        this.loadingLength=loadingLength;
    }

    @:final public inline function assignPerformer(performer:Performer):Void this.performer=performer;

    public function loading(loading:LoadingStep):Void Make.Abtsract("Program.loading(loading:LoadingStep):Void");

    public function onStart():Void {}
    public function onPause():Void {}
    public function onResume():Void {}
    public function onStop():Void {}

    public function onUpdate():Bool Make.Abtsract("Program.onUpdate():Bool");
    public function onDraw(surface:Surface):Void Make.Abtsract("Program.onDraw(surface:Surface):Void");
    public function onResize(surface:Surface):Void {}

// Smartphone Event
    public function caughtAccelerometerUpdate(e:AccelerometerEvent):Void {}
    public function caughtTouchBegin(e:TouchEvent):Void {}
    public function caughtTouchEnd(e:TouchEvent):Void {}
    public function caughtTouchMove(e:TouchEvent):Void {}
    public function caughtTouchOut(e:TouchEvent):Void {}
    public function caughtTouchOver(e:TouchEvent):Void {}
    public function caughtTouchRollOut(e:TouchEvent):Void {}
    public function caughtTouchRollOver(e:TouchEvent):Void {}
    public function caughtTouchTap(e:TouchEvent):Void {}

// Web Event
    public function caughtHTTPStatus(e:HTTPStatusEvent):Void {}
    public function caughtHTTPResponseStatus(e:HTTPStatusEvent):Void {}
    public function caughtNetStatus(e:NetStatusEvent):Void {}
    public function caughtSampleData(e:SampleDataEvent):Void {}
    public function caughtTextInput(e:TextEvent):Void {}
    public function caughtLink(e:TextEvent):Void {}
    public function caughtData(e:DataEvent):Void {}
    public function caughtUploadCompleteData(e:DataEvent):Void {}

// Error Event
    public function caughtError(e:ErrorEvent):Void {}
    public function caughtAsyncError(e:AsyncErrorEvent):Void {}
    public function caughtIOError(e:IOErrorEvent):Void {}
    public function caughtSecurityError(e:SecurityErrorEvent):Void {}
    public function caughtUncaughtError(e:UncaughtErrorEvent):Void {}

// Focus Event
    public function caughtFocusIn(e:FocusEvent):Void {}
    public function caughtFocusOut(e:FocusEvent):Void {}
    public function caughtKeyboardFocusChange(e:FocusEvent):Void {}
    public function caughtMouseFocusChange(e:FocusEvent):Void {}

// Core Event
    public function caughtTimer(e:TimerEvent):Void {}
    public function caughtTimerComplete(e:TimerEvent):Void {}
    public function caughtSoundComplete(e:Event):Void {}
    public function caughtProgress(e:ProgressEvent):Void {}
    public function caughtSocketData(e:ProgressEvent):Void {}

// Bind Event
    public function caughtBindLoad(e:Event):Void {}
    public function caughtBindingBegin(e:BindingEvent):Void {}
    public function caughtBindingReceived(e:BindingEvent):Void {}
    public function caughtBindingCancelled(e:BindingEvent):Void {}

// Joystick Event
    public function caughtJoystickAxisMove(e:JoystickEvent):Void {}
    public function caughtJoystickBallMove(e:JoystickEvent):Void {}
    public function caughtJoystickHatMove(e:JoystickEvent):Void {}
    public function caughtJoystickButtonDown(e:JoystickEvent):Void {}
    public function caughtJoystickButtonUp(e:JoystickEvent):Void {}
    public function caughtJoystickAdded(e:JoystickEvent):Void {}
    public function caughtJoystickRemoved(e:JoystickEvent):Void {}

// Keyboard Event
    public function caughtKeyDown(e:KeyboardEvent):Void {}
    public function caughtKeyUp(e:KeyboardEvent):Void {}

// Mouse Event
    public function caughtMouseSimpleClick(e:MouseEvent):Void {}
    public function caughtMouseRightClick(e:MouseEvent):Void {}
    public function caughtMouseDoubleClick(e:MouseEvent):Void {}
    public function caughtMouseMiddleClick(e:MouseEvent):Void {}
    public function caughtMouseMiddlePressed(e:MouseEvent):Void {}
    public function caughtMouseMiddleReleased(e:MouseEvent):Void {}
    public function caughtMouseLeftPressed(e:MouseEvent):Void {}
    public function caughtMouseLeftReleased(e:MouseEvent):Void {}
    public function caughtMouseRightPressed(e:MouseEvent):Void {}
    public function caughtMouseRightReleased(e:MouseEvent):Void {}
    public function caughtMouseMove(e:MouseEvent):Void {}
    public function caughtMouseOut(e:MouseEvent):Void {}
    public function caughtMouseOver(e:MouseEvent):Void {}
    public function caughtMouseWheel(e:MouseEvent):Void {}
    public function caughtMouseRollOut(e:MouseEvent):Void {}
    public function caughtMouseRollOver(e:MouseEvent):Void {}

// Gamepad Event
    public function caughtGamepadAxisMove(e:GamepadEvent):Void {}
    public function caughtGamepadButtonDown(e:GamepadEvent):Void {}
    public function caughtGamepadButtonUp(e:GamepadEvent):Void {}
    public function caughtGamepadAdded(e:GamepadEvent):Void {}
    public function caughtGamepadRemoved(e:GamepadEvent):Void {}

// Ressource Event
    public function caughtRessourceInited(e:Event):Void {}
    public function caughtConfigLoaded(e:RessourceEvent):Void {}
    public function caughtConfigSaved(e:RessourceEvent):Void {}

// Lang Event
    public function caughtLangChanged(e:LangEvent):Void {}
    public function caughtLangChunkLoaded(e:LangEvent):Void {}

// Ambient Event
    public function caughtAmbientLoad(e:Event):Void {}
    public function caughtAmbientChange(e:AmbientEvent):Void {}
    public function caughtAmbientEnd(e:AmbientEvent):Void {}
    public function caughtAmbientStart(e:AmbientEvent):Void {}

}

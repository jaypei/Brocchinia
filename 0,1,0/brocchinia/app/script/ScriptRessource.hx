package brocchinia.app.script;

/**
 * ScriptRessource from package brocchinia.app.script.
 * Created by Chlorodatafile the 28/08/2015 at 01:16, on the project PotatoProject
 * Version : 0.0.2
 * This class is used to give you the loaded ressource during a Script.
 */


import openfl.media.SoundChannel;
import openfl.media.Sound;
import openfl.display.BitmapData;
import brocchinia.util.math.TimeDelta;

abstract ScriptRessource(Script) {
    public var deltaTime(get,never):TimeDelta;
    public inline function get_deltaTime():TimeDelta return this.deltaTime;

    public var startTime(get,never):Int;
    public inline function get_startTime():Int return this.startTime;

    public var endTime(get,never):Int;
    public inline function get_endTime():Int return this.endTime;

    public var duration(get,never):UInt;
    public inline function get_duration():UInt return this.states[this.currentState].duration;

    public var sprites(get,never):Vector<BitmapData>;
    public inline function get_sprites():Vector<BitmapData> return this.sprites;

    public var sounds(get,never):Vector<Sound>;
    public inline function get_sounds():Vector<Sound> return this.sounds;

    public var music(get,never):SoundChannel;
    public inline function get_music():SoundChannel return this.music;

    public inline function new(v:Script) this=v;
}
package brocchinia.app.script;

/**
 * ScriptState from package brocchinia.app.script.
 * Created by Chlorodatafile the 28/08/2015 at 01:14, on the project PotatoProject
 * Version : 0.0.2
 * This class give you the structure of a state of the Script.
 */

typedef ScriptState = {
    @:optional var skippable:Bool;
    @:optional var goToWhenSkip:UInt;

    @:optional var wait:Bool;

    @:optional var begin:Callback;
    @:optional var update:Callback;
    @:optional var end:Callback;

    var draw:DrawFunction;
    var duration:UInt;
};

typedef Callback = ScriptRessource -> Void;
typedef DrawFunction = ScriptRessource -> Surface -> Void;
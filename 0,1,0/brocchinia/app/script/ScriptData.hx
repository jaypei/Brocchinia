package brocchinia.app.script;

/**
 * ScriptData from package brocchinia.app.script.
 * Created by Chlorodatafile the 28/08/2015 at 01:14, on the project PotatoProject
 * Version : 0.0.2
 * This class contain the main part of the construction of a Script.
 */


typedef ScriptData = {
    @:optional var spriteToLoad:Array<String>;
    @:optional var soundToLoad:Array<String>;
    @:optional var musicPath:String;

    @:optional var autoSkip:Bool;
    var states:Array<ScriptState>;

    @:optional var resetProgram:Bool;
    @:optional var programParameter:Array<Dynamic>;
    var loadingInstance:LoadingInstance;

    var goToModule:UInt;
};
package brocchinia.app.script;

/**
 * Class Script from brocchinia.app.script.
 * Created by chlorodatafile the 05/07/15 for the first time in RVB-Project.
 * Version : 0.3.2
 * A class to simplify the creation of an introduction, or a credit scene.
 */


import haxe.Timer;
import brocchinia.app.LoadingInstance.LoadingStep;
import brocchinia.app.events.CatchableEvent;
import openfl.media.SoundChannel;
import openfl.media.Sound;
import openfl.display.BitmapData;
import brocchinia.util.math.TimeDelta;
import brocchinia.Vector;
import openfl.Assets;


class Script extends Program {

    @:noCompletion public var deltaTime:TimeDelta;
    @:noCompletion public var startTime:Int;
    @:noCompletion public var endTime:Int;
    @:noCompletion public var sprites:Vector<BitmapData>;
    @:noCompletion public var sounds:Vector<Sound>;
    @:noCompletion public var music:SoundChannel;
    @:noCompletion public var musicBuffer:Sound;

    @:noCompletion private var spritesPath:Array<String>;
    @:noCompletion private var soundsPath:Array<String>;
    @:noCompletion private var musicPath:String;

    @:noCompletion private var autoSkip:Bool;
    @:noCompletion public var states:Array<ScriptState>;
    @:noCompletion public var currentState:UInt;

    @:noCompletion private var resetProgram:Bool;
    @:noCompletion private var programParameter:Array<Dynamic>;
    @:noCompletion private var loadingInstance:LoadingInstance;
    @:noCompletion private var goToModule:UInt;

    public function new(catchedEvent:Array<CatchableEvent>,data:ScriptData) {
        super(catchedEvent,calcBuffer(data));
        spritesPath=data.spriteToLoad!=null ? data.spriteToLoad : [];
        soundsPath=data.soundToLoad!=null ? data.soundToLoad : [];
        musicPath=data.musicPath;
        autoSkip=data.autoSkip!=null? data.autoSkip : false;
        resetProgram=data.resetProgram!=null? data.resetProgram : false;
        programParameter=data.programParameter!=null? data.programParameter : [];
        goToModule=data.goToModule;
        loadingInstance=data.loadingInstance;
        currentState=0;
        states = data.states;
        for (state in data.states) {
            if (state.begin==null) state.begin=void;
            if (state.update==null) state.update=void;
            if (state.end==null) state.end=void;
            if (state.skippable==null) state.skippable=false;
            if (state.wait==null) state.wait=false;
        }
    }

    @:final private function void(v:ScriptRessource):Void {}

    @:final private inline function calcBuffer(data:ScriptData):UInt
        return (data.musicPath!=null? 1 : 0)
            + (data.soundToLoad!=null? data.soundToLoad.length : 1)
            + (data.spriteToLoad!=null? data.spriteToLoad.length : 1);

    @:final override public function loading(loading:LoadingStep):Void {
        // Import the sprites
        sprites=new Vector<BitmapData>(spritesPath.length);
        for (i in 0...spritesPath.length) {
            sprites[i]=Assets.getBitmapData (spritesPath[i]);
            loading++;
        }
        spritesPath=null;
        loading++;

        // Import the sounds
        sounds=new Vector<Sound>(soundsPath.length);
        for (i in 0...soundsPath.length) {
            sounds[i]=Assets.getSound(soundsPath[i]);
            loading++;
        }
        soundsPath=null;
        loading++;

        // Import the music
        if (musicPath!=null) {
            musicBuffer= Assets.getMusic(musicPath);
            musicPath=null;
            loading++;
        }
    }

    override public function onStart():Void {
        if (musicBuffer!=null)
            music=musicBuffer.play();
        if (states[currentState].wait&&!autoSkip) __delay(__timeEnd);
        else __delay(__next);
    }

    @:final override public function onPause():Void
        throw "A script can't be paused";

    override public function onStop():Void {
        for (sound in sounds)
            sound.close();
        for (sprite in sprites)
            sprite.dispose();
        if (music!=null)
            music.stop();
        if (musicBuffer!=null)
            musicBuffer.close();
        sounds=null;
        sprites=null;
        music=null;
        musicBuffer=null;
    }

    @:final override public function onUpdate():Bool {
        deltaTime=new TimeDelta(startTime);
        states[currentState].update(new ScriptRessource(this));
        return true;
    }

    @:final override public function onDraw(surface:Surface):Void
        states[currentState].draw(new ScriptRessource(this),surface);

    @:final override public function onResize(surface:Surface):Void
        states[currentState].draw(new ScriptRessource(this),surface);

    @:final public function skip(force:Bool=false):Void {
        if (force||states[currentState].skippable) {
            if (currentState+1==states.length) {
                __stop();
                return;
            }
            currentState=states[currentState].goToWhenSkip-1;
            __next();
        }
    }

    @:final @:noCompletion private function __next():Void {
        states[currentState].end(new ScriptRessource(this));
        ++currentState;
        if (currentState==states.length) __stop();
        else if (states[currentState].wait&&!autoSkip) __delay(__timeEnd);
        else __delay(__next);
    }

    @:final @:noCompletion private function __delay(f:Void->Void):Void {
        states[currentState].begin(new ScriptRessource(this));
        startTime=Time.now;
        endTime=startTime+states[currentState].duration;
        Timer.delay(f,states[currentState].duration);
    }

    @:final @:noCompletion private inline function __stop():Void {
        if (resetProgram) performer.launch(goToModule,programParameter,loadingInstance,true);
        else performer.load(goToModule,programParameter,loadingInstance,true);
    }

    @:final @:noCompletion private function __timeEnd():Void
        states[currentState].skippable=true;

    @:final override public function onResume():Void {}
}

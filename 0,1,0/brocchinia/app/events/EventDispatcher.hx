package brocchinia.app.events;

/**
 * EventDispatcher from package brocchinia.app.events.
 * Created by Chlorodatafile the 28/08/2015 at 10:50, on the project PotatoProject
 * Version : 0.0.4
 * The EventDispatcher is an abstracted class who allow you to assign event on an EventCatcher.
 */


import openfl.events.Event;
import openfl.events.AccelerometerEvent;
import openfl.events.TouchEvent;
import openfl.events.HTTPStatusEvent;
import openfl.events.NetStatusEvent;
import openfl.events.SampleDataEvent;
import openfl.events.TextEvent;
import openfl.events.DataEvent;
import openfl.events.ErrorEvent;
import openfl.events.AsyncErrorEvent;
import openfl.events.UncaughtErrorEvent;
import openfl.events.IOErrorEvent;
import openfl.events.SecurityErrorEvent;
import openfl.events.FocusEvent;
import openfl.events.TimerEvent;
import openfl.events.ProgressEvent;
import openfl.events.JoystickEvent;
import openfl.events.KeyboardEvent;
import openfl.events.MouseEvent;
import brocchinia.io.bind.BindedID;
import brocchinia.io.events.BindEvent;
import brocchinia.io.events.GamepadEvent;
import brocchinia.io.events.RessourceEvent;
import brocchinia.io.events.LangEvent;
import brocchinia.io.events.AmbientEvent;

import brocchinia.Make;
import openfl.display.Stage;
import openfl.display.DisplayObject;

class EventDispatcher extends DisplayObject {

    public function new() super();

    @:noCompletion @:final private function __catchEvent(catcher:EventCatcher) {
        for (e in catcher.catchedEvent)
            switch (e) {
// Smartphone Event
                case ACCELEROMETER_UPDATE: stage.addEventListener(AccelerometerEvent.UPDATE,catcher.caughtAccelerometerUpdate);
                case TOUCH_BEGIN: stage.addEventListener(TouchEvent.TOUCH_BEGIN,catcher.caughtTouchBegin);
                case TOUCH_END: stage.addEventListener(TouchEvent.TOUCH_END,catcher.caughtTouchEnd);
                case TOUCH_MOVE: stage.addEventListener(TouchEvent.TOUCH_MOVE,catcher.caughtTouchMove);
                case TOUCH_OUT: stage.addEventListener(TouchEvent.TOUCH_OUT,catcher.caughtTouchOut);
                case TOUCH_OVER: stage.addEventListener(TouchEvent.TOUCH_OVER,catcher.caughtTouchOver);
                case TOUCH_ROLL_OUT: stage.addEventListener(TouchEvent.TOUCH_ROLL_OUT,catcher.caughtTouchRollOut);
                case TOUCH_ROLL_OVER: stage.addEventListener(TouchEvent.TOUCH_ROLL_OVER,catcher.caughtTouchRollOver);
                case TOUCH_TAP: stage.addEventListener(TouchEvent.TOUCH_TAP,catcher.caughtTouchTap);

// Web Event
                case HTTP_STATUS: stage.addEventListener(HTTPStatusEvent.HTTP_STATUS,catcher.caughtHTTPStatus);
                case HTTP_RESPONSE_STATUS: stage.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS,catcher.caughtHTTPResponseStatus);
                case NET_STATUS: stage.addEventListener(NetStatusEvent.NET_STATUS,catcher.caughtNetStatus);
                case SAMPLE_DATA: stage.addEventListener(SampleDataEvent.SAMPLE_DATA,catcher.caughtSampleData);
                case TEXT_INPUT: stage.addEventListener(TextEvent.TEXT_INPUT,catcher.caughtTextInput);
                case LINK: stage.addEventListener(TextEvent.LINK,catcher.caughtLink);
                case DATA: stage.addEventListener(DataEvent.DATA,catcher.caughtData);
                case UPLOAD_COMPLETE_DATA: stage.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA,catcher.caughtUploadCompleteData);

// Error Event
                case ERROR: stage.addEventListener(ErrorEvent.ERROR,catcher.caughtError);
                case ASYNC_ERROR: stage.addEventListener(AsyncErrorEvent.ASYNC_ERROR,catcher.caughtAsyncError);
                case IO_ERROR: stage.addEventListener(IOErrorEvent.IO_ERROR,catcher.caughtIOError);
                case SECURITY_ERROR: stage.addEventListener(SecurityErrorEvent.SECURITY_ERROR,catcher.caughtSecurityError);
                case UNCAUGHT_ERROR: stage.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR,catcher.caughtUncaughtError);

// Focus Event
                case FOCUS_IN: stage.addEventListener(FocusEvent.FOCUS_IN,catcher.caughtFocusIn);
                case FOCUS_OUT: stage.addEventListener(FocusEvent.FOCUS_OUT,catcher.caughtFocusOut);
                case KEYBOARD_FOCUS_CHANGE: stage.addEventListener(FocusEvent.KEY_FOCUS_CHANGE,catcher.caughtKeyboardFocusChange);
                case MOUSE_FOCUS_CHANGE: stage.addEventListener(FocusEvent.MOUSE_FOCUS_CHANGE,catcher.caughtMouseFocusChange);

// Core Event
                case TIMER: stage.addEventListener(TimerEvent.TIMER,catcher.caughtTimer);
                case TIMER_COMPLETE: stage.addEventListener(TimerEvent.TIMER_COMPLETE,catcher.caughtTimerComplete);
                case SOUND_COMPLETE: stage.addEventListener(Event.SOUND_COMPLETE,catcher.caughtSoundComplete);
                case PROGRESS: stage.addEventListener(ProgressEvent.PROGRESS,catcher.caughtProgress);
                case SOCKET_DATA: stage.addEventListener(ProgressEvent.SOCKET_DATA,catcher.caughtSocketData);
                case NEW(tag,f): stage.addEventListener(tag,f);

// Bind Event
                case BIND_LOAD: stage.addEventListener(BindEvent.BIND_LOAD,catcher.caughtBindLoad);
                case BINDING_BEGIN: stage.addEventListener(BindEvent.BINDING_BEGIN,catcher.caughtBindingBegin);
                case BINDING_RECEIVED: stage.addEventListener(BindEvent.BINDING_RECEIVED,catcher.caughtBindingReceived);
                case BINDING_CANCELLED: stage.addEventListener(BindEvent.BINDING_CANCELLED,catcher.caughtBindingCancelled);
                case BINDED_KEY_PRESS(who,f): stage.addEventListener(BindEvent.BINDED_KEY_PRESS+who,f);
                case BINDED_KEY_RELEASE(who,f): stage.addEventListener(BindEvent.BINDED_KEY_RELEASE+who,f);

// Joystick Event
                case JOYSTICK_AXIS_MOVE: stage.addEventListener(JoystickEvent.AXIS_MOVE,catcher.caughtJoystickAxisMove);
                case JOYSTICK_BALL_MOVE: stage.addEventListener(JoystickEvent.BALL_MOVE,catcher.caughtJoystickBallMove);
                case JOYSTICK_HAT_MOVE: stage.addEventListener(JoystickEvent.HAT_MOVE,catcher.caughtJoystickHatMove);
                case JOYSTICK_BUTTON_DOWN: stage.addEventListener(JoystickEvent.BUTTON_DOWN,catcher.caughtJoystickButtonDown);
                case JOYSTICK_BUTTON_UP: stage.addEventListener(JoystickEvent.BUTTON_UP,catcher.caughtJoystickButtonUp);
                case JOYSTICK_ADDED: stage.addEventListener(JoystickEvent.DEVICE_ADDED,catcher.caughtJoystickAdded);
                case JOYSTICK_REMOVED: stage.addEventListener(JoystickEvent.DEVICE_REMOVED,catcher.caughtJoystickRemoved);

// Keyboard Event
                case KEY_DOWN: stage.addEventListener(KeyboardEvent.KEY_DOWN,catcher.caughtKeyDown);
                case KEY_UP: stage.addEventListener(KeyboardEvent.KEY_UP,catcher.caughtKeyUp);

// Mouse Event
                case MOUSE_SIMPLE_CLICK: stage.addEventListener(MouseEvent.CLICK,catcher.caughtMouseSimpleClick);
                case MOUSE_RIGHT_CLICK: stage.addEventListener(MouseEvent.RIGHT_CLICK,catcher.caughtMouseRightClick);
                case MOUSE_DOUBLE_CLICK: stage.addEventListener(MouseEvent.DOUBLE_CLICK,catcher.caughtMouseDoubleClick);
                case MOUSE_MIDDLE_CLICK: stage.addEventListener(MouseEvent.MIDDLE_CLICK,catcher.caughtMouseMiddleClick);
                case MOUSE_MIDDLE_PRESSED: stage.addEventListener(MouseEvent.MIDDLE_MOUSE_DOWN,catcher.caughtMouseMiddlePressed);
                case MOUSE_MIDDLE_RELEASED: stage.addEventListener(MouseEvent.MIDDLE_MOUSE_UP,catcher.caughtMouseMiddleReleased);
                case MOUSE_LEFT_PRESSED: stage.addEventListener(MouseEvent.MOUSE_DOWN,catcher.caughtMouseLeftPressed);
                case MOUSE_LEFT_RELEASED: stage.addEventListener(MouseEvent.MOUSE_UP,catcher.caughtMouseLeftReleased);
                case MOUSE_RIGHT_PRESSED: stage.addEventListener(MouseEvent.RIGHT_MOUSE_DOWN,catcher.caughtMouseRightPressed);
                case MOUSE_RIGHT_RELEASED: stage.addEventListener(MouseEvent.RIGHT_MOUSE_UP,catcher.caughtMouseRightReleased);
                case MOUSE_MOVE: stage.addEventListener(MouseEvent.MOUSE_MOVE,catcher.caughtMouseMove);
                case MOUSE_OUT: stage.addEventListener(MouseEvent.MOUSE_OUT,catcher.caughtMouseOut);
                case MOUSE_OVER: stage.addEventListener(MouseEvent.MOUSE_OVER,catcher.caughtMouseOver);
                case MOUSE_WHEEL: stage.addEventListener(MouseEvent.MOUSE_WHEEL,catcher.caughtMouseWheel);
                case MOUSE_ROLL_OUT: stage.addEventListener(MouseEvent.ROLL_OUT,catcher.caughtMouseRollOut);
                case MOUSE_ROLL_OVER: stage.addEventListener(MouseEvent.ROLL_OVER,catcher.caughtMouseRollOver);

// Gamepad Event
                case GAMEPAD_AXIS_MOVE: stage.addEventListener(GamepadEvent.AXIS_MOVE,catcher.caughtGamepadAxisMove);
                case GAMEPAD_BUTTON_DOWN: stage.addEventListener(GamepadEvent.BUTTON_DOWN,catcher.caughtGamepadButtonDown);
                case GAMEPAD_BUTTON_UP: stage.addEventListener(GamepadEvent.BUTTON_UP,catcher.caughtGamepadButtonUp);
                case GAMEPAD_ADDED: stage.addEventListener(GamepadEvent.ADDED,catcher.caughtGamepadAdded);
                case GAMEPAD_REMOVED: stage.addEventListener(GamepadEvent.REMOVED,catcher.caughtGamepadRemoved);

// Ressource Event
                case RESSOURCE_INITED: stage.addEventListener(RessourceEvent.RESSOURCE_INITED,catcher.caughtRessourceInited);
                case CONFIG_LOADED: stage.addEventListener(RessourceEvent.CONFIG_LOADED,catcher.caughtConfigLoaded);
                case CONFIG_SAVED: stage.addEventListener(RessourceEvent.CONFIG_SAVED,catcher.caughtConfigSaved);

// Lang Event
                case LANG_CHANGED: stage.addEventListener(LangEvent.LANG_CHANGED,catcher.caughtLangChanged);
                case LANG_CHUNK_LOADED: stage.addEventListener(LangEvent.LANG_CHUNK_LOADED,catcher.caughtLangChunkLoaded);

// Ambient Event
                case AMBIENT_LOAD: stage.addEventListener(AmbientEvent.AMBIENT_LOAD,catcher.caughtAmbientLoad);
                case AMBIENT_CHANGE: stage.addEventListener(AmbientEvent.AMBIENT_CHANGE,catcher.caughtAmbientChange);
                case AMBIENT_END: stage.addEventListener(AmbientEvent.AMBIENT_END,catcher.caughtAmbientEnd);
                case AMBIENT_START: stage.addEventListener(AmbientEvent.AMBIENT_START,catcher.caughtAmbientStart);
            }
    }

    @:noCompletion @:final private function __removeEvent(catcher:EventCatcher) {
        for (e in catcher.catchedEvent)
            switch (e) {
// Smartphone Event
                case ACCELEROMETER_UPDATE: stage.removeEventListener(AccelerometerEvent.UPDATE,catcher.caughtAccelerometerUpdate);
                case TOUCH_BEGIN: stage.removeEventListener(TouchEvent.TOUCH_BEGIN,catcher.caughtTouchBegin);
                case TOUCH_END: stage.removeEventListener(TouchEvent.TOUCH_END,catcher.caughtTouchEnd);
                case TOUCH_MOVE: stage.removeEventListener(TouchEvent.TOUCH_MOVE,catcher.caughtTouchMove);
                case TOUCH_OUT: stage.removeEventListener(TouchEvent.TOUCH_OUT,catcher.caughtTouchOut);
                case TOUCH_OVER: stage.removeEventListener(TouchEvent.TOUCH_OVER,catcher.caughtTouchOver);
                case TOUCH_ROLL_OUT: stage.removeEventListener(TouchEvent.TOUCH_ROLL_OUT,catcher.caughtTouchRollOut);
                case TOUCH_ROLL_OVER: stage.removeEventListener(TouchEvent.TOUCH_ROLL_OVER,catcher.caughtTouchRollOver);
                case TOUCH_TAP: stage.removeEventListener(TouchEvent.TOUCH_TAP,catcher.caughtTouchTap);

// Web Event
                case HTTP_STATUS: stage.removeEventListener(HTTPStatusEvent.HTTP_STATUS,catcher.caughtHTTPStatus);
                case HTTP_RESPONSE_STATUS: stage.removeEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS,catcher.caughtHTTPResponseStatus);
                case NET_STATUS: stage.removeEventListener(NetStatusEvent.NET_STATUS,catcher.caughtNetStatus);
                case SAMPLE_DATA: stage.removeEventListener(SampleDataEvent.SAMPLE_DATA,catcher.caughtSampleData);
                case TEXT_INPUT: stage.removeEventListener(TextEvent.TEXT_INPUT,catcher.caughtTextInput);
                case LINK: stage.removeEventListener(TextEvent.LINK,catcher.caughtLink);
                case DATA: stage.removeEventListener(DataEvent.DATA,catcher.caughtData);
                case UPLOAD_COMPLETE_DATA: stage.removeEventListener(DataEvent.UPLOAD_COMPLETE_DATA,catcher.caughtUploadCompleteData);

// Error Event
                case ERROR: stage.removeEventListener(ErrorEvent.ERROR,catcher.caughtError);
                case ASYNC_ERROR: stage.removeEventListener(AsyncErrorEvent.ASYNC_ERROR,catcher.caughtAsyncError);
                case IO_ERROR: stage.removeEventListener(IOErrorEvent.IO_ERROR,catcher.caughtIOError);
                case SECURITY_ERROR: stage.removeEventListener(SecurityErrorEvent.SECURITY_ERROR,catcher.caughtSecurityError);
                case UNCAUGHT_ERROR: stage.removeEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR,catcher.caughtUncaughtError);

// Focus Event
                case FOCUS_IN: stage.removeEventListener(FocusEvent.FOCUS_IN,catcher.caughtFocusIn);
                case FOCUS_OUT: stage.removeEventListener(FocusEvent.FOCUS_OUT,catcher.caughtFocusOut);
                case KEYBOARD_FOCUS_CHANGE: stage.removeEventListener(FocusEvent.KEY_FOCUS_CHANGE,catcher.caughtKeyboardFocusChange);
                case MOUSE_FOCUS_CHANGE: stage.removeEventListener(FocusEvent.MOUSE_FOCUS_CHANGE,catcher.caughtMouseFocusChange);

// Core Event
                case TIMER: stage.removeEventListener(TimerEvent.TIMER,catcher.caughtTimer);
                case TIMER_COMPLETE: stage.removeEventListener(TimerEvent.TIMER_COMPLETE,catcher.caughtTimerComplete);
                case SOUND_COMPLETE: stage.removeEventListener(Event.SOUND_COMPLETE,catcher.caughtSoundComplete);
                case PROGRESS: stage.removeEventListener(ProgressEvent.PROGRESS,catcher.caughtProgress);
                case SOCKET_DATA: stage.removeEventListener(ProgressEvent.SOCKET_DATA,catcher.caughtSocketData);
                case NEW(tag,f): stage.removeEventListener(tag,f);

// Bind Event
                case BIND_LOAD: stage.removeEventListener(BindEvent.BIND_LOAD,catcher.caughtBindLoad);
                case BINDING_BEGIN: stage.removeEventListener(BindEvent.BINDING_BEGIN,catcher.caughtBindingBegin);
                case BINDING_RECEIVED: stage.removeEventListener(BindEvent.BINDING_RECEIVED,catcher.caughtBindingReceived);
                case BINDING_CANCELLED: stage.removeEventListener(BindEvent.BINDING_CANCELLED,catcher.caughtBindingCancelled);
                case BINDED_KEY_PRESS(who,f): stage.removeEventListener(BindEvent.BINDED_KEY_PRESS+who,f);
                case BINDED_KEY_RELEASE(who,f): stage.removeEventListener(BindEvent.BINDED_KEY_RELEASE+who,f);

// Joystick Event
                case JOYSTICK_AXIS_MOVE: stage.removeEventListener(JoystickEvent.AXIS_MOVE,catcher.caughtJoystickAxisMove);
                case JOYSTICK_BALL_MOVE: stage.removeEventListener(JoystickEvent.BALL_MOVE,catcher.caughtJoystickBallMove);
                case JOYSTICK_HAT_MOVE: stage.removeEventListener(JoystickEvent.HAT_MOVE,catcher.caughtJoystickHatMove);
                case JOYSTICK_BUTTON_DOWN: stage.removeEventListener(JoystickEvent.BUTTON_DOWN,catcher.caughtJoystickButtonDown);
                case JOYSTICK_BUTTON_UP: stage.removeEventListener(JoystickEvent.BUTTON_UP,catcher.caughtJoystickButtonUp);
                case JOYSTICK_ADDED: stage.removeEventListener(JoystickEvent.DEVICE_ADDED,catcher.caughtJoystickAdded);
                case JOYSTICK_REMOVED: stage.removeEventListener(JoystickEvent.DEVICE_REMOVED,catcher.caughtJoystickRemoved);

// Keyboard Event
                case KEY_DOWN: stage.removeEventListener(KeyboardEvent.KEY_DOWN,catcher.caughtKeyDown);
                case KEY_UP: stage.removeEventListener(KeyboardEvent.KEY_UP,catcher.caughtKeyUp);

// Mouse Event
                case MOUSE_SIMPLE_CLICK: stage.removeEventListener(MouseEvent.CLICK,catcher.caughtMouseSimpleClick);
                case MOUSE_RIGHT_CLICK: stage.removeEventListener(MouseEvent.RIGHT_CLICK,catcher.caughtMouseRightClick);
                case MOUSE_DOUBLE_CLICK: stage.removeEventListener(MouseEvent.DOUBLE_CLICK,catcher.caughtMouseDoubleClick);
                case MOUSE_MIDDLE_CLICK: stage.removeEventListener(MouseEvent.MIDDLE_CLICK,catcher.caughtMouseMiddleClick);
                case MOUSE_MIDDLE_PRESSED: stage.removeEventListener(MouseEvent.MIDDLE_MOUSE_DOWN,catcher.caughtMouseMiddlePressed);
                case MOUSE_MIDDLE_RELEASED: stage.removeEventListener(MouseEvent.MIDDLE_MOUSE_UP,catcher.caughtMouseMiddleReleased);
                case MOUSE_LEFT_PRESSED: stage.removeEventListener(MouseEvent.MOUSE_DOWN,catcher.caughtMouseLeftPressed);
                case MOUSE_LEFT_RELEASED: stage.removeEventListener(MouseEvent.MOUSE_UP,catcher.caughtMouseLeftReleased);
                case MOUSE_RIGHT_PRESSED: stage.removeEventListener(MouseEvent.RIGHT_MOUSE_DOWN,catcher.caughtMouseRightPressed);
                case MOUSE_RIGHT_RELEASED: stage.removeEventListener(MouseEvent.RIGHT_MOUSE_UP,catcher.caughtMouseRightReleased);
                case MOUSE_MOVE: stage.removeEventListener(MouseEvent.MOUSE_MOVE,catcher.caughtMouseMove);
                case MOUSE_OUT: stage.removeEventListener(MouseEvent.MOUSE_OUT,catcher.caughtMouseOut);
                case MOUSE_OVER: stage.removeEventListener(MouseEvent.MOUSE_OVER,catcher.caughtMouseOver);
                case MOUSE_WHEEL: stage.removeEventListener(MouseEvent.MOUSE_WHEEL,catcher.caughtMouseWheel);
                case MOUSE_ROLL_OUT: stage.removeEventListener(MouseEvent.ROLL_OUT,catcher.caughtMouseRollOut);
                case MOUSE_ROLL_OVER: stage.removeEventListener(MouseEvent.ROLL_OVER,catcher.caughtMouseRollOver);

// Gamepad Event
                case GAMEPAD_AXIS_MOVE: stage.removeEventListener(GamepadEvent.AXIS_MOVE,catcher.caughtGamepadAxisMove);
                case GAMEPAD_BUTTON_DOWN: stage.removeEventListener(GamepadEvent.BUTTON_DOWN,catcher.caughtGamepadButtonDown);
                case GAMEPAD_BUTTON_UP: stage.removeEventListener(GamepadEvent.BUTTON_UP,catcher.caughtGamepadButtonUp);
                case GAMEPAD_ADDED: stage.removeEventListener(GamepadEvent.ADDED,catcher.caughtGamepadAdded);
                case GAMEPAD_REMOVED: stage.removeEventListener(GamepadEvent.REMOVED,catcher.caughtGamepadRemoved);

// Ressource Event
                case RESSOURCE_INITED: stage.removeEventListener(RessourceEvent.RESSOURCE_INITED,catcher.caughtRessourceInited);
                case CONFIG_LOADED: stage.removeEventListener(RessourceEvent.CONFIG_LOADED,catcher.caughtConfigLoaded);
                case CONFIG_SAVED: stage.removeEventListener(RessourceEvent.CONFIG_SAVED,catcher.caughtConfigSaved);

// Lang Event
                case LANG_CHANGED: stage.removeEventListener(LangEvent.LANG_CHANGED,catcher.caughtLangChanged);
                case LANG_CHUNK_LOADED: stage.removeEventListener(LangEvent.LANG_CHUNK_LOADED,catcher.caughtLangChunkLoaded);

// Ambient Event
                case AMBIENT_LOAD: stage.removeEventListener(AmbientEvent.AMBIENT_LOAD,catcher.caughtAmbientLoad);
                case AMBIENT_CHANGE: stage.removeEventListener(AmbientEvent.AMBIENT_CHANGE,catcher.caughtAmbientChange);
                case AMBIENT_END: stage.removeEventListener(AmbientEvent.AMBIENT_END,catcher.caughtAmbientEnd);
                case AMBIENT_START: stage.removeEventListener(AmbientEvent.AMBIENT_START,catcher.caughtAmbientStart);
            }
    }

}
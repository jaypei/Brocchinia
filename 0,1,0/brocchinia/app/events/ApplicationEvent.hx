package brocchinia.app.events;

/**
 * ApplicationEvent from package brocchinia.app.events.
 * Created by Chlorodatafile the 28/08/2015 at 00:12, on the project PotatoProject
 * Version : 0.0.3
 * This class contain the Event who can dispatch an Application.
 */


import openfl.events.Event;

class ApplicationEvent extends Event {

    public static var APPLICATION_INITIALISED:String    = "brocchiniaApplicationInitialised";
    public static var APPLICATION_TERMINATED:String     = "brocchiniaApplicationTerminated";
    public static var POPUP_POP_IN:String               = "brocchiniaApplicationPopUpPopIn";
    public static var POPUP_POP_OUT:String              = "brocchiniaApplicationPopUpPopOut";
    public static var LAUNCH_PROGRAM:String             = "brocchiniaApplicationLaunchProgram";
    public static var PAUSE_PROGRAM:String              = "brocchiniaApplicationPauseProgram";
    public static var RESUME_PROGRAM:String             = "brocchiniaApplicationResumeProgram";
    public static var DISPOSE_PROGRAM:String            = "brocchiniaApplicationDisposeProgram";
    public static var OPEN_WINDOW:String                = "brocchiniaApplicationOpenWindow";
    public static var CLOSE_WINDOW:String               = "brocchiniaApplicationCloseWindow";

    public var which:Performer;

    public function new(type:String, ?from:Performer) {
        super(type, false, false);
        which=from;
    }

    public override function clone():Event {
        return new ApplicationEvent(type,which);
    }

    public override function toString():String {
        return "[ApplicationEvent type=\"" + type + "\" bubbles=false cancelable=false which=" + which + "]";
    }
}

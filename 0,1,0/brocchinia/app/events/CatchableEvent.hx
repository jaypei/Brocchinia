package brocchinia.app.events;

/**
 * Class CatchableEvent from brocchinia.app.events.
 * Created by Chlorodatafile the 04/08/2015 for the first time in RVB-Project.
 * Version : 0.3.1
 * This Enum contain all the Catchable Event in Brocchinia.
 */


import brocchinia.io.bind.BindedID;
import brocchinia.io.events.BindEvent.BindedKeyEvent;
import openfl.events.Event;

enum CatchableEvent {
    // Smartphone Event
    ACCELEROMETER_UPDATE;       //tag=AccelerometerEvent.UPDATE
    TOUCH_BEGIN;                //tag=TouchEvent.TOUCH_BEGIN
    TOUCH_END;                  //tag=TouchEvent.TOUCH_END
    TOUCH_MOVE;                 //tag=TouchEvent.TOUCH_MOVE
    TOUCH_OUT;                  //tag=TouchEvent.TOUCH_OUT
    TOUCH_OVER;                 //tag=TouchEvent.TOUCH_OVER
    TOUCH_ROLL_OUT;             //tag=TouchEvent.TOUCH_ROLL_OUT
    TOUCH_ROLL_OVER;            //tag=TouchEvent.TOUCH_ROLL_OVER
    TOUCH_TAP;                  //tag=TouchEvent.TOUCH_TAP

    // Web Event
    HTTP_STATUS;                //tag=HTTPStatusEvent.HTTP_STATUS
    HTTP_RESPONSE_STATUS;       //tag=HTTPStatusEvent.HTTP_RESPONSE_STATUS
    NET_STATUS;                 //tag=NetStatusEvent.NET_STATUS
    SAMPLE_DATA;                //tag=SampleDataEvent.SAMPLE_DATA
    TEXT_INPUT;                 //tag=TextEvent.TEXT_INPUT
    LINK;                       //tag=TextEvent.LINK
    DATA;                       //tag=DataEvent.DATA
    UPLOAD_COMPLETE_DATA;       //tag=DataEvent.UPLOAD_COMPLETE_DATA

    // Error Event
    ERROR;                      //tag=ErrorEvent.ERROR
    ASYNC_ERROR;                //tag=AsyncErrorEvent.ASYNC_ERROR
    IO_ERROR;                   //tag=IOErrorEvent.IO_ERROR
    SECURITY_ERROR;             //tag=SecurityErrorEvent.SECURITY_ERROR
    UNCAUGHT_ERROR;             //tag=UncaughtErrorEvent.UNCAUGHT_ERROR

    // Focus Event
    FOCUS_IN;                   //tag=FocusEvent.FOCUS_IN
    FOCUS_OUT;                  //tag=FocusEvent.FOCUS_OUT
    KEYBOARD_FOCUS_CHANGE;      //tag=FocusEvent.KEY_FOCUS_CHANGE
    MOUSE_FOCUS_CHANGE;         //tag=FocusEvent.MOUSE_FOCUS_CHANGE

    // Core Event
    TIMER;                      //tag=TimerEvent.TIMER
    TIMER_COMPLETE;             //tag=TimerEvent.TIMER_COMPLETE
    SOUND_COMPLETE;             //tag=Event.SOUND_COMPLETE
    PROGRESS;                   //tag=ProgressEvent.PROGRESS
    SOCKET_DATA;                //tag=ProgressEvent.SOCKET_DATA
    NEW(tag:String,f:Event->Void);

    // Bind Event
    BIND_LOAD;                                                 //tag=BindEvent.BIND_LOAD
    BINDING_BEGIN;                                             //tag=BindEvent.BINDING_BEGIN
    BINDING_RECEIVED;                                          //tag=BindEvent.BINDING_RECEIVE
    BINDING_CANCELLED;                                         //tag=BindEvent.BINDING_CANCELLED
    BINDED_KEY_PRESS(who:BindedID,f:BindedKeyEvent->Void);     //tag=BindEvent.BINDED_KEY_PRESS
    BINDED_KEY_RELEASE(who:BindedID,f:BindedKeyEvent->Void);   //tag=BindEvent.BINDED_KEY_RELEASE

    // Joystick Event
    JOYSTICK_AXIS_MOVE;         //tag=JoystickEvent.AXIS_MOVE
    JOYSTICK_BALL_MOVE;         //tag=JoystickEvent.BALL_MOVE
    JOYSTICK_HAT_MOVE;          //tag=JoystickEvent.HAT_MOVE
    JOYSTICK_BUTTON_DOWN;       //tag=JoystickEvent.BUTTON_DOWN
    JOYSTICK_BUTTON_UP;         //tag=JoystickEvent.BUTTON_UP
    JOYSTICK_ADDED;             //tag=JoystickEvent.DEVICE_ADDED
    JOYSTICK_REMOVED;           //tag=JoystickEvent.DEVICE_REMOVED

    // Keyboard Event
    KEY_DOWN;                   //tag=KeyboardEvent.KEY_DOWN
    KEY_UP;                     //tag=KeyboardEvent.KEY_UP

    // Mouse Event
    MOUSE_SIMPLE_CLICK;         //tag=MouseEvent.CLICK
    MOUSE_RIGHT_CLICK;          //tag=MouseEvent.RIGHT_CLICK
    MOUSE_DOUBLE_CLICK;         //tag=MouseEvent.DOUBLE_CLICK
    MOUSE_MIDDLE_CLICK;         //tag=MouseEvent.MIDDLE_CLICK
    MOUSE_MIDDLE_PRESSED;       //tag=MouseEvent.MIDDLE_MOUSE_DOWN
    MOUSE_MIDDLE_RELEASED;      //tag=MouseEvent.MIDDLE_MOUSE_UP
    MOUSE_LEFT_PRESSED;         //tag=MouseEvent.MOUSE_DOWN
    MOUSE_LEFT_RELEASED;        //tag=MouseEvent.MOUSE_UP
    MOUSE_RIGHT_PRESSED;        //tag=MouseEvent.RIGHT_MOUSE_DOWN
    MOUSE_RIGHT_RELEASED;       //tag=MouseEvent.RIGHT_MOUSE_UP
    MOUSE_MOVE;                 //tag=MouseEvent.MOUSE_MOVE
    MOUSE_OUT;                  //tag=MouseEvent.MOUSE_OUT
    MOUSE_OVER;                 //tag=MouseEvent.MOUSE_OVER
    MOUSE_WHEEL;                //tag=MouseEvent.MOUSE_WHEEL
    MOUSE_ROLL_OUT;             //tag=MouseEvent.ROLL_OUT
    MOUSE_ROLL_OVER;            //tag=MouseEvent.ROLL_OVER

    // Gamepad Event
    GAMEPAD_AXIS_MOVE;          //tag=GamepadEvent.AXIS_MOVE
    GAMEPAD_BUTTON_DOWN;        //tag=GamepadEvent.BUTTON_DOWN
    GAMEPAD_BUTTON_UP;          //tag=GamepadEvent.BUTTON_UP
    GAMEPAD_ADDED;              //tag=GamepadEvent.ADDED
    GAMEPAD_REMOVED;            //tag=GamepadEvent.REMOVED

    // Data Event
    RESSOURCE_INITED;           //tag=ParameterEvent.RESSOURCE_INITED
    CONFIG_LOADED;              //tag=ParameterEvent.CONFIG_LOADED
    CONFIG_SAVED;               //tag=ParameterEvent.CONFIG_SAVED

    // Lang Event
    LANG_CHANGED;               //tag=LangEvent.LANG_CHANGED
    LANG_CHUNK_LOADED;          //tag=LangEvent.LANG_CHUNK_LOADED
    
    // Ambient Event
    AMBIENT_LOAD;               //tag=AmbientEvent.AMBIENT_LOAD
    AMBIENT_CHANGE;             //tag=AmbientEvent.AMBIENT_CHANGE
    AMBIENT_END;                //tag=AmbientEvent.AMBIENT_END
    AMBIENT_START;              //tag=AmbientEvent.AMBIENT_START
}